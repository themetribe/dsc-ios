//
//  PastRewardsVC.swift
//  DRunnerCustomer
//
//  Created by mac306 on 11/08/20.
//  Copyright © 2020 mac306. All rights reserved.
//

import UIKit

class PastRewardsVC: UIViewController, UITableViewDelegate,UITableViewDataSource {
   
    

@IBOutlet weak var tblData : UITableView?

override func viewDidLoad() {
    super.viewDidLoad()

    self.tblData?.tableFooterView = UIView()
    // Do any additional setup after loading the view.
    }


    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
           return 100.0
       }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
   
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           let cell : MyRewardsCell = self.tblData?.dequeueReusableCell(withIdentifier: "MyRewardsCell", for: indexPath) as! MyRewardsCell
           return cell
       }
       
}
