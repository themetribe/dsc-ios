//
//  MyRewardsContainerVC.swift
//  DRunnerCustomer
//
//  Created by mac306 on 11/08/20.
//  Copyright © 2020 mac306. All rights reserved.
//

import UIKit

class MyRewardsContainerVC: UIViewController {

    @IBOutlet var btnActiveRewards: UIButton?
    @IBOutlet var btnPastRewards: UIButton?
    @IBOutlet var containView: UIView?
       
    var pageViewController: UIPageViewController?
    var viewControllers: [UIViewController]?
    var currentpage = 0
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    self.setupPageController()
    self.setButtonUI(ctPage: 0)
                // Do any additional setup after loading the view.
    }
    @IBAction func onBackButton( _ sender : UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }

            
            private func setupPageController() {
                   DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
                       self.pageViewController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
                    self.pageViewController?.delegate = self
                       self.viewControllers = [
                           REWARDS_SB.instantiateViewController(withIdentifier: "ActiveRewardsVC"),
                           REWARDS_SB.instantiateViewController(withIdentifier: "PastRewardsVC")
                          ]
                       
                    self.pageViewController?.setViewControllers([ self.viewControllerAtIndex(0)!], direction: .forward, animated: true, completion: nil)
                    self.pageViewController?.dataSource = self
                    if let scrollView =  self.pageViewController?.view.subviews.filter({$0.isKind(of: UIScrollView.self)}).first as? UIScrollView {
                         scrollView.backgroundColor = UIColor.clear
                           scrollView.isScrollEnabled = false
                       }
                       
                     self.pageViewController!.view.frame = self.containView?.frame as! CGRect
                    self.addChild(self.pageViewController!)
                     
                     self.view?.addSubview((self.pageViewController?.view!)!)
                    
                     self.view?.gestureRecognizers = self.pageViewController?.gestureRecognizers
                   }
                  
               }
            func setButtonUI(ctPage : Int)
            {
                if ctPage == 0
                {
                    self.btnPastRewards?.layer.addBorder(edge: .bottom, color: UIColor.white, thickness: 2.0)
                    self.btnPastRewards?.setTitleColor(.darkGray, for: .normal)
                    self.btnActiveRewards?.layer.addBorder(edge: .bottom, color: .black, thickness: 2.0)
                    self.btnActiveRewards?.setTitleColor(.black, for: .normal)
                   
                    
                }
                else{
                     self.btnPastRewards?.layer.addBorder(edge: .bottom, color: UIColor.black, thickness: 2.0)
                     self.btnPastRewards?.setTitleColor(.black, for: .normal)
                    self.btnActiveRewards?.layer.addBorder(edge: .bottom, color: .white, thickness: 2.0)
                     self.btnActiveRewards?.setTitleColor(.darkGray, for: .normal)
                    
                }
                
            }
            @IBAction func onActiveRewards(_ sender: Any) {
                
                self.setButtonUI(ctPage: 0)
                if currentpage == 0{
                    return
                }
                else{
                    currentpage = 0
                              self.changePage(.reverse);
                    }
                    self.handlePageChange()
                         
                }
                     
            @IBAction func onPastRewards(_ sender: Any) {
                    
                self.setButtonUI(ctPage: 1)
                         if currentpage == 1{
                            return
                         }
                         else{
                              currentpage = 1
                              self.changePage(.forward);
                          }
                          self.handlePageChange()
                      }
            
           
        }
extension MyRewardsContainerVC: UIPageViewControllerDataSource, UIPageViewControllerDelegate {
            
            func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
                var index = indexOfViewController(viewController)
                
                if (index == 0) || (index == NSNotFound) {
                    return nil
                }
                
                index -= 1
                
                return viewControllerAtIndex(index)
            }
            
            func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
                var index = indexOfViewController(viewController)
                
                if index == NSNotFound {
                    return nil
                }
                
                index += 1
                
                if index == viewControllers?.count {
                    return nil
                }
                
                return viewControllerAtIndex(index)
            }
            
            
            
            func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
                guard let indexPage = viewControllers?.firstIndex(of: (pageViewController.viewControllers?.first)!) else{
                    return
                }
                print("current page=== ",indexPage)
                currentpage = indexPage
                self.handlePageChange()
                
            }
            
        }

        // MARK: - Helpers
extension MyRewardsContainerVC {
            func changePage(_ direction: UIPageViewController.NavigationDirection) {
                let viewController = viewControllerAtIndex(currentpage)
                if viewController == nil {
                    return
                }
                pageViewController?.setViewControllers([viewController!], direction: direction, animated: true, completion: nil)
            }
            
            @objc fileprivate func handlePageChange(){
                
                if currentpage == 0{
                    
                }else if currentpage == 1{
                    
                }else{
                    
                }
                DispatchQueue.main.async {
                    
                    
                }
            }
            
            fileprivate func viewControllerAtIndex(_ index: Int) -> UIViewController? {
                if viewControllers?.count == 0 || index >= viewControllers!.count {
                    return nil
                }
                
                return viewControllers?[index]
            }
            
            fileprivate func indexOfViewController(_ viewController: UIViewController) -> Int {
                return viewControllers?.firstIndex(of: viewController) ?? NSNotFound
            }
        }

