//
//  RewardsVC.swift
//  DRunnerCustomer
//
//  Created by mac306 on 11/08/20.
//  Copyright © 2020 mac306. All rights reserved.
//

import UIKit

class RewardsVC: UIViewController {
    var userData = defaults.object(forKey: udUserInfo) as! Dictionary<String,Any>
    @IBOutlet weak var lblRanking : UILabel?
    @IBOutlet weak var imgRanking : UIImageView?
    @IBOutlet weak var lblPoints : UILabel?
    @IBOutlet weak var tblData : UITableView?
    var arrayPointList : Array = [Dictionary<String,Any>()]
    override func viewDidLoad() {
        super.viewDidLoad()
       
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        
        DispatchQueue.main.async {
             self.lblPoints?.text = "\(Helper.shared.getUserPoints()) Points"
            self.lblRanking?.text = "\(self.userData["ranking"] as? String ?? "")"
            self.imgRanking?.image = Helper.shared.getRankingImage(userRank: self.userData["ranking"] as? String ?? "")
        }
        
        self.arrayPointList.removeAll()
        self.getRewardsList()
    }
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    @IBAction func onBackButton( _ sender : UIButton)
    {
        DispatchQueue.main.async {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func getRewardsList()
    {
        API_HELPER.sharedInstance.postDataInJsonUsingNS(header: K.ApiName.Redeem_Points_List, withParameter: ["user_id" : Helper.shared.getUserID()], inVC: self, showHud: true, addAuthHeader: true) { (result, message, status) in
            DispatchQueue.main.async {
                if status
                {
                    self.arrayPointList.removeAll()
                    self.arrayPointList = result["data"] as! Array
                    self.tblData?.reloadData()
                }
                else
                {
                    self.showToast(message)
                }
            }
        }
    }
    @objc func onRedeemPoints(_ sender : UIButton)
    {
        let pointsData = self.arrayPointList[sender.tag]
        API_HELPER.sharedInstance.postDataInJsonUsingNS(header: K.ApiName.Redeem_Points, withParameter: ["user_id" : Helper.shared.getUserID(), "point_id" : pointsData["id"] as? String ?? ""], inVC: self, showHud: true, addAuthHeader: true) { (result, message, status) in
            DispatchQueue.main.async {
                if status
                {
                    Helper.shared.setUserPoints(value: result["reward_points"] as? String ?? "0")
                    Helper.shared.setUserWallet(value: result["wallet_amount"] as? String ?? "0")
                    self.lblPoints?.text = "\(Helper.shared.getUserPoints()) Points"
                }
                
                self.showToast(message)
            }
        }
        
    }
    
}

extension RewardsVC : UITableViewDelegate, UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.arrayPointList.count
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : RewardsListCell = tableView.dequeueReusableCell(withIdentifier: "RewardsListCell", for: indexPath) as! RewardsListCell
        
        let pointsData = self.arrayPointList[indexPath.row]
        cell.lblPrice?.text = "Get S$ \(pointsData["off"] as? String ?? "") in your wallet"
        cell.lblPoints?.text = "\(pointsData["points"] as? String ?? "")"
        cell.btnRedeem?.tag = indexPath.row
        cell.btnRedeem?.addTarget(self, action: #selector(self.onRedeemPoints(_:)), for: .touchUpInside)
        return cell
    }
    
}
