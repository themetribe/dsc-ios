//
//  SettingsCell.swift
//  DRunnerCustomer
//
//  Created by mac306 on 27/07/20.
//  Copyright © 2020 mac306. All rights reserved.
//

import UIKit

class SettingsCell: UITableViewCell {

     @IBOutlet weak var imgCell : UIImageView?
     @IBOutlet weak var lblName : UILabel?
    @IBOutlet weak var lblPoints : UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @objc func addDataInCell(settingTitle : String , settingImage : String)
    {
        self.lblName?.text = settingTitle
       // self.imageView?.image = UIImage(named : settingImage)
    }

}
class SettingsTopCell: UITableViewCell {

     @IBOutlet weak var imgUser : UIImageView?
     @IBOutlet weak var lblName : UILabel?
    @IBOutlet weak var btnEditProfile : UIButton?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    

}
