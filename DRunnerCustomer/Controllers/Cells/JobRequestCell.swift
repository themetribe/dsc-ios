//
//  JobRequestCell.swift
//  DRunnerCustomer
//
//  Created by mac306 on 29/09/20.
//  Copyright © 2020 mac306. All rights reserved.
//

import UIKit

class JobRequestCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
class JobRunnerDetailCell: UITableViewCell {
    @IBOutlet weak var viewProcessing : UIView?
    @IBOutlet weak var imgRunner : UIImageView?
    @IBOutlet weak var lblRunner : UILabel?
    @IBOutlet weak var lblNoAssignRunner : UILabel?
    @IBOutlet weak var lblVehicalType : UILabel?
    @IBOutlet weak var btnCall : UIButton?
    @IBOutlet weak var btnChat : UIButton?
    @IBOutlet weak var btnLike : UIButton?
    @IBOutlet weak var btnTrack : UIButton?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
class JobOrderDetailCell: UITableViewCell {
    
    @IBOutlet weak var lblStatus : UILabel?
    @IBOutlet weak var lblDate : UILabel?
    @IBOutlet weak var lblCategory : UILabel?
    @IBOutlet weak var lblWeight : UILabel?
    @IBOutlet weak var itemDetail : UILabel?
    @IBOutlet weak var lblMultistop : UILabel?
    @IBOutlet weak var lblFair : UILabel?
    @IBOutlet weak var lblWaitingCharge : UILabel?
    @IBOutlet weak var lblTotalFair : UILabel?
    
    @IBOutlet weak var lblOfferAmount : UILabel?
    @IBOutlet weak var lblNightCharge : UILabel?
    @IBOutlet weak var lblNetAmount : UILabel?
   
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

class JobLocationDetailCell: UITableViewCell {
    @IBOutlet weak var lblDetail : UILabel?
    @IBOutlet weak var lblStatus : UILabel?
    @IBOutlet weak var lblName : UILabel?
    @IBOutlet weak var lblMobile : UILabel?
    @IBOutlet weak var lblLandMark : UILabel?
    @IBOutlet weak var lblLocation : UILabel?
   
   
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
