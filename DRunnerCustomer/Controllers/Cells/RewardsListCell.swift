//
//  RewardsListCell.swift
//  DRunnerCustomer
//
//  Created by mac306 on 11/08/20.
//  Copyright © 2020 mac306. All rights reserved.
//

import UIKit

class RewardsListCell: UITableViewCell {

    @IBOutlet weak var lblPrice : UILabel?
    @IBOutlet weak var lblPoints : UILabel?
    @IBOutlet weak var btnRedeem : UIButton?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
