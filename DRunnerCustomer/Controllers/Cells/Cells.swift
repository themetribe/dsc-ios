//
//  Cells.swift
//  DRunnerCustomer
//
//  Created by mac306 on 27/07/20.
//  Copyright © 2020 mac306. All rights reserved.
//

import UIKit
import AARatingBar

class Cells: UITableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

class VehicalCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle : UILabel?
    @IBOutlet weak var lblPrice : UILabel?
    @IBOutlet weak var lblSize : UILabel?
    @IBOutlet weak var lblTime : UILabel?
    @IBOutlet weak var imgSelection : UIImageView?
    @IBOutlet weak var imgVehical : UIImageView?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}


class ServiceBookingCell: UITableViewCell {
    
    @IBOutlet weak var lblAddress : UILabel?
    @IBOutlet weak var lblWait : UILabel?
    @IBOutlet weak var lblTitle : UILabel?
    @IBOutlet weak var lblTxt : UILabel?
    @IBOutlet weak var imageLocation : UIImageView?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
class NotificationCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle : UILabel?
    @IBOutlet weak var lblSubTitle : UILabel?
    @IBOutlet weak var lblAgoDays : UILabel?
    @IBOutlet weak var lblDate : UILabel?
    
    @IBOutlet weak var imgMessage : UIImageView?
    @IBOutlet weak var imgDetail : UIImageView?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

class ProfileCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle : UILabel?
    @IBOutlet weak var lblTitleTxt : UILabel?
    @IBOutlet weak var btnEditProfile : UIButton?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

class CancelReuqestCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle : UILabel?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

class DeliveryLocationCell: UITableViewCell {
    
    @IBOutlet weak var btnLocation : UIButton?
    @IBOutlet weak var btnAddNew : UIButton?
    @IBOutlet weak var lblLocationName : UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

class DeliveryType: UITableViewCell {
    
    @IBOutlet weak var btnUrgentDelivery : UIButton?
    @IBOutlet weak var btnScheduleDelivery : UIButton?
    @IBOutlet weak var lblDeliveryTime : UILabel?
    @IBOutlet weak var btnSend : UIButton?
    @IBOutlet weak var btnViewPrice : UIButton?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}


class PromoCell: UITableViewCell {
    
    @IBOutlet weak var btnApply : TwoTagButton?
    @IBOutlet weak var lblPromoCode : UILabel?
    @IBOutlet weak var lblOfferAmount : UILabel?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

class RatingTopCell: UITableViewCell {
    
    @IBOutlet weak var imgRunner : UIImageView?
    @IBOutlet weak var viewRating : AARatingBar?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
class RatingMiddleCell: UITableViewCell {
    
    @IBOutlet weak var clTip : UICollectionView?
    @IBOutlet weak var txtAmount : TextField?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
class RatingBottomCell: UITableViewCell {
    
    @IBOutlet weak var clImprove : UICollectionView?
    @IBOutlet weak var txtComment : UITextView?
    @IBOutlet weak var btnSubmit : UIButton?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
class TipCollectioCell: UICollectionViewCell {
    
    @IBOutlet weak var lblTip : UILabel?
    
}

class RateCollectioCell: UICollectionViewCell {
    
    @IBOutlet weak var lblText : UILabel?
    @IBOutlet weak var imgRate : UIImageView?
}
