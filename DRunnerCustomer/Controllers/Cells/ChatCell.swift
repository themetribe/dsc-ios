//
//  ChatCell.swift
//  DRunnerCustomer
//
//  Created by mac306 on 07/08/20.
//  Copyright © 2020 mac306. All rights reserved.
//

import UIKit

class ChatCell: UITableViewCell {
    
    @IBOutlet weak var constraint_width: NSLayoutConstraint!
    @IBOutlet weak var imgChatBg: UIImageView!
    @IBOutlet weak var imgChat: UIImageView!
    @IBOutlet weak var lblTxt: UILabel!
    @IBOutlet weak var lblTxtActive: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var btnDownload: TwoTagButton!
    @IBOutlet weak var viewBlurred: UIView!
    @IBOutlet weak var indicator : TwoTagActivityIndicator!
    @IBOutlet weak var btnChat: TwoTagButton!
    @IBOutlet weak var imgTick: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func changeImage(_ name: String, backgroundColor : UIColor) {
        guard let image = UIImage(named: name) else { return }
        imgChatBg.image = image
            .resizableImage(withCapInsets:
                                UIEdgeInsets(top: 17, left: 21, bottom: 17, right: 21),
                            resizingMode: .stretch)
            .withRenderingMode(.alwaysTemplate)
        imgChatBg.tintColor = backgroundColor
    }
}
