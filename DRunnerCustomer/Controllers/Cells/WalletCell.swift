//
//  WalletCell.swift
//  DRunnerCustomer
//
//  Created by mac306 on 29/07/20.
//  Copyright © 2020 mac306. All rights reserved.
//

import UIKit

class WalletCell: UITableViewCell {

    @IBOutlet weak var lblBal : UILabel?
    @IBOutlet weak var lblOrder : UILabel?
    @IBOutlet weak var lblTotalBal : UILabel?
    @IBOutlet weak var lblDate : UILabel?
    @IBOutlet weak var lblTitle : UILabel?
    @IBOutlet weak var btnRecharge : UIButton?
    @IBOutlet weak var imgP: UIImageView?
     @IBOutlet weak var btnShowAll : UIButton?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
