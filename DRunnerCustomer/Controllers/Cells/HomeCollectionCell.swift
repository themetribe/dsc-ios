//
//  HomeCollectionCell.swift
//  DRunnerCustomer
//
//  Created by mac306 on 29/07/20.
//  Copyright © 2020 mac306. All rights reserved.
//

import UIKit
import AARatingBar

class HomeCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var lblDriverRank : UILabel?
    @IBOutlet weak var lblServiceName : UILabel?
    @IBOutlet weak var imgService : UIImageView?
    @IBOutlet weak var btnBook : UIButton?
    @IBOutlet weak var lblDescription : UILabel?
    @IBOutlet weak var viewRating : AARatingBar?
    
    @IBOutlet weak var lblCompletedTrips : UILabel?
    @IBOutlet weak var lblRating : UILabel?
    @IBOutlet weak var lblCancelledTrips : UILabel?
    @IBOutlet weak var imgUser : UIImageView?
    @IBOutlet weak var lblRunnerStatus : UILabel?
}
