//
//  RunnerCell.swift
//  DRunnerCustomer
//
//  Created by mac306 on 06/10/20.
//  Copyright © 2020 mac306. All rights reserved.
//

import UIKit

class RunnerCell: UITableViewCell {
    @IBOutlet weak var lblbRunnerName : UILabel?
    @IBOutlet weak var lblRunnerVehical : UILabel?
    @IBOutlet weak var btnFavUnFav : UIButton?
    @IBOutlet weak var imgRunner : UIImageView?
    @IBOutlet weak var lblRunnerStatus : UILabel?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
