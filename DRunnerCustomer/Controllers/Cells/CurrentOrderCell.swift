//
//  CurrentOrderCell.swift
//  DRunnerCustomer
//
//  Created by mac306 on 27/07/20.
//  Copyright © 2020 mac306. All rights reserved.
//

import UIKit

class CurrentOrderCell: UITableViewCell {

   @IBOutlet weak var imgRuneer : UIImageView?
    @IBOutlet weak var lblServiceName : UILabel?
    @IBOutlet weak var lblVehicalType : UILabel?
    @IBOutlet weak var lblPickUpAddress : UILabel?
    @IBOutlet weak var lblDropAddress : UILabel?
    @IBOutlet weak var lblJobDate : UILabel?
    @IBOutlet weak var btnStatus : UIButton?
    @IBOutlet weak var btnCancel : UIButton?
    @IBOutlet weak var btnTip : UIButton?
    @IBOutlet weak var lblPicUpName : UILabel?
    @IBOutlet weak var lblDropOffName : UILabel?
    
    }
