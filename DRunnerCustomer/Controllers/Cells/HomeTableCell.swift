//
//  HomeTableCell.swift
//  DRunnerCustomer
//
//  Created by mac306 on 29/07/20.
//  Copyright © 2020 mac306. All rights reserved.
//

import UIKit

class HomeTableCell: UITableViewCell {
    
    @IBOutlet weak var lblName : UILabel?
    
    @IBOutlet weak var imgUser : UIImageView?
    @IBOutlet weak var clView : UICollectionView?
    @IBOutlet weak var imgPoints : UIImageView?
    @IBOutlet weak var lblPoints : UILabel?
    @IBOutlet weak var lblWallet : UILabel?
    
    @IBOutlet weak var btnPoints : UIButton?
    @IBOutlet weak var btnWallet : UIButton?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
