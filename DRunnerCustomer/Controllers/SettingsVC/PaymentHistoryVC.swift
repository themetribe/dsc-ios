//
//  PaymentHistoryVC.swift
//  DRunnerCustomer
//
//  Created by mac306 on 11/08/20.
//  Copyright © 2020 mac306. All rights reserved.
//

import UIKit

class PaymentHistoryVC: UIViewController {

    @IBOutlet weak var tblData : UITableView?
    var arrWallet = Array<Any>()
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tblData?.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    @IBAction func onBackButton( _ sender : UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }

   

}
extension PaymentHistoryVC : UITableViewDelegate, UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
           return 1
       }
       func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           
        return self.arrWallet.count
           
       }


       func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
           return 60.0
       }
      
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell : WalletCell = tableView.dequeueReusableCell(withIdentifier: "WalletCell", for: indexPath) as! WalletCell
            let walletData = self.arrWallet[indexPath.row] as! Dictionary<String, Any>
            cell.lblTitle?.text = walletData["source"] as? String ?? ""
            //cell.lblDate?.text = walletData["created_at"] as? String ?? ""
            cell.lblDate?.text = Helper.shared.convertDateFormate(value : walletData["created_at"] as? String ?? "0")
            cell.lblBal?.text = "S$ \(walletData["amount"] as? String ?? "")"
            if walletData["type"] as? String ?? "" == "plus"
            {
                cell.lblBal?.textColor = UIColor.init(red: 36.0/255.0, green: 148.0/255.0, blue: 36.0/255.0, alpha: 1.0)
            }
            else
            {
                cell.lblBal?.textColor = .red
            }
                             return cell
       }
       
}
