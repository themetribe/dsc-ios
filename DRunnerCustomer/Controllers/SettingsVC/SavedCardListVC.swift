//
//  SavedCardListVC.swift
//  DRunnerCustomer
//
//  Created by mac306 on 17/09/20.
//  Copyright © 2020 mac306. All rights reserved.
//

import UIKit
import Stripe


class SavedCardCell : UITableViewCell
{
    @IBOutlet weak var lblCardExpM : UILabel?
    
    @IBOutlet weak var lblCardNumber : UILabel?
    @IBOutlet weak var btnSelectedCard : UIButton?
    @IBOutlet weak var btnDeleteCard : UIButton?
    
}

class SavedCardListVC: UIViewController, STPPaymentCardTextFieldDelegate {
    
    var arrSavedCard = Array<Any>()
    var money : Float = 0.0
    @IBOutlet weak var Stack_Card: UIStackView!
    lazy var paymentTextField: STPPaymentCardTextField = {
        let cardTextField = STPPaymentCardTextField()
        return cardTextField
    }()
    @IBOutlet weak var txt_Name: UITextField!
    @IBOutlet weak var tblData : UITableView?
    var paymentMethodIds = String()
    var paymentToken = String()
    @IBOutlet weak var btnSaveCard : UIButton?
    var saveCard = "no"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        paymentTextField = STPPaymentCardTextField()
        paymentTextField.delegate = self
        Stack_Card.axis = .horizontal
        Stack_Card.alignment = .fill // .leading .firstBaseline .center .trailing .lastBaseline
        Stack_Card.distribution = .fillEqually // .fillEqually .fillProportionally .equalSpacing .equalCentering
        Stack_Card.spacing = 0
        Stack_Card.addArrangedSubview(paymentTextField)
        
        self.tblData?.tableFooterView = UIView()
        
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.getSavedCardList()
    }
    
    @IBAction func onBackButton( _ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onSaveCard (_ sender : UIButton)
    {
        if saveCard == "no"
        {
            saveCard = "yes"
            self.btnSaveCard?.isSelected =  true
        }
        else
        {
            saveCard = "no"
            self.btnSaveCard?.isSelected =  false
        }
    }
    
    @IBAction func onPayment( _sender : UIButton)
    {
        
        self.onPayment()
        
    }
    
    @objc func onPayment()
    {
        if paymentTextField.cardParams.number?.trim().count ?? 0 == 0
        {
            self.showToast("Please enter card number")
            return
        }
        
        let cardParams: STPCardParams = STPCardParams()
        cardParams.number = paymentTextField.cardParams.number
        cardParams.expMonth = paymentTextField.cardParams.expMonth as! UInt
        cardParams.expYear = paymentTextField.cardParams.expYear as! UInt
        cardParams.cvc = paymentTextField.cardParams.cvc
        
        
        DispatchQueue.main.async {
            LOADER.show(views: self.view)
        }
        
        STPAPIClient.shared.createToken(withCard: cardParams) { token, error in
            guard let token = token else {
                print(error?.localizedDescription ?? "" as Any)
                self.showToast(error?.localizedDescription ?? "")
                LOADER.hide(delegate: self)
                return
            }
            let tokenID = token.tokenId
            
            print(tokenID)
            self.addInToWallet(tokenStr: token.tokenId, cardSave: self.saveCard, cardId: "")
        }
    }
    
    
    func addInToWallet(tokenStr : String, cardSave : String, cardId : String)
    {
        let param = ["user_id" : Helper.shared.getUserID(),
                     "stripe_token" : tokenStr,
                     "card_id" : cardId,
                     "amount" : String(format : "%.f",self.money),
                     "is_save": cardSave
        ] as [String : Any]
        
        API_HELPER.sharedInstance.postDataInJsonUsingNS(header: K.ApiName.AddPayment, withParameter: param as NSDictionary, inVC: self, showHud: false, addAuthHeader: true) { (result, message, status) in
            DispatchQueue.main.async {
                LOADER.hide(delegate: self)
                if status
                {
                    self.showSingleButtonAlert(title: kAppName, message: message) {
                        Helper.shared.setUserWallet(value: result["wallet_amount"] as? String ?? "0")
                        self.navigationController?.popViewController(animated: true)
                    }
                }
                else
                {
                    self.showToast(message)
                }
            }
        }
    }
    
    
    
    
    func getSavedCardList()
    {
        API_HELPER.sharedInstance.postDataInJsonUsingNS(header: K.ApiName.SavedCardList , withParameter: ["user_id" : Helper.shared.getUserID()], inVC: self, showHud: true, addAuthHeader: true) { (result, message, status) in
            DispatchQueue.main.async {
                if status
                {
                    self.arrSavedCard = result["data"] as! Array<Any>
                    self.tblData?.reloadData()
                }
            }
            
        }
    }
    
    @objc func paymentSavedCard(_ sender : UIButton)
    {
        sender.isSelected = true
        DispatchQueue.main.async {
            LOADER.show(views: self.view)
        }
        
        let cardDetail  = self.arrSavedCard[sender.tag] as! [String : Any]
        self.addInToWallet(tokenStr: "", cardSave: "no", cardId: cardDetail["card_id"] as? String ?? "")
    }
    
    @objc func onDeleteCard(_ sender : UIButton)
    {
        let cardDetail  = self.arrSavedCard[sender.tag] as! [String : Any]
        
        self.showDoubleButtonAlert(title: kAppName, message: "Are you sure you want to delete this card?", action1: "Yes", action2: "No", completion1: {
            API_HELPER.sharedInstance.postDataInJsonUsingNS(header: K.ApiName.RemoveCard, withParameter: ["customer_id": cardDetail["customer_id"] as? String ?? "" , "card_id": cardDetail["card_id"] as? String ?? ""], inVC: self, showHud: true, addAuthHeader: true) { (result, message, status) in
                DispatchQueue.main.async {
                    if status
                    {
                        self.getSavedCardList()
                    }
                    else
                    {
                        self.showToast(message)
                    }
                }
            }
        }) {
            
        }
    }
    
    
    func addCardViewControllerDidCancel(_ addCardViewController: STPAddCardViewController) {
        
        self.dismiss(animated: true) {
            
        }
    }
    
    func addCardViewController(_ addCardViewController: STPAddCardViewController, didCreateToken token: STPToken, completion: @escaping STPErrorBlock) {
        
        print("here")
        self.dismiss(animated: true) {
            
        }
    }
}

extension SavedCardListVC : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        return "Saved Cards"
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if self.arrSavedCard.count > 0
        {
            return 45.0
        }
        return 0.0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSavedCard.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : SavedCardCell = self.tblData?.dequeueReusableCell(withIdentifier: "SavedCardCell", for: indexPath) as!  SavedCardCell
        let cardDetail = self.arrSavedCard[indexPath.row] as! Dictionary<String,Any>
        
        cell.lblCardNumber?.text = "**** ***** **** \(cardDetail["last4"] as? String ?? "")"
        cell.lblCardExpM?.text = "\(cardDetail["exp_month"] as? String ?? "") / \(cardDetail["exp_year"] as? String ?? "")"
        // cell.lblCardExpM?.text = "\(cardDetail["exp_year"] as? String ?? "")"
        
        cell.btnDeleteCard?.tag = indexPath.row
        cell.btnDeleteCard?.addTarget(self, action: #selector(self.onDeleteCard(_:)), for: .touchUpInside)
        cell.btnSelectedCard?.tag = indexPath.row
        cell.btnSelectedCard?.addTarget(self, action: #selector(self.paymentSavedCard(_:)), for: .touchUpInside)
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cardDetail  = self.arrSavedCard[indexPath.row] as! [String : Any]
        
        self.addInToWallet(tokenStr: "", cardSave: "no", cardId: cardDetail["card_id"] as? String ?? "")
        //        let cardDetail = self.arrSavedCard[indexPath.row] as! Dictionary<String,Any>
        //
        //        let cardParams: STPCardParams = STPCardParams()
        //        cardParams.number = paymentTextField.cardParams.number
        //        cardParams.expMonth = paymentTextField.cardParams.expMonth as! UInt
        //        cardParams.expYear = paymentTextField.cardParams.expYear as! UInt
        //        cardParams.cvc = paymentTextField.cardParams.cvc
        //
        //        let cardParamss = paymentTextField.cardParams
        //
        //
        //
        //        DispatchQueue.main.async {
        //            LOADER.show(views: self.view)
        //        }
        //
        //        STPAPIClient.shared().createToken(withCard: cardParams) { token, error in
        //            guard let token = token else {
        //                return
        //            }
        //            let tokenID = token.tokenId
        //
        //            print(tokenID)
        //            self.addInToWallet(tokenStr: token.tokenId, cardSave: self.saveCard)
        //
        //
        //        }
    }
    
    
}
