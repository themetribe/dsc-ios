//
//  WalletVC.swift
//  DRunnerCustomer
//
//  Created by mac306 on 29/07/20.
//  Copyright © 2020 mac306. All rights reserved.
//

import UIKit

class WalletVC: UIViewController {

    var userData = defaults.object(forKey: udUserInfo) as! Dictionary<String,Any>
    @IBOutlet weak var tblData : UITableView?
    @IBOutlet weak var btnShowAll : UIButton?
    var arrWallet = Array<Any>()
        override func viewDidLoad() {
            super.viewDidLoad()
            self.tblData?.tableFooterView = UIView()
            self.btnShowAll?.isHidden = true
            // Do any additional setup after loading the view.
        }
        override func viewWillAppear(_ animated: Bool) {
            self.getWalletHistory()
        }
    @IBAction func onBackButton(_ sender : UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func onShowAllTransiction(_ sender : UIButton)
    {
        let paymentHistory = PAYMENT_SB.instantiateViewController(withIdentifier: "PaymentHistoryVC") as! PaymentHistoryVC
        paymentHistory.arrWallet = self.arrWallet
        self.navigationController?.pushViewController(paymentHistory, animated: true)
    }
    
    @IBAction func onTopUPWallet(_ sender : UIButton)
    {
        let profileVC = PAYMENT_SB.instantiateViewController(withIdentifier: "AddMoneyVC") as! AddMoneyVC
        self.navigationController?.pushViewController(profileVC, animated: true)
    }
    
    func getWalletHistory()
    {
        API_HELPER.sharedInstance.postDataInJsonUsingNS(header: K.ApiName.WalletHistory, withParameter: ["user_id" : Helper.shared.getUserID()], inVC: self, showHud: true, addAuthHeader: true) { (result, message, status) in
            DispatchQueue.main.async {
                if status
                {
                    self.arrWallet.removeAll()
                   
                    Helper.shared.setUserWallet(value: result["wallet_amount"] as? String ?? "0")
                    self.arrWallet = result["wallet_history"] as! Array
                    self.tblData?.reloadData()
                }
            }
        }
        
    }

}
extension WalletVC : UITableViewDelegate,UITableViewDataSource
    {
        func numberOfSections(in tableView: UITableView) -> Int {
            return 1
        }
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            if self.arrWallet.count == 0
            {
                return 1
            }
            else{
                if arrWallet.count > 5
                {
                    return 6
                }
                return arrWallet.count
            }
        }
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            if indexPath.row == 0{
                let cell : WalletCell = tableView.dequeueReusableCell(withIdentifier: "WalletCellTop", for: indexPath) as! WalletCell
                cell.btnShowAll?.addTarget(self, action: #selector(self.onShowAllTransiction(_:)), for: .touchUpInside)
                 self.arrWallet.count > 5 ? ( cell.btnShowAll?.isHidden = false) : ( cell.btnShowAll?.isHidden = true)
                cell.lblTotalBal?.text = Helper.shared.getUserWallet()
                return cell
            }
            else{
                let cell : WalletCell = tableView.dequeueReusableCell(withIdentifier: "WalletCell", for: indexPath) as! WalletCell
                
                let walletData = self.arrWallet[indexPath.row - 1] as! Dictionary<String, Any>
                cell.lblTitle?.text = walletData["source"] as? String ?? ""
                
                //Helper.shared.convertDateFormate(value : walletData["created_at"] as? String ?? "0")
                cell.lblDate?.text = Helper.shared.convertDateFormate(value : walletData["created_at"] as? String ?? "0")
                    //Helper.shared.convertDateFormate(value : requestDetail["schedule_date_time"] as? String ?? "0")
                //Helper.shared.getTimeFromDateTimeStamp(timeSt: walletData["created_at"] as? String ?? "")
                cell.lblBal?.text = "S$ \(walletData["amount"] as? String ?? "")"
                if walletData["type"] as? String ?? "" == "plus"
                {
                    cell.lblBal?.textColor = UIColor.init(red: 36.0/255.0, green: 148.0/255.0, blue: 36.0/255.0, alpha: 1.0)
                }
                else
                {
                    cell.lblBal?.textColor = .red
                }
                    return cell
            }
        }
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            if indexPath.row == 0
            {
                return 285.0
            }
            else
            {
                return 60.0
            }
        }
    }
