//
//  ManageSettings.swift
//  DRunnerCustomer
//
//  Created by mac306 on 11/08/20.
//  Copyright © 2020 mac306. All rights reserved.
//

import UIKit

class ManageSettings: UIViewController {
    
    @IBOutlet weak var lblVersionNo : UILabel?
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
           self.navigationController?.navigationBar.isHidden = false
       }
    
    @IBAction func onBackButton( _ sender : UIButton)
    {
           self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onUpdateButton( _ sender : UIButton)
    {
              
    }
    
   @IBAction func onSignOutButton( _ sender : UIButton)
   {
    
    self.showDoubleButtonAlert(title: kAppName, message: "Do you want to logout?", action1: "Yes", action2: "No", completion1: {
        
        let param = [  "user_id" : Helper.shared.getUserID()
                                  ]
              API_HELPER.sharedInstance.postDataInJsonUsingNS(header: K.ApiName.LogOut, withParameter: param as NSDictionary, inVC: self, showHud: true, addAuthHeader: true) { (result, message, status) in
                        DispatchQueue.main.async {
                           if status
                           {
                                Helper.shared.setUserID(value: "0")
                                Helper.shared.setAuthToken(value: "")
                                APP_DELEGATE.setViewControllers()
                            
                            }
                           else{
                            self.showToast(message)
                            }
                                        
                        }
              }
        
        
    }) {
        
    }
             
   }
    @IBAction func onTermOfService( _ sender : UIButton)
    {
                let profileVC = STATIC_SB.instantiateViewController(withIdentifier: "StaticPagesVC") as! StaticPagesVC
                          profileVC.urlString = TS
                          self.navigationController?.pushViewController(profileVC, animated: true)
    }

}
