//
//  AddMoneyVC.swift
//  DRunnerCustomer
//
//  Created by mac306 on 10/09/20.
//  Copyright © 2020 mac306. All rights reserved.
//

import UIKit
import Stripe

class AddMoneyVC: UIViewController {
    
    func addCardViewControllerDidCancel(_ addCardViewController: STPAddCardViewController) {
        
        self.dismiss(animated: true) {
            
        }
    }
    
    func addCardViewController(_ addCardViewController: STPAddCardViewController, didCreateToken token: STPToken, completion: @escaping STPErrorBlock) {
        
        print("here")
        self.dismiss(animated: true) {
            
        }
    }
    
    
    
    @IBOutlet weak var lblBalance : UILabel?
    @IBOutlet weak var lblCurrency : UILabel?
    @IBOutlet weak var txtAmount : UITextField?
    lazy var cardTextField: STPPaymentCardTextField = {
        let cardTextField = STPPaymentCardTextField()
        return cardTextField
    }()
    lazy var payButton: UIButton = {
        let button = UIButton(type: .custom)
        button.layer.cornerRadius = 5
        button.backgroundColor = .systemBlue
        button.titleLabel?.font = UIFont.systemFont(ofSize: 22)
        button.setTitle("Pay", for: .normal)
        button.addTarget(self, action: #selector(pay), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        //        let stackView = UIStackView(arrangedSubviews: [cardTextField, payButton])
        //        stackView.axis = .vertical
        //        stackView.spacing = 20
        //        stackView.translatesAutoresizingMaskIntoConstraints = false
        //        view.addSubview(stackView)
        //        NSLayoutConstraint.activate([
        //            stackView.leftAnchor.constraint(equalToSystemSpacingAfter: view.leftAnchor, multiplier: 2),
        //            view.rightAnchor.constraint(equalToSystemSpacingAfter: stackView.rightAnchor, multiplier: 2),
        //            stackView.topAnchor.constraint(equalToSystemSpacingBelow: view.topAnchor, multiplier: 2),
        //        ])
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.lblBalance?.text = Helper.shared.getUserWallet()
    }
    
    @IBAction func onBackButton( _ sender : UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func pay() {
        // Create an STPCardParams instance
        let cardParams = STPCardParams()
        
        cardParams.number = cardTextField.cardParams.number
        cardParams.expMonth = cardTextField.cardParams.expMonth as! UInt
        cardParams.expYear = cardTextField.cardParams.expYear as! UInt
        cardParams.cvc = cardTextField.cardParams.cvc
        let cardParamss = cardTextField.cardParams
        let paymentMethodParams = STPPaymentMethodParams(card: cardParamss, billingDetails: nil, metadata: nil)
        
        STPAPIClient.shared.createPaymentMethod(with: paymentMethodParams) { (paymentMethod, error) in
            guard let paymentMethod = paymentMethod else {
                // Display the error to the user
                //ProgressHide()
                return
            }
            print(paymentMethod.stripeId)
        }
        
        STPAPIClient.shared.createToken(withCard: cardParams) { token, error in
            guard let token = token else {
                // Handle the error
                return
            }
            let tokenID = token.tokenId
            print(tokenID)
           
        }
        

    }
    
    
    @IBAction func onFirstRecomnded(_ sender : UIButton)
    {
        self.txtAmount?.text = "20"
        
    }
    
    @IBAction func onSecondRecomnded(_ sender : UIButton)
    {
        self.txtAmount?.text = "30"
    }
    
    @IBAction func onThirdRecomnded(_ sender : UIButton)
    {
        self.txtAmount?.text = "50"
    }
    
    @IBAction func onContinueButton(_ sender : UIButton)
    {
        if !((self.txtAmount?.text!.count)! > 0)
        {
            self.showToast("Please enter amount.")
            return
        }
        let amount = Float(self.txtAmount?.text ?? "0.00")
        
        if amount! < 20.0
        {
            self.showToast("Add minimum S$20.")
            return
        }
        
        let savedCardList = PAYMENT_SB.instantiateViewController(withIdentifier: "SavedCardListVC") as! SavedCardListVC
        savedCardList.money = amount ?? 0.00
        self.navigationController?.pushViewController(savedCardList, animated: true)
        
    }
    
}
