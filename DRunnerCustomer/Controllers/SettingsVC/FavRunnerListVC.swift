//
//  FavRunnerListVC.swift
//  DRunnerCustomer
//
//  Created by mac306 on 06/10/20.
//  Copyright © 2020 mac306. All rights reserved.
//

import UIKit

class FavRunnerListVC: UIViewController, UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tblList : UITableView?
    var arrList = Array<Any>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Favourite Runner"
        self.tblList?.tableFooterView = UIView()
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.getRunnerList()
    }
    
    @IBAction func onBackButton( _ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    func getRunnerList()
    {
        API_HELPER.sharedInstance.postDataInJsonUsingNS(header: K.ApiName.FavRunner , withParameter: ["user_id" : Helper.shared.getUserID()], inVC: self, showHud: true, addAuthHeader: true) { (result, message, status) in
                   DispatchQueue.main.async {
                       if status
                       {
                           self.arrList = result["data"] as! Array<Any>
                            if self.arrList.count > 0
                            {
                                self.tblList?.isHidden = false
                                self.tblList?.reloadData()
                            }
                        else
                            {
                             self.tblList?.isHidden = true
                        }
                       }
                   }
                   
               }
    }
    
    @objc func onUnFavIcon(_ sender : UIButton)
    {
        let runnerDetail = self.arrList[sender.tag] as! Dictionary<String,Any>
        
        API_HELPER.sharedInstance.postDataInJsonUsingNS(header: K.ApiName.FavUnFave , withParameter: ["user_id" : Helper.shared.getUserID(),"runner_id" : runnerDetail["id"] as? String ?? "", "is_favourite" : "no"], inVC: self, showHud: true, addAuthHeader: true) { (result, message, status) in
            DispatchQueue.main.async {
                if status
                {
                    self.showToast(message)
                    self.arrList.remove(at: sender.tag)
                    if self.arrList.count > 0
                    {
                        self.tblList?.isHidden = false
                        self.tblList?.reloadData()
                    }
                    else
                    {
                        self.tblList?.isHidden = true
                    }
                    
                }
                else
                {
                    self.showToast(message)
                }
            }
            
        }
    }
    

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell : RunnerCell = self.tblList?.dequeueReusableCell(withIdentifier: "RunnerCell", for: indexPath) as! RunnerCell
        cell.selectionStyle = .none
        let runnerDetail = self.arrList[indexPath.row] as! Dictionary<String,Any>
        cell.lblbRunnerName?.text = runnerDetail["name"] as? String ?? ""
        cell.lblRunnerVehical?.text = "DStar - \(runnerDetail["transport_category"] as? String ?? "")"
        cell.btnFavUnFav?.tag = indexPath.row
        cell.btnFavUnFav?.addTarget(self, action: #selector(self.onUnFavIcon(_:)), for: .touchUpInside)
        if runnerDetail["image"] as? String ?? "" != ""
        {
            cell.imgRunner?.loadImageUsingCache(withUrl: runnerDetail["image"] as? String ?? "")
        }
        
        if runnerDetail["view_status"] as? String ?? "" == "online"
        {
            cell.lblRunnerStatus?.backgroundColor = .green
        }
        else
        {
             cell.lblRunnerStatus?.backgroundColor = .red
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let runnerData = self.arrList[indexPath.row] as! Dictionary<String,Any>
        if runnerData["view_status"] as? String ?? "" == "online"
        {
                     
            let alertController = UIAlertController(title: kAppName, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
                 
            alertController.addAction(UIAlertAction(title: "Help Me Buy", style: .default, handler: {(alert: UIAlertAction!) in
            
                self.bookJob(runnerId: runnerData["id"] as? String ?? "" , serviceType: 0)
            }))
            alertController.addAction(UIAlertAction(title: "Help Me Send", style: .default, handler: {(alert: UIAlertAction!) in
                         self.bookJob(runnerId: runnerData["id"] as? String ?? "" , serviceType: 1)
            }))
            alertController.addAction(UIAlertAction(title: "Help Me Queue", style: .default, handler: {(alert: UIAlertAction!) in
                           let profileVC = BOOKTASK_SB.instantiateViewController(withIdentifier: "HMQBookingVC") as! HMQBookingVC
                           profileVC.runner_id = runnerData["id"] as? String ?? ""
                           self.navigationController?.pushViewController(profileVC, animated: true)
                       }))
                       alertController.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: {(alert: UIAlertAction!) in
                                    
                              }))
                       DispatchQueue.main.async {
                              self.present(alertController, animated: true, completion:{})
                       }
                   }
                   else
                   {
                       self.showToast("Runner is not available right now. Please try again latter.")
                   }
    }
    func bookJob(runnerId : String , serviceType : Int)
    {
        let profileVC = BOOKTASK_SB.instantiateViewController(withIdentifier: "ServiceBookingVC") as! ServiceBookingVC
        profileVC.serviceType = serviceType
        profileVC.runner_id = runnerId
        self.navigationController?.pushViewController(profileVC, animated: true)
    }
}
