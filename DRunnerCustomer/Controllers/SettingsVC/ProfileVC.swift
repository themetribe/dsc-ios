//
//  ProfileVC.swift
//  DRunnerCustomer
//
//  Created by mac306 on 29/07/20.
//  Copyright © 2020 mac306. All rights reserved.
//

import UIKit

class ProfileVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var imgProfile : UIImageView?
    @IBOutlet weak var imgPoints : UIImageView?
    @IBOutlet weak var lblUserName : UILabel?
    @IBOutlet weak var lblUniqueId : UILabel?
    @IBOutlet weak var lblPoint : UILabel?
    @IBOutlet weak var lblPointStatus : UILabel?
    @IBOutlet weak var txtUserName : UITextField?
    @IBOutlet weak var btnEdit : UIButton?
    @IBOutlet weak var txtMobileNumber : UITextField?
    @IBOutlet weak var txtEmail : UITextField?
    @IBOutlet weak var btnCountryCode : UIButton?
    @IBOutlet weak var btnLogout : UIButton?
    @IBOutlet weak var btnPoints : UIButton?
    @IBOutlet weak var imgCamera : UIImageView?
    let imagePicker = UIImagePickerController()
    var isImageEdit : Bool = false
    
    var isProfileEdititng : Bool = false
    var userData = defaults.object(forKey: udUserInfo) as! Dictionary<String,Any>
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imagePicker.delegate = self
        self.txtUserName?.isUserInteractionEnabled = false
        self.txtEmail?.isUserInteractionEnabled = false
        self.initData()
        //self.getUserProfile()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = false
        
//        Helper.shared.getUserProfile(vc: self, showHud: true) { (status) in
//            self.initData()
//        }
        if self.isImageEdit == false
        {
            self.getUserProfile()
        }
        
    }
    
    func initData()
    {
        DispatchQueue.main.async {
            self.userData = defaults.object(forKey: udUserInfo) as! Dictionary<String,Any>
            self.txtUserName?.text = "\(self.userData["first_name"] as? String ?? "") \(self.userData["last_name"] as? String ?? "")"
            self.lblUserName?.text = "\(self.userData["first_name"] as? String ?? "") \(self.userData["last_name"] as? String ?? "")"
            
            self.lblUniqueId?.text = "\("UID: ") \(self.userData["user_id"] as? String ?? "")" 
            if self.userData["profile_pic"] as? String ?? "" != ""
            {
                self.imgProfile?.loadImageUsingCache(withUrl: self.userData["profile_pic"] as? String ?? "")
            }
            self.txtEmail?.text = self.userData["email"] as? String ?? ""
            self.txtMobileNumber?.text = self.userData["mobile"] as? String ?? ""
            self.btnCountryCode?.setTitle(self.userData["country_code"] as? String ?? "", for: .normal)
            self.lblPoint?.text = "\(self.userData["reward_points"] as? String ?? "" ) Points"
            self.imgPoints?.image = Helper.shared.getRankingImage(userRank: self.userData["ranking"] as? String ?? "")
            self.isProfileEdititng = false
            self.txtUserName?.isUserInteractionEnabled = false
            self.txtEmail?.isUserInteractionEnabled = false
            self.btnEdit?.setTitle("Edit", for: .normal)
            self.imgCamera?.alpha = 0.0
            self.btnPoints?.setTitle(self.userData["ranking"] as? String ?? "", for: .normal)
            
        }
    }
    
    
    func getUserProfile()
    {
        API_HELPER.sharedInstance.postDataInJsonUsingNS(header: K.ApiName.Profile, withParameter: ["user_id": Helper.shared.getUserID()], inVC: self, showHud: true, addAuthHeader: true) { (resultData, message, status) in
            DispatchQueue.main.async {
                if !status
                {
                    Helper.shared.showAlert(msg: message)
               }
                else{
                    let dict : NSMutableDictionary = APP_DELEGATE.convertAllDictionaryValueToNil(resultData.mutableCopy() as! NSMutableDictionary) as! NSMutableDictionary
                    defaults.set(dict, forKey: udUserInfo)
                    self.initData()
                }
            }
        }
    }
    
    
    @IBAction func onBackButton(_ sender : UIButton)
    {
        DispatchQueue.main.async {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func onEditButton(_ sender : UIButton)
    {
        if isProfileEdititng == false
        {
            self.isProfileEdititng = true
            self.txtUserName?.isUserInteractionEnabled = true
            self.txtEmail?.isUserInteractionEnabled = true
            self.txtUserName?.resignFirstResponder()
            self.btnEdit?.setTitle("Update", for: .normal)
            self.imgCamera?.alpha = 1.0
        }
        else
        {
            DispatchQueue.main.async {
                self.view.endEditing(true)
            }
            self.updateUserProfile()
        }
    }
    
    
    
    @IBAction func onMobileNumberChange(_ sender : UIButton)
    {
        DispatchQueue.main.async {
        let otpVC = MAIN_SB.instantiateViewController(withIdentifier: "AddMobileNumberVC") as! AddMobileNumberVC
        otpVC.isMobileNumberChange = true
        self.navigationController?.pushViewController(otpVC, animated: true)
        }
    }
    @IBAction func onPointStatus(_ sender : UIButton)
    {
        DispatchQueue.main.async {
        let profileVC = POINTS_SB.instantiateViewController(withIdentifier: "RewardsVC") as! RewardsVC
        self.navigationController?.pushViewController(profileVC, animated: true)
        }
    }
    
    @IBAction func onLogout(_ sender : UIButton)
    {
        self.showDoubleButtonAlert(title: kAppName, message: "Do you want to logout?", action1: "Yes", action2: "No", completion1: {
            
            let param = [  "user_id" : Helper.shared.getUserID()
            ]
            API_HELPER.sharedInstance.postDataInJsonUsingNS(header: K.ApiName.LogOut, withParameter: param as NSDictionary, inVC: self, showHud: true, addAuthHeader: true) { (result, message, status) in
                DispatchQueue.main.async {
                    if status
                    {
                        Helper.shared.setUserID(value: "0")
                        Helper.shared.setAuthToken(value: "")
                        APP_DELEGATE.setViewControllers()
                        
                    }
                    else{
                        self.showToast(message)
                    }
                }
            }
        })
        {}
    }
    
    @IBAction func onImageEditing(_ sender : UIButton)
    {
        if self.isProfileEdititng == false
        {
            return
        }
        
        imagePicker.allowsEditing = true
        self.showActionSheetWith(items: ["Camera","Photo Album"], title: kAppName, message: "") { (type) in
            if type == "Camera"
            {
                self.imagePicker.sourceType = UIImagePickerController.SourceType.camera
            }
            else {
                self.imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            }
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        
    }
    
    func updateUserProfile()
    {
        if self.txtUserName?.text?.trim().count == 0 {
            self.showToast(AppMessages.FNAME_BLANK)
            return
        }
        if (self.txtUserName?.text?.trim().count)! < 2   || (self.txtUserName?.text?.trim().count)! > 80 {
            self.showToast(AppMessages.FNAME_VALID)
            return
        }
        
        if self.txtEmail?.text?.trim().count == 0 {
            self.showToast(AppMessages.EMAIL_BLANK)
            return
        }
        if self.txtEmail?.text?.trim().isValidEmailId == false {
            self.showToast(AppMessages.EMAIL_VALID)
            return
        }
        
        
        let trimmedString = self.txtUserName?.text?.trimmingCharacters(in: .whitespaces)
        let fullNameArr = trimmedString?.components(separatedBy: " ")
        var fname    = fullNameArr?[0]
        var lName = ""
        if (fullNameArr?.count)! > 1
        {
            fname = trimmedString!
            lName = fullNameArr?.last ?? ""
            fname = fname?.replacingOccurrences(of: lName, with: "")
        }
        
        var countryCode = self.btnCountryCode?.titleLabel?.text
        countryCode = countryCode?.replacingOccurrences(of: "+", with: "")
        
        let param = [       "user_id": Helper.shared.getUserID(),
                            "first_name" : fname,
                            "last_name" : lName,
                            "email" : self.txtEmail?.text ?? "",
        ] as [String : Any]
        API_HELPER.sharedInstance.editUserProfile(parameter: param as NSDictionary, imageArray: [self.imgProfile!.image], inVC: self, showHud: true) { (status) in
            self.isImageEdit = false
            self.showToast("Profile update successfully")
            if status
            {
               self.initData()
            }
        }
        
    }
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        
        let image = info[UIImagePickerController.InfoKey.editedImage]! as! UIImage
        self.isImageEdit = true
        self.imgProfile?.image = image
        picker.dismiss(animated: true, completion: {
            self.updateUserProfile()
        }
        )
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.isImageEdit = false
        picker.dismiss(animated: true, completion: nil)
    }
    
}
extension ProfileVC : UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //For mobile numer validation
        if textField == self.txtMobileNumber {
            let allowedCharacters = CharacterSet(charactersIn:"0123456789")//Here change this characters based on your requirement
            let characterSet = CharacterSet(charactersIn: string)
            return allowedCharacters.isSuperset(of: characterSet)
        }
        if textField == self.txtUserName {
            
            
        }
        
        return true
    }
}
