//
//  PointsVC.swift
//  DRunnerCustomer
//
//  Created by mac306 on 11/08/20.
//  Copyright © 2020 mac306. All rights reserved.
//

import UIKit

class PointsVC: UIViewController {
    
    @IBOutlet weak var lblPointsTitle : UILabel?
    @IBOutlet weak var imgUserRewards : UIImageView?
    @IBOutlet weak var lblPoints : UILabel?
    @IBOutlet weak var imgPoints : UIImageView?
    @IBOutlet weak var lblPointsRemain : UILabel?
    @IBOutlet weak var pointsProgress : UIProgressView?
    var userData = defaults.object(forKey: udUserInfo) as! Dictionary<String,Any>
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // Do any additional setup after loading the view.
    }
    //    override func viewWillAppear(_ animated: Bool) {
    //        self.tabbar?.navigationBar.isHidden = true
    //    }
    //    override func viewWillDisappear(_ animated: Bool) {
    //        self.navigationController?.navigationBar.isHidden = false
    //    }
    override func viewWillAppear(_ animated: Bool) {
        DispatchQueue.main.async {
            self.lblPoints?.text = "\(Helper.shared.getUserPoints()) Points"
            self.lblPointsTitle?.text = "\(self.userData["ranking"] as? String ?? "")"
            self.imgPoints?.image = Helper.shared.getRankingImage(userRank: self.userData["ranking"] as? String ?? "")
        }
    }
    
    @IBAction func onBackButton( _ sender : UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onViewHistoryButton( _ sender : UIButton)
    {
        let profileVC = POINTS_SB.instantiateViewController(withIdentifier: "PointsHistoryVC") as! PointsHistoryVC
        self.navigationController?.pushViewController(profileVC, animated: true)
    }
    
    @IBAction func onRedeemPoints( _ sender : UIButton)
    {
        let profileVC = POINTS_SB.instantiateViewController(withIdentifier: "RewardsVC") as! RewardsVC
        self.navigationController?.pushViewController(profileVC, animated: true)
    }
}
