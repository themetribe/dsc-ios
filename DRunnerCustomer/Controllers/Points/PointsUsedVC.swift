//
//  PointsUsedVC.swift
//  DRunnerCustomer
//
//  Created by mac306 on 11/08/20.
//  Copyright © 2020 mac306. All rights reserved.
//

import UIKit

class PointsUsedVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tblData : UITableView?
    var usedDataK = Array<Any>()
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tblData?.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.getRewardsHistory()
    }
    
    func getRewardsHistory()
    {
        API_HELPER.sharedInstance.postDataInJsonUsingNS(header: K.ApiName.RewardHistory, withParameter: ["user_id" : Helper.shared.getUserID()], inVC: self, showHud: true, addAuthHeader: true) { (result, message, status) in
            
            DispatchQueue.main.async {
                
                self.usedDataK = result["used_data"] as! Array
               
                self.tblData?.reloadData()
                if self.usedDataK.count > 0
                {
                    self.tblData?.alpha = 1.0
                }
                else
                {
                    self.tblData?.alpha = 0.0
                }
            }
        }
    }

 func numberOfSections(in tableView: UITableView) -> Int {
        return self.usedDataK.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let receiveData  = self.usedDataK[section] as! [String : Any]
        return (receiveData["used_points"] as! Array<Any>).count
        
    }


    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90.0
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44.0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableCell(withIdentifier: "PointHistorySectionCell") as! PointHistorySectionCell
         let receiveData  = self.usedDataK[section] as! [String : Any]
        headerView.lblTitle?.text = receiveData["date"] as? String ?? ""
        return headerView.contentView
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : PointHistoryCell = self.tblData?.dequeueReusableCell(withIdentifier: "PointHistoryCell", for: indexPath) as! PointHistoryCell
        let receiveData  = self.usedDataK[indexPath.section] as! [String : Any]
        let arrayD = receiveData["used_points"] as! Array<Any>
        let dataPoints = arrayD[indexPath.row] as! [String : Any]
        cell.lblTitle?.text = dataPoints["source"] as? String ?? ""
        cell.lblTime?.text = dataPoints["updated_at"] as? String ?? ""
        cell.lblPoints?.text = "\(dataPoints["points"] as? String ?? "") Points"
        return cell
    }
    
    

}
