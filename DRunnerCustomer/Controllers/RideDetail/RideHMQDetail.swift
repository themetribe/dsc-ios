//
//  RideHMQDetail.swift
//  DRunnerCustomer
//
//  Created by mac306 on 29/09/20.
//  Copyright © 2020 mac306. All rights reserved.
//

import UIKit
import ChatSDK
import ChatProvidersSDK
import MessagingSDK

class RideHMQDetail: UIViewController {
    
    @IBOutlet weak var tblList : UITableView?
    
    var request_id : String = ""
    var isDataGet : Bool = false
    var arrPickUp : Array = [Dictionary<String,Any>()]
    var requestData : Dictionary = Dictionary<String,Any>()
    @IBOutlet weak var btnCancel : UIButton?
    
    var userData = defaults.object(forKey: udUserInfo) as! Dictionary<String,Any>
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblList?.tableFooterView = UIView()
        self.title = "Help Me Queue"
        self.btnCancel?.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.onRequestDetail()
    }
    
    @IBAction func onBackButton( _ sender : UIButton)
    {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func onRequestDetail()
    {
        API_HELPER.sharedInstance.postDataInJsonUsingNS(header: K.ApiName.RequestDetail, withParameter: ["user_id" : Helper.shared.getUserID(), "request_id" : request_id], inVC: self, showHud: true, addAuthHeader: true) { (result, message, status) in
            DispatchQueue.main.async {
                if status
                {
                    self.isDataGet = true
                    self.requestData = result as! [String : Any]
                    self.arrPickUp.removeAll()
                    self.arrPickUp.append(result["pickup_info"] as! [String : Any])
                    let requestDetail = self.requestData["runner_detail"] as! [String : Any]
                    if self.requestData["status"] as? String ?? "" != "Cancelled"
                    {
                        if requestDetail["runner_id"] == nil{
                            
                            self.btnCancel?.isHidden = true
                        }
                        else{
                            self.btnCancel?.isHidden = false
                            self.btnCancel?.setTitle("Help", for: .normal)
                        }
                    }
                    else
                    {
                        self.btnCancel?.isHidden = true
                    }
                    if kNotificationObject["notification_type"] as? String == "chat" {
                        self.onChatButton(UIButton())
                    }
                    kNotificationObject = NSDictionary()
                    
                    self.tblList?.reloadData()
                }
                kNotificationObject = NSDictionary()
            }
        }
    }
    
    
    
    @objc func onLikeRunner(_ sender : UIButton)
    {
        // let requestDetail = self.arrFutureList[sender.tag]
        var fav = "no"
        var requestDetail = self.requestData["runner_detail"] as! [String : Any]
        if requestDetail["is_favourite"] as? String ?? "0" == "0"
        {
            fav = "yes"
        }
        
        API_HELPER.sharedInstance.postDataInJsonUsingNS(header: K.ApiName.FavUnFave , withParameter: ["user_id" : Helper.shared.getUserID(),"runner_id" : requestDetail["runner_id"] as? String ?? "", "is_favourite" : fav], inVC: self, showHud: true, addAuthHeader: true) { (result, message, status) in
            DispatchQueue.main.async {
                if status
                {
                    if fav == "yes"
                    {
                        requestDetail["is_favourite"]  = "1"
                    }
                    else
                    {
                        requestDetail["is_favourite"]  = "0"
                    }
                    self.requestData["runner_detail"] = requestDetail
                    self.tblList?.reloadRows(at: [IndexPath.init(row: 0, section: 0)], with: .automatic)
                }
                else
                {
                    self.showToast(message)
                }
            }
            
        }
    }
    
    @IBAction func onCancelButton(_ sender : UIButton)
    {
        let requestDetail = self.requestData["runner_detail"] as! [String : Any]
        if requestDetail["runner_id"] == nil{
            let waitingVC = BOOKTASK_SB.instantiateViewController(identifier: "CancelRequestVC") as! CancelRequestVC
            waitingVC.request_id = self.request_id
            self.navigationController?.pushViewController(waitingVC, animated: true)
        }
        else
        {
            do {
                let chatEngine = try ChatEngine.engine()
                let viewController = try Messaging.instance.buildUI(engines: [chatEngine], configs: [])
                viewController.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(viewController, animated: true)
            } catch {
                // handle error
            }
        }
    }
    @objc func onTrackButton (_ sender : UIButton)
    {
        let profileVC = RATING_SB.instantiateViewController(withIdentifier: "DriverLocationVC") as! DriverLocationVC
        profileVC.requestData = self.requestData
        self.navigationController?.pushViewController(profileVC, animated: true)
    }
    
    
    @objc func onCallButton( _ sender : UIButton)
    {
        if self.requestData["status"] as? String ?? "" != "Completed"
        {
            let requestDetail = self.requestData["runner_detail"] as! [String : Any]
          /*  if requestDetail["runner_id"] != nil{
                let mobile = requestDetail["runner_mobile"] as? String ?? ""
                
                if let url = URL(string: "tel://\(mobile)") {
                    UIApplication.shared.openURL(url)
                }
            }*/
            
            let param = ["user_id": Helper.shared.getUserID(),
                         "sender_no": "+65\(self.userData["mobile"] as? String ?? "")",
                         "receiver_no": "+65\(requestDetail["runner_mobile"] as? String ?? "")",
                         "receiver_name": requestDetail["runner_name"] as? String ?? "",
                         "sender_name": "\(self.userData["first_name"] as? String ?? "") \(self.userData["last_name"] as? String ?? "")"]
            
            API_HELPER.sharedInstance.postDataInJsonUsingNS(header: K.ApiName.twillioToken , withParameter: param as NSDictionary, inVC: self, showHud: true, addAuthHeader: true) { (result, message, status) in
                DispatchQueue.main.async {
                    if status {
                        
                        let mobile = result["receiver_no"] as? String ?? ""
                        if let url = URL(string: "tel://\(mobile)") {
                            if #available(iOS 10.0, *) {
                                UIApplication.shared.open(url, options: [:], completionHandler: nil)
                            } else {
                                UIApplication.shared.openURL(url)
                            }
                        }
                    }
                    else {
                        self.showToast(message)
                    }
                }
            }
            
        }
    }
    @objc func onChatButton (_ sender : UIButton)
    {
        let profileVC = CHAT_SB.instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
        profileVC.requestDetail = self.requestData
        self.navigationController?.pushViewController(profileVC, animated: true)
    }
}

extension RideHMQDetail : UITableViewDataSource,UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.isDataGet
        {
            return 3
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0
        {
            let cell : JobRunnerDetailCell = tableView.dequeueReusableCell(withIdentifier: "JobRunnerDetailCell", for: indexPath) as! JobRunnerDetailCell
            cell.selectionStyle = .none
            cell.btnLike?.addTarget(self, action: #selector(self.onLikeRunner(_:)), for: .touchUpInside)
            cell.btnTrack?.addTarget(self, action: #selector(self.onTrackButton(_:)), for: .touchUpInside)
            cell.btnCall?.addTarget(self, action: #selector(self.onCallButton(_:)), for: .touchUpInside)
            cell.btnChat?.addTarget(self, action: #selector(self.onChatButton(_:)), for: .touchUpInside)
            let requestDetail = self.requestData["runner_detail"] as! [String : Any]
            if requestDetail["runner_id"] != nil{
                cell.viewProcessing?.isHidden = true
                cell.btnTrack?.isHidden = false
                cell.lblNoAssignRunner?.isHidden = true
                cell.btnCall?.isUserInteractionEnabled = true
                cell.btnChat?.isUserInteractionEnabled = true
                ///cell.lblVehicalType?.text = requestDetail["runner_vehicle"] as? String ?? ""
                cell.lblRunner?.text = requestDetail["runner_name"] as? String ?? ""
                if  requestDetail["runner_image"] as? String ?? "" != ""
                {
                    cell.imgRunner?.loadImageUsingCache(withUrl: requestDetail["runner_image"] as? String ?? "")
                }
                if requestDetail["is_favourite"] as? String ?? "0" == "1"
                {
                    cell.btnLike?.setImage(UIImage(named: "fav"), for: .normal)
                }
                else
                {
                    cell.btnLike?.setImage(UIImage(named: "unfav"), for: .normal)
                }
                if self.requestData["status"] as? String == "Accepted" || self.requestData["status"] as? String == "Processing"
                {
                    cell.btnTrack?.isHidden = false
                }
                else
                {
                    cell.btnTrack?.isHidden = true
                }
                
            }
            else
            {
                cell.viewProcessing?.isHidden = self.requestData["status"] as? String == "Cancelled" ? true : false
                cell.btnTrack?.isHidden = true
                cell.lblNoAssignRunner?.isHidden = false
                cell.btnCall?.isUserInteractionEnabled = false
                cell.btnChat?.isUserInteractionEnabled = false
                cell.lblRunner?.text = "-"
                cell.lblVehicalType?.text = "-"
                
            }
            cell.lblVehicalType?.text = "Booking Id: \(self.requestData["booking_id"] as? String ?? "")"
            
            return cell
        }
        else  if indexPath.row == 1
        {
            let cell : JobOrderDetailCell = tableView.dequeueReusableCell(withIdentifier: "JobOrderDetailCell", for: indexPath) as! JobOrderDetailCell
            cell.selectionStyle = .none
            
            var location = [String : Any]()
            
            location = self.arrPickUp[0]
            
            cell.lblWeight?.text = location["username"] as? String
            
            cell.itemDetail?.text = self.requestData["item_detail"] as? String ?? "-"
            
            cell.lblTotalFair?.text = "S$ \(self.requestData["net_amount"] as? String ?? "0.0")"
            
            let duration = Int(self.requestData["duration"] as? String ?? "0")!
            
            cell.lblCategory?.text = "\(duration/60)hrs \(duration%60) min"
            cell.lblDate?.text =  Helper.shared.convertDateFormate(value : self.requestData["schedule_date_time"] as? String ?? "0")
            
            cell.lblWaitingCharge?.text = "S$ \(self.requestData["waiting_charges"] as? String ?? "0.0")"
            cell.lblOfferAmount?.text = "S$ \(self.requestData["offer_amount"] as? String ?? "0.0")"
            cell.lblNetAmount?.text = "S$ \(self.requestData["amount"] as? String ?? "0.0")"
            cell.lblNightCharge?.text = "S$ \(self.requestData["surcharge"] as? String ?? "0.0")"
            return cell
        }
        else
        {
            let cell : JobLocationDetailCell = tableView.dequeueReusableCell(withIdentifier: "JobLocationDetailCell", for: indexPath) as! JobLocationDetailCell
            cell.selectionStyle = .none
            var location = [String : Any]()
            
            location = self.arrPickUp[0]
            cell.lblDetail?.text = "Queue Location"
            
            //cell.lblName?.text = location["name"] as? String
            cell.lblMobile?.text = location["contact_number"] as? String ?? "-"
            cell.lblLandMark?.text = location["landmark"] as? String ?? "-"
            cell.lblLocation?.text = location["address"] as? String ?? "-"
            cell.lblStatus?.text = "  \((location["status"] as? String ?? "Pending").uppercased())  "
            return cell
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0
        {
            let requestDetail = self.requestData["runner_detail"] as! [String : Any]
            if requestDetail["runner_id"] != nil{
                return 300
            }
            else
            {
                return 50
            }
        }
        else if  indexPath.row == 1
        {
            return 330
        }
        
        else{
            return 160
        }
    }
}
