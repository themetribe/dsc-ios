//
//  DriverLocationVC.swift
//  DRunnerCustomer
//
//  Created by mac306 on 19/10/20.
//  Copyright © 2020 mac306. All rights reserved.
//

import UIKit
import GoogleMaps

class DriverLocationVC: UIViewController {
    
    @IBOutlet weak var mapV : GMSMapView?
    @IBOutlet weak var btnDriverLocation : UIButton?
    var requestData : Dictionary = Dictionary<String,Any>()
    var driverLatitude : String = ""
    var driverLongitude : String = ""
    var clDropLocation = [CLLocationCoordinate2D]()
    var userData = defaults.object(forKey: udUserInfo) as! Dictionary<String,Any>
    var markerD = GMSMarker()
    var polyline = GMSPolyline()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mapV?.bringSubviewToFront(self.btnDriverLocation!)
        
        let _ = LocationManager.sharedInstance
        
        let pickUp = self.requestData["pickup_info"] as! [String : Any]
        let dropOff = self.requestData["drop_info"] as! [[String : Any]]
        let clPickup = CLLocationCoordinate2D.init(latitude: Double(pickUp["latitude"] as? String ?? "0.0")!, longitude: Double(pickUp["longitude"] as? String ?? "0.0")!)
        if pickUp["status"] as? String ?? "" == "pending"
        {
            self.clDropLocation.append(clPickup)
        }
        
        print(clPickup.latitude, clPickup.longitude)
        
        let marker = GMSMarker()
        
        let markerView = UIImageView(image: UIImage(named: "pickUp")!)
        
        marker.position = clPickup
        marker.iconView = markerView
        
        if self.requestData["service_name"] as? String ?? "" == "Help Me Queue"
        {
            marker.title = "Queue Location"
        }
        else if self.requestData["service_name"] as? String ?? "" == "Help Me Buy"
        {
            marker.title = "Buying Location"
        }
        else
        {
            marker.title = "Pickup Location"
        }
        marker.snippet = pickUp["landmark"] as? String ?? ""
        marker.map = self.mapV
        
        for i in 0..<dropOff.count
        {
            let drop = dropOff[i]
            let clD = CLLocationCoordinate2D.init(latitude: Double(drop["latitude"] as? String ?? "0.0")!, longitude: Double(drop["longitude"] as? String ?? "0.0")!)
            
            let markerView = UIImageView(image: UIImage(named: "drop")!)
            let markerD = GMSMarker()
            markerD.position = CLLocationCoordinate2D(latitude:Double(drop["latitude"] as? String ?? "0.0")!, longitude:  Double(drop["longitude"] as? String ?? "0.0")!)
            markerD.iconView = markerView
            markerD.title = "Drop Off"
            markerD.snippet = drop["landmark"] as? String ?? ""
            markerD.map = self.mapV
            if drop["status"] as? String ?? "" == "pending"
            {
                self.clDropLocation.append(clD)
            }
            
        }
        
        let camera = GMSCameraPosition.camera(withLatitude: Double(pickUp["latitude"] as? String ?? "0.0")!, longitude:  Double(pickUp["longitude"] as? String ?? "0.0")!, zoom: 15.0)
        self.mapV?.camera = camera
        
        Timer.scheduledTimer(withTimeInterval: 0.2, repeats: false) { (timer) in
            self.getRunnerLocationUpdate()
        }
    
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.userData = defaults.object(forKey: udUserInfo) as! Dictionary<String,Any>
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    @IBAction func onBackButton( _ sender : UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func getRunnerLocationUpdate()
    {
        let requestDetail = self.requestData["runner_detail"] as! [String : Any]
        if requestDetail["runner_id"] != nil{
            let myPath = FirebaseDB.refs.databaseRunners.child("\(requestDetail["runner_id"] as? String ?? "")")
            
            myPath.observe(.value) { (snapshot) in
                if snapshot.exists(), let values = snapshot.value {
                    
                    self.markerD.map = nil
                    let jsonDict = values as! [String: Any]
                    self.driverLatitude = jsonDict["latitude"] as? String ?? "0.0"
                    self.driverLongitude = jsonDict["longitude"] as? String ?? "0.0"
                    
                    let driver = CLLocationCoordinate2D.init(latitude: Double(self.driverLatitude)!, longitude: Double(self.driverLongitude)!)
                    
                    self.markerD = GMSMarker()
                    self.markerD.position = driver
                    self.markerD.iconView = UIImageView(image: UIImage(named: "loctaion")!)
                    self.markerD.title = requestDetail["runner_name"] as? String ?? "Runner"
                    self.markerD.snippet = "Runner"
                    self.markerD.map = self.mapV
                    
                    
                    self.getPolylineRoute(from: driver, to: self.clDropLocation) { (isSuccess, polylinePoints) in
                        if isSuccess {
                            DispatchQueue.main.async {
                                
                                self.drawPolyline(withMapView: self.mapV!, withPolylinePoints: polylinePoints)
                            }
                        } else {
                            print("Falied to draw polyline")
                        }
                    }
                }
            }
        }
    }
    
    func getPolylineRoute(from source: CLLocationCoordinate2D, to destinations: [CLLocationCoordinate2D], completionHandler: @escaping (Bool, String) -> ()) {
        
        guard let destination = destinations.last else {
            return
        }
        var wayPoints = ""
        for (index, point) in destinations.enumerated() {
            if index == 0 { // Skipping first location that is current location.
                continue
            }
            wayPoints = wayPoints.count == 0 ? "\(point.latitude),\(point.longitude)" : "\(wayPoints)%7C\(point.latitude),\(point.longitude)"
        }
        
        let url = URL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=\(source.latitude),\(source.longitude)&destination=\(destination.latitude),\(destination.longitude)&sensor=true&mode=driving&waypoints=\(wayPoints)&key=\(kGoogleServiceKey)")!
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            if error != nil {
                print("Failed : \(String(describing: error?.localizedDescription))")
                return
            } else {
                do {
                    if let json: [String: Any] = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any] {
                        guard let routes = json["routes"] as? [[String: Any]] else { return }
                        if (routes.count > 0) {
                            let overview_polyline = routes[0]
                            let dictPolyline = overview_polyline["overview_polyline"] as? NSDictionary
                            let points = dictPolyline?.object(forKey: "points") as? String
                            completionHandler(true, points!)
                        } else {
                            completionHandler(false, "")
                        }
                    }
                } catch {
                    print("Error : \(error)")
                }
            }
        }
        task.resume()
    }
    //    getPolylineRoute(from: coods[0], to: coods) { (isSuccess, polylinePoints) in
    //        if isSuccess {
    //            DispatchQueue.main.async {
    //                self.drawPolyline(withMapView: self.mapView, withPolylinePoints: polylinePoints)
    //            }
    //        } else {
    //            print("Falied to draw polyline")
    //        }
    //    }
    
    func drawPolyline(withMapView googleMapView: GMSMapView, withPolylinePoints polylinePoints: String){
        polyline.map = nil
        let path = GMSPath(fromEncodedPath: polylinePoints)!
        polyline = GMSPolyline(path: path)
        polyline.strokeWidth = 3.0
        polyline.strokeColor = .black
        polyline.map = self.mapV
        
    }
    
    @IBAction func goToRunnerLocation( _ sender : UIButton)
    {
        if self.driverLatitude != ""
        {
            let camera = GMSCameraPosition.camera(withLatitude: Double(self.driverLatitude)!, longitude: Double(self.driverLongitude)!, zoom: 18.0)
            self.mapV?.camera = camera
            
        }
    }
}
