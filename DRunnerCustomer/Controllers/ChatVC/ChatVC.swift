//
//  ChatVC.swift
//  DRunnerCustomer
//
//  Created by mac306 on 07/08/20.
//  Copyright © 2020 mac306. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseCore
import FirebaseStorage

struct FirebaseDB
{
    struct refs
    {
        static let databaseRoot   = Database.database().reference()
        static let databaseChats  = databaseRoot.child("Message")
        static let databaseRunners  = databaseRoot.child("Runners")
    }
    //other params
    
    //Recent params
    static let TIMESTAMP = "time"
    static let MESSAGE   = "message"
    static let IS_ONLINE = "isOnline"
    static let COUNT     = "count"
    
    //Message table params
    static let FROM = "from"
    static let MESSAGE_TYPE = "type";
    static let IMAGE_ID = "image_id"
    static let LAT = "lat";
    static let LNG = "lng";
    
}

//fileprivate var storageRef: StorageReference = Storage.storage().reference(forURL: "https://dstart-runner.firebaseio.com/")
//
//fileprivate var channelRef: DatabaseReference = Database.database().reference().child("lastmessage")
//fileprivate var CurrentStatus: DatabaseReference = Database.database().reference()

class ChatVC: UIViewController {
    
    
    
    @IBOutlet weak var constraint_text_view_bottom: NSLayoutConstraint!
    @IBOutlet weak var growingTextView: GrowingTextView!
    @IBOutlet weak var table_view : UITableView!
    @IBOutlet weak var btn_Send: UIButton!
    @IBOutlet weak var constraint_record_View_Leading: NSLayoutConstraint!
    
    
    @IBOutlet weak var viewMessage: UIView?
    @IBOutlet weak var btnAttachment_widthConstraint: NSLayoutConstraint!
    @IBOutlet weak var view_record: UIView!
    @IBOutlet weak var lbl_recordTime: UILabel!
    
    var requestDetail : Dictionary = Dictionary<String,Any>()
    var arrChat : Array = [Dictionary<String,Any>]()
    var dateArray = Array<String>()
    let imagePicker = UIImagePickerController()
    var isImageEdit : Bool = false
    var chatArrayData = [[Dictionary<String,Any>]]()
    
    //@IBOutlet weak var viewDeleteUser: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        growingTextView.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification , object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification , object: nil)
        self.getChat()
        if self.requestDetail["status"] as? String ?? "" == "Completed"
        {
            self.viewMessage?.alpha = 0.0
        }
        else
        {
            self.viewMessage?.alpha = 1.0
        }
        
        // Do any additional setup after loading the view.
       
    }
    
    override func viewDidAppear(_ animated: Bool) {
        Database.database().goOnline()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        Database.database().goOffline()
    }
    
    func getChat()
    {
        let myPath = FirebaseDB.refs.databaseChats.child("\(self.requestDetail["request_id"] as? String ?? "")")
        
        self.arrChat.removeAll()
        myPath.observe(.value) { (snapshot) in
            if snapshot.exists(), let values = snapshot.value{
                let jsonDict = values as! [String: Any]
                self.arrChat.removeAll()
                for (key, value) in jsonDict {
                    self.arrChat.append(value as! Dictionary<String, Any>)
                }
                //self.arrChat = self.arrChat.reversed()
                DispatchQueue.main.async {
                    
                    self.dateArray = []
                    for i in 0..<self.arrChat.count
                    {
                        let timeN = self.arrChat[i]["timestamp"] as? NSNumber ?? 0
                        let dt = Helper.shared.getTimeFromDateTimeStamp(timeSt: timeN.stringValue)
                        self.dateArray.append(dt)
                    }
                    self.dateArray = Array(Set(self.dateArray))
                    
                    self.dateArray = Helper.shared.dateSortingAccordingTime(timeSt : self.dateArray)
                    
                    self.arrChat = self.arrChat.sorted{($1["timestamp"] as! NSNumber).stringValue > ($0["timestamp"] as! NSNumber).stringValue }
                    
                    self.chatArrayData = []
                    
                    for i in 0..<self.dateArray.count
                    {
                        
                        let dateS = self.dateArray[i]
                        var chatD = [Dictionary<String,Any>]()
                        for j in 0..<self.arrChat.count
                        {
                            
                            let timeN = self.arrChat[j]["timestamp"] as? NSNumber ?? 0
                            let dateStr = Helper.shared.getTimeFromDateTimeStamp(timeSt: timeN.stringValue)
                            
                            if dateS == dateStr
                            {
                                chatD.append(self.arrChat[j])
                            }
                        }
                        self.chatArrayData.append(chatD)
                    }
                    
                    self.table_view.reloadData()
                    self.scrollViewToEnd()
                }
            }
            else{
                
                
            }
        }
    }
    
    func jsonData(obj:Any) -> Data{
        let jsonData = try? JSONSerialization.data(withJSONObject: obj, options: [])
        let reqJSONStr = String(data: jsonData ?? Data(), encoding: .utf8)
        let data = reqJSONStr?.data(using: .utf8)
        
        return data!
        
    }
    
    
    @IBAction func onBackButton( _ sender : UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func onImageZoom(_ sender : TwoTagButton)
    {
        
        let cell : ChatCell = self.table_view.cellForRow(at: IndexPath.init(row: sender.row!, section: sender.section!)) as! ChatCell
        
        let imageInfo : JTSImageInfo = JTSImageInfo()
        imageInfo.image = cell.imgChat.image
        imageInfo.referenceRect = cell.imgChat.frame
        imageInfo.referenceView = cell.imgChat.superview
        imageInfo.referenceCornerRadius = cell.imgChat.layer.cornerRadius
        let imageViewer : JTSImageViewController = JTSImageViewController.init(imageInfo: imageInfo, mode: JTSImageViewControllerMode.image, backgroundStyle: JTSImageViewControllerBackgroundOptions.scaled)
        
        imageViewer .show(from: self, transition: JTSImageViewControllerTransition.fromOriginalPosition)
    }
    
    
    @IBAction func onPictureButton( _ sender : UIButton)
    {
        self.showActionSheetWith(items: ["Camera","Photo library"], title: kAppName, message: "Send picture") { (sheetTitle) in
            if sheetTitle == "Camera"
            {
                self.openMediaPickerWith(source: .camera, mediaType: "Camera")
            }
            else{
                self.openMediaPickerWith(source: .photoLibrary, mediaType: "Camera")
            }
        }
    }
    
    
    func openMediaPickerWith(source:UIImagePickerController.SourceType , mediaType : String){
        if UIImagePickerController.isSourceTypeAvailable(source) {
            // let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = source
            imagePicker.isEditing = true
            imagePicker.mediaTypes = ["public.image"]
            
            imagePicker.allowsEditing = true
            
            if let popoverController = imagePicker.popoverPresentationController {
                popoverController.sourceView = self.view
                popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
                popoverController.permittedArrowDirections = []
            }
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    
    
    //MARK: - KEYBOARD NOTIFICATIONS ---------
    @objc func keyboardWillHide(_ sender: Notification) {
        if let userInfo = (sender as NSNotification).userInfo {
            if let _ = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size.height {
                self.constraint_text_view_bottom.constant =  0
                UIView.animate(withDuration: 0.25, animations: { () -> Void in
                                let rect = CGRect(x: self.table_view.frame.origin.x, y: 0, width:self.table_view.frame.size.width , height: self.view.frame.size.height)
                                self.table_view.frame = rect
                                self.view.layoutIfNeeded() })
            }
        }
    }
    @objc func keyboardWillShow(_ sender: Notification) {
        
        if let userInfo = (sender as NSNotification).userInfo {
            if let keyboardHeight = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size.height {
                self.constraint_text_view_bottom.constant = keyboardHeight
                UIView.animate(withDuration: 0.25, animations: { () -> Void in
                    let rect = CGRect(x: self.table_view.frame.origin.x, y: 0, width:self.table_view.frame.size.width , height: self.table_view.frame.size.height - keyboardHeight)
                    self.table_view.frame = rect
                    self.view.layoutIfNeeded()
                    self.scrollViewToEnd()
                    
                })
            }
        }
    }
    
    
    @IBAction func onSendButton(_ sender : UIButton)
    {
        
        if !(Connectivity.isConnectedToInternet)
        {
            Helper.shared.showAlert(msg: No_Internet)
            return
        }
        if growingTextView.text?.replacingOccurrences(of: " ", with: "") != ""{
            
            guard growingTextView.text?.trimmingCharacters(in: .whitespacesAndNewlines) != "" else {
                return
            }
            let requestD = self.requestDetail["runner_detail"] as! [String : Any]
            if requestD["runner_id"] != nil{
                
                let myPath = FirebaseDB.refs.databaseChats.child("\(self.requestDetail["request_id"] as? String ?? "")").childByAutoId()
                let param = ["message" : growingTextView.text ?? "",
                             "sender_id" : Helper.shared.getUserID() ,
                             "receiver_id" : requestD["runner_id"] as? String ?? "0",
                             "message_type" : "text",
                             "timestamp" : [".sv": "timestamp"],
                             "isRead" : false,
                             "keyID" : myPath.key!
                ] as [String : Any]
                
                
                myPath.setValue(param) { (error:Error?, ref:DatabaseReference) in
                    if let error = error {
                        print("Data could not be saved: \(error).")
                        
                    } else {
                        print("Data saved successfully!")
                        self.notificationSentToRunner(messageType: "text", message: self.growingTextView.text)
                        self.growingTextView.text = ""
                        self.growingTextView.placeholder1 = "Type your message..."
                        
                    }
                }
            }
        }
    }
    
    
    func notificationSentToRunner(messageType : String, message : String)
    {
        let requestD = self.requestDetail["runner_detail"] as! [String : Any]
        if requestD["runner_id"] != nil{
            
            let param = ["receiver_id" : requestD["runner_id"] as? String ?? "0",
                         "sender_id" : Helper.shared.getUserID(),
                         "msg_type" : messageType,
                         "request_id" : self.requestDetail["request_id"] as? String ?? "",
                         "message" : message]
            
            API_HELPER.sharedInstance.postDataInJsonUsingNS(header: K.ApiName.SendChatNotification, withParameter: param as NSDictionary, inVC: self, showHud: false, addAuthHeader: true) { (result, message, status) in
                
            }
        }
    }
    
    
    
    @objc func scrollViewToEnd()
    {
        if self.chatArrayData.count > 0
        {
            let arrD = self.chatArrayData[self.chatArrayData.count - 1]
            if arrD.count > 0
            {
                let indexP = IndexPath.init(row: arrD.count - 1, section: self.chatArrayData.count - 1)
                self.table_view.scrollToRow(at: indexP, at: .bottom, animated: false)
                
            }
        }
    }
}
extension ChatVC :  UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        
        let image = info[UIImagePickerController.InfoKey.editedImage]! as! UIImage
        self.isImageEdit = true
        // self.imgProfile?.image = image
        picker.dismiss(animated: true, completion: {
            // self.updateUserProfile()
            self.uploadImagePic(img1: image)
            //let metaDataConfig = StorageMetadata()
            
        })
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.isImageEdit = false
        picker.dismiss(animated: true, completion: nil)
    }
    
    func uploadImagePic(img1 :UIImage){
        
        API_HELPER.sharedInstance.uploadMultipartData(methodType: .post, header: K.ApiName.ChatImgUpload, withParameter: ["request_id": self.requestDetail["request_id"] as? String ?? ""], imageArray: [img1], inVC: self, showHud: true, addAuthHeader: true, videoAdd: false, audioAdd: false, fileAdd: false, gifAdd: false) { (result, message, status) in
            if status
            {
                let requestD = self.requestDetail["runner_detail"] as! [String : Any]
                if requestD["runner_id"] != nil{
                    let myPath = FirebaseDB.refs.databaseChats.child("\(self.requestDetail["request_id"] as? String ?? "")").childByAutoId()
                    
                    let param = ["message" : result["image_url"] as? String ?? "" , "sender_id" : Helper.shared.getUserID() , "receiver_id" : requestD["runner_id"] as? String ?? "0", "request_id" : self.requestDetail["request_id"] as? String ?? "", "message_type" : "image", "timestamp" : [".sv": "timestamp"], "isRead" : false, "keyID" : myPath.key! ] as [String : Any]
                    
                    myPath.setValue(param) { (error:Error?, ref:DatabaseReference) in
                        if let error = error {
                            print("Data could not be saved: \(error).")
                            
                        } else {
                            print("Data saved successfully!")
                            self.notificationSentToRunner(messageType: "image", message: result["image_url"] as? String ?? "")
                            self.growingTextView.text = ""
                            self.growingTextView.placeholder1 = "Type your message..."
                            
                        }
                    }
                }
            }
        }
        
    }
    
    
}
extension ChatVC : UITextViewDelegate
{
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        return true
    }
    func textViewDidChange(_ textView: UITextView) {
        
        //        if (textView.text?.count)! == 0 &&  textView.text == ""
        //        {
        //            btn_Send.tag = 1
        //            self.btn_Send.setImage(UIImage(named :"mike"), for: .normal)
        //        }
        //        else
        //        {
        //            btn_Send.tag = 0
        //            self.btn_Send.setImage(UIImage(named :"sent-mail"), for: .normal)
        //        }
    }
}
extension ChatVC : UITableViewDataSource,UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.chatArrayData.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //  return chatData.count
        if self.chatArrayData.count > 0
        {
            let chatD = self.chatArrayData[section]
            return chatD.count
        }
        return 0
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let viewH = UIView.init()
        viewH.frame = CGRect.init(x: 0, y: 0, width: self.table_view.frame.size.width, height: 30)
        
        let lblHeader = UILabel()
        lblHeader.textAlignment = .center
        lblHeader.frame = CGRect.init(x: 10, y: 5, width: self.table_view.frame.size.width - 20, height: 20)
        if self.dateArray.count > 0
        {
            
            lblHeader.text = Helper.shared.getDayAndDate(dateStr: self.dateArray[section])
        }
        viewH.addSubview(lblHeader)
        return viewH
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let chatDetail = self.chatArrayData[indexPath.section][indexPath.row]
        if chatDetail["message_type"] as? String ?? "" == "text"
        {
            return UITableView.automaticDimension
        }
        else
        {
            return 260.0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let chatDetail = self.chatArrayData[indexPath.section][indexPath.row]
        if chatDetail["sender_id"] as? String ?? "" == Helper.shared.getUserID()
        {
            if chatDetail["message_type"] as? String ?? "" == "text"
            {
                let cell : ChatCell = tableView.dequeueReusableCell(withIdentifier: "ChatMySide", for: indexPath) as! ChatCell
                cell.changeImage("bubble_sent", backgroundColor: AppColor)
                cell.lblTxtActive.text = chatDetail["message"] as? String ?? ""
//                cell.lblTxtActive.urlLinkTapHandler = { label, url, range in
//
//                }
                let isRead = chatDetail["isRead"] as? Bool
                cell.imgTick.image = isRead == true ? UIImage(named: "double_tick") : UIImage(named: "single_tick")
                let timeN = chatDetail["timestamp"] as? NSNumber ?? 0
                cell.lblTime.text = Helper.shared.getTimeFromTimeStamp(timeSt : timeN.stringValue)
                
                return cell
            }
            else
            {
                let cell : ChatCell = tableView.dequeueReusableCell(withIdentifier: "ChatMySide2", for: indexPath) as! ChatCell
                cell.changeImage("bubble_sent", backgroundColor: AppColor)
                cell.btnChat.row = indexPath.row
                cell.btnChat.section = indexPath.section
                cell.btnChat.addTarget(self, action: #selector(self.onImageZoom(_:)), for: .touchUpInside)
                
                if chatDetail["message"] as? String ?? "" != ""
                {
                    
                    cell.imgChat?.loadImageUsingCache(withUrl: chatDetail["message"] as? String ?? "")
                }
                
                let isRead = chatDetail["isRead"] as? Bool
                cell.imgTick.image = isRead == true ? UIImage(named: "double_tick") : UIImage(named: "single_tick")
                let timeN = chatDetail["timestamp"] as? NSNumber ?? 0
                cell.lblTime.text = Helper.shared.getTimeFromTimeStamp(timeSt : timeN.stringValue)
                
                return cell
            }
        }
        else
        {
            if chatDetail["message_type"] as? String ?? "" == "text"
            {
                let cell : ChatCell = tableView.dequeueReusableCell(withIdentifier: "ChatMySide1", for: indexPath) as! ChatCell
                cell.changeImage("bubble_received", backgroundColor: UIColor.lightGray)
                cell.lblTxtActive.text = chatDetail["message"] as? String ?? ""
//                cell.lblTxtActive.urlLinkTapHandler = { label, url, range in
//
//                }
                let timeN = chatDetail["timestamp"] as? NSNumber ?? 0
                cell.lblTime.text = Helper.shared.getTimeFromTimeStamp(timeSt : timeN.stringValue)
                return cell
            }
            else
            {
                let cell : ChatCell = tableView.dequeueReusableCell(withIdentifier: "ChatMySide3", for: indexPath) as! ChatCell
                cell.changeImage("bubble_received", backgroundColor: UIColor.lightGray)
                cell.btnChat.row = indexPath.row
                cell.btnChat.section = indexPath.section
                cell.btnChat.addTarget(self, action: #selector(self.onImageZoom(_:)), for: .touchUpInside)
                
                if chatDetail["message"] as? String ?? "" != ""
                {
                    
                    cell.imgChat?.loadImageUsingCache(withUrl: chatDetail["message"] as? String ?? "")
                }
                
                let timeN = chatDetail["timestamp"] as? NSNumber ?? 0
                cell.lblTime.text = Helper.shared.getTimeFromTimeStamp(timeSt : timeN.stringValue)
                
                return cell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        var chatDetail = self.chatArrayData[indexPath.section][indexPath.row]
        if chatDetail["sender_id"] as? String ?? "" != Helper.shared.getUserID()
        {
            if chatDetail["isRead"] as! Bool == false {
                chatDetail["isRead"] = true
                
                let myPath = FirebaseDB.refs.databaseChats.child("\(self.requestDetail["request_id"] as? String ?? "")").child("\(chatDetail["keyID"] ?? "")")
                myPath.updateChildValues(chatDetail)
                
            }
        }
    }

}
