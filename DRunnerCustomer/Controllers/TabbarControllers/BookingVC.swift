//
//  BookingVC.swift
//  DRunnerCustomer
//
//  Created by mac306 on 17/07/20.
//  Copyright © 2020 mac306. All rights reserved.
//

import UIKit


class BookingVC: UIViewController{
    
    @IBOutlet weak var tblData : UITableView?
    var arrayRunnerData  : Array = [Dictionary<String,Any>]()
    var arrayReviewData  : Array = [Dictionary<String,Any>]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblData?.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.getTopTunners()
        self.getAdminReview()
        self.navigationController?.navigationBar.isHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    
    func getTopTunners()
    {
        
        API_HELPER.sharedInstance.postDataInJsonUsingNS(header: K.ApiName.TopRunner, withParameter: ["user_id":Helper.shared.getUserID(),"latitude" :  LAT,"longitude" : LONG], inVC: self, showHud: false, addAuthHeader: true) { (result, message, true) in
                DispatchQueue.main.async {
                    self.arrayRunnerData = result["data"] as! Array
                    self.tblData?.reloadData()
            }
        }
    }
    
    func getAdminReview() {
        
        API_HELPER.sharedInstance.postDataInJsonUsingNS(header: K.ApiName.adminReview, withParameter: ["user_id":Helper.shared.getUserID()], inVC: self, showHud: true, addAuthHeader: true) { (result, message, true) in
                DispatchQueue.main.async {
                    self.arrayReviewData = result["data"] as! Array
                    self.tblData?.reloadData()
            }
        }
    }
    
}
extension BookingVC: UITableViewDelegate,UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0
        {
            return 300.0
        }
        else if indexPath.row == 1
        {
            return 170
        }
        return 250.0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if indexPath.row == 0
        {
            let cell : HomeTableCell = tableView.dequeueReusableCell(withIdentifier: "HomeTableCellTop", for: indexPath) as! HomeTableCell
            cell.clView?.dataSource = self
            cell.clView?.delegate = self
            cell.clView?.tag = indexPath.row
            cell.clView?.reloadData()
            return cell
        }
        else if indexPath.row == 1
        {
            let cell : HomeTableCell = tableView.dequeueReusableCell(withIdentifier: "HomeTableCellMiddle", for: indexPath) as! HomeTableCell
            cell.clView?.dataSource = self
            cell.clView?.delegate = self
            cell.clView?.tag = indexPath.row
            cell.clView?.reloadData()
            return cell
        }
        else{
            let cell : HomeTableCell = tableView.dequeueReusableCell(withIdentifier: "HomeTableCellLast", for: indexPath) as! HomeTableCell
            cell.clView?.dataSource = self
            cell.clView?.delegate = self
            cell.clView?.tag = indexPath.row
            cell.clView?.reloadData()
            return cell
        }
        
    }
}
extension BookingVC: UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 1
        {
            return 3
        }
        else if collectionView.tag == 0
        {
            return self.arrayReviewData.count
        }
        else
        {
            return self.arrayRunnerData.count
            //return 10
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView.tag == 1
        {
            let cell : HomeCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionCell\(collectionView.tag)", for: indexPath) as! HomeCollectionCell
            
            if indexPath.row == 0
            {
                cell.lblServiceName?.text = "Help Me Buy"
                cell.imgService?.image = UIImage(named: "iconeone")
            }
            else if indexPath.row == 1
            {
                cell.lblServiceName?.text = "Help Me Send"
                cell.imgService?.image = UIImage(named: "icontwo")
            }
            else
            {
                cell.lblServiceName?.text = "Help Me Queue"
                cell.imgService?.image = UIImage(named: "iconthree")
            }
            cell.btnBook?.tag = indexPath.row
            //cell.btnBook?.addTarget(self, action: #selector(onBooking(_:)), for: .touchUpInside)
            return cell
        }
        else{
            if collectionView.tag == 0
            {
                let cell : HomeCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionCell\(collectionView.tag)", for: indexPath) as! HomeCollectionCell
                let data = self.arrayReviewData[indexPath.row]
                if data["image"] as? String ?? "" != ""
                {
                    cell.imgUser?.loadImageUsingCache(withUrl: data["image"] as? String ?? "")
                }
                cell.lblDescription?.text = data["comment"] as? String ?? ""
                
                let rating = (data["rating"] as? String ?? "")
                if let n = NumberFormatter().number(from: rating) {
                    cell.viewRating?.value = CGFloat((n as NSNumber).doubleValue)
                }
                
                return cell
            }
            else
            {
                let cell : HomeCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionCell\(collectionView.tag)", for: indexPath) as! HomeCollectionCell
                let runnerData = self.arrayRunnerData[indexPath.row]
                
                if runnerData["image"] as? String ?? "" != ""
                {
                    cell.imgUser?.loadImageUsingCache(withUrl: runnerData["image"] as? String ?? "")
                }
                cell.lblDriverRank?.text = "\(indexPath.row + 1)"
                cell.lblCompletedTrips?.text = runnerData["complete_orders"] as? String ?? ""
                cell.lblCancelledTrips?.text = runnerData["cancelled_orders"] as? String ?? ""
                cell.lblRating?.text = runnerData["rating"] as? String ?? ""
                cell.lblServiceName?.text = runnerData["name"] as? String ?? ""
                if runnerData["view_status"] as? String ?? "" == "online"
                {
                    cell.lblRunnerStatus?.backgroundColor = .green
                }
                else
                {
                     cell.lblRunnerStatus?.backgroundColor = .red
                }
                return cell
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.tag == 1
        {
            if indexPath.row == 2
            {
                let profileVC = BOOKTASK_SB.instantiateViewController(withIdentifier: "HMQBookingVC") as! HMQBookingVC
                            
                                self.navigationController?.pushViewController(profileVC, animated: true)
            }
            else
            {
            self.bookJob(runnerId: "", serviceType: indexPath.row)
            }
           
        }
        else if collectionView.tag == 2
        {
            let runnerData = self.arrayRunnerData[indexPath.row]
            if runnerData["view_status"] as? String ?? "" == "online"
            {
              
                let alertController = UIAlertController(title: kAppName, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
          
            
                alertController.addAction(UIAlertAction(title: "Help Me Buy", style: .default, handler: {(alert: UIAlertAction!) in
                    self.bookJob(runnerId: runnerData["id"] as? String ?? "" , serviceType: 0)
                }))
                alertController.addAction(UIAlertAction(title: "Help Me Send", style: .default, handler: {(alert: UIAlertAction!) in
                  self.bookJob(runnerId: runnerData["id"] as? String ?? "" , serviceType: 1)
                }))
                alertController.addAction(UIAlertAction(title: "Help Me Queue", style: .default, handler: {(alert: UIAlertAction!) in
                    let profileVC = BOOKTASK_SB.instantiateViewController(withIdentifier: "HMQBookingVC") as! HMQBookingVC
                    profileVC.runner_id = runnerData["id"] as? String ?? ""
                    self.navigationController?.pushViewController(profileVC, animated: true)
                }))
                alertController.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: {(alert: UIAlertAction!) in
                             
                       }))
                DispatchQueue.main.async {
                       self.present(alertController, animated: true, completion:{})
                }
            }
            else
            {
                self.showToast("Runner is not available right now. Please try again latter.")
            }
        }
    }
    
    func bookJob(runnerId : String , serviceType : Int)
    {
        let profileVC = BOOKTASK_SB.instantiateViewController(withIdentifier: "ServiceBookingVC") as! ServiceBookingVC
        profileVC.serviceType = serviceType
        profileVC.runner_id = runnerId
        self.navigationController?.pushViewController(profileVC, animated: true)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView.tag == 1
        {
            return CGSize.init(width: collectionView.frame.size.width / 3.0 - 5 , height: collectionView.frame.size.width / 3.0 - 10)
        }
        
        return CGSize.init(width: collectionView.frame.size.width - 40 , height: 178)
        
        
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt SECTION: Int) -> CGFloat {
        return 5.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5.0
    }
}
