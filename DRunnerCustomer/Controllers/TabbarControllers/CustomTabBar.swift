//
//  CustomTabVC.swift
//  DRunnerCustomer
//
//  Created by mac306 on 17/07/20.
//  Copyright © 2020 mac306. All rights reserved.
//

import UIKit

class CustomTabBar: UITabBarController, UITabBarControllerDelegate {

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        self.tabBar.unselectedItemTintColor = .gray
        
        // Do any additional setup after loading the view.
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        //Tab tapped
        guard let viewControllers = tabBarController.viewControllers else { return }
        let tappedIndex = viewControllers.index(of: viewController)!
        //Tab tapped at tappedIndex
      if tappedIndex == 2
      {
        print(tappedIndex)
        }
    }
    
}
 
