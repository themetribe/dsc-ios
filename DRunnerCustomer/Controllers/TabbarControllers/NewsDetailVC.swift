//
//  NewsDetailVC.swift
//  DRunnerCustomer
//
//  Created by mac306 on 05/10/20.
//  Copyright © 2020 mac306. All rights reserved.
//

import UIKit

class newzCell : UITableViewCell
{
    @IBOutlet weak var imgCell : UIImageView?
    @IBOutlet weak var txtDetail : UITextView?
}

class NewsDetailVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextViewDelegate {

    @IBOutlet weak var tblNewzData : UITableView?
    var newzDetail : Dictionary = Dictionary<String,Any>()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = newzDetail["title"] as? String ?? ""
        self.tblNewzData?.tableFooterView = UIView.init()
        // Do any additional setup after loading the view.
    }
    @IBAction func onBackButton( _ sender : UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }


    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : newzCell = self.tblNewzData?.dequeueReusableCell(withIdentifier: "newzCell", for: indexPath) as! newzCell
      
        let des = newzDetail["description"] as? String ?? ""
        cell.txtDetail?.attributedText = des.htmlToAttributedString
        cell.txtDetail?.font = UIFont(name: "Poppins-Regular", size: 13.0)
        cell.txtDetail?.delegate = self
        if newzDetail["image"] as? String ?? "" != ""
        {
            cell.imgCell?.loadImageUsingCache(withUrl: newzDetail["image"] as? String ?? "")
        }
        return cell
    }
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        return true
    }
}
