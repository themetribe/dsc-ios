//
//  SettingsVC.swift
//  DRunnerCustomer
//
//  Created by mac306 on 17/07/20.
//  Copyright © 2020 mac306. All rights reserved.
//

import UIKit
//import ZDCChat
import ChatSDK
import ChatProvidersSDK
import MessagingSDK

class SettingsVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tblSettings : UITableView?
    
    let tblData : Array = ["","Points","Rewards","Favourite Runner","Invite Friends","My Wallet","Help Centre","Change Password","Settings"]
    let tblImageData : Array = ["","Personal-Information","Manage-Cards","","Invite-Friends","wallet","Help","Change Password","about"]
    var userData = defaults.object(forKey: udUserInfo) as! Dictionary<String,Any>
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tblSettings?.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        getUserProfile()
        self.tblSettings?.reloadData()
    }
    
    func getUserProfile()
    {
        API_HELPER.sharedInstance.postDataInJsonUsingNS(header: K.ApiName.Profile, withParameter: ["user_id": Helper.shared.getUserID()], inVC: self, showHud: true, addAuthHeader: true) { (resultData, message, status) in
            DispatchQueue.main.async {
                if !status
                {
                    Helper.shared.showAlert(msg: message)
               }
                else{
                    let dict : NSMutableDictionary = APP_DELEGATE.convertAllDictionaryValueToNil(resultData.mutableCopy() as! NSMutableDictionary) as! NSMutableDictionary
                    defaults.set(dict, forKey: udUserInfo)
                    self.userData = defaults.object(forKey: udUserInfo) as! Dictionary<String,Any>
                    Helper.shared.setUserPoints(value: dict["reward_points"] as? String ?? "0")
                    self.tblSettings?.reloadData()
                }
            }
        }
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tblData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0
        {
            let cell : SettingsTopCell = tableView.dequeueReusableCell(withIdentifier: "SettingsTopCell", for: indexPath) as! SettingsTopCell
            
            cell.selectionStyle = .none
            
            cell.lblName?.text = "\(userData["first_name"] as? String ?? "") \(userData["last_name"] as? String ?? "")"
            
            if userData["profile_pic"] as? String ?? "" != ""
            {
                 cell.imgUser?.loadImageUsingCache(withUrl: self.userData["profile_pic"] as? String ?? "")
            }

             return cell
        }
        else
        {
            let cell : SettingsCell = tableView.dequeueReusableCell(withIdentifier: "SettingsCell", for: indexPath) as! SettingsCell
            cell.selectionStyle = .none
            cell.lblPoints?.alpha = 1.0
            cell.lblPoints?.text = indexPath.row == 1 ? ( "\(Helper.shared.getUserPoints()) Points") : ""
            cell.addDataInCell(settingTitle: self.tblData[indexPath.row], settingImage: self.tblImageData[indexPath.row])
             return cell
        }
       
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0
        {
            DispatchQueue.main.async {
            let profileVC = HOME_SB.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
            self.navigationController?.pushViewController(profileVC, animated: true)
            }
        }
        else if indexPath.row == 1
        {
            let profileVC = POINTS_SB.instantiateViewController(withIdentifier: "PointsVC") as! PointsVC
            self.navigationController?.pushViewController(profileVC, animated: true)
        }
        else if indexPath.row == 2
        {
            let profileVC = POINTS_SB.instantiateViewController(withIdentifier: "RewardsVC") as! RewardsVC
            self.navigationController?.pushViewController(profileVC, animated: true)
        }
            else if indexPath.row == 3
            {
                let profileVC = RATING_SB.instantiateViewController(withIdentifier: "FavRunnerListVC") as! FavRunnerListVC
                self.navigationController?.pushViewController(profileVC, animated: true)
            }
        else if indexPath.row == 4
        {
            let items: [Any] = ["Let me recommend you this application, Please use my referal code \(userData["referral_code"] as? String ?? "") to earn.", URL(string: "https://dstar-runner.com/")!]

            let activityViewController = UIActivityViewController(activityItems: items, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop ]
            self.present(activityViewController, animated: true, completion: nil)
            
        }
        else if indexPath.row == 5
        {
            let profileVC = PAYMENT_SB.instantiateViewController(withIdentifier: "WalletVC") as! WalletVC
            self.navigationController?.pushViewController(profileVC, animated: true)
        }
        else if indexPath.row == 6
        {
            do {
                           let chatEngine = try ChatEngine.engine()
                           let viewController = try Messaging.instance.buildUI(engines: [chatEngine], configs: [])
                           viewController.hidesBottomBarWhenPushed = true
                           self.navigationController?.pushViewController(viewController, animated: true)
                       } catch {
                           // handle error
                       }
            //ZDCChat.start(in: self.navigationController, withConfig: nil)
        }
        else if indexPath.row == 7
        {
            let profileVC = MAIN_SB.instantiateViewController(withIdentifier: "ChangePasswordVC") as! ChangePasswordVC
                           
            self.navigationController?.pushViewController(profileVC, animated: true)
        }
        else if indexPath.row == 8
        {
            let profileVC = HOME_SB.instantiateViewController(withIdentifier: "ManageSettings") as! ManageSettings
                     
            self.navigationController?.pushViewController(profileVC, animated: true)
          
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0
        {
            return 80.0
        }
        else
        {
            return 60.0
        }
    }

}
