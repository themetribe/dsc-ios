//
//  NotificationVC.swift
//  DRunnerCustomer
//
//  Created by mac306 on 17/07/20.
//  Copyright © 2020 mac306. All rights reserved.
//

import UIKit

class NotificationVC: UIViewController {
 
    @IBOutlet weak var tblList : UITableView?
    @IBOutlet weak var lblMessage : UILabel?
    var notificationList  : Array = [Dictionary<String,Any>()]
    var notificationData = Dictionary<String,Any>()
    @IBOutlet weak var btnDeleteAll : UIButton?
    var selectedRow = -100
    override func viewDidLoad() {
        super.viewDidLoad()
        self.notificationList.removeAll()
        self.btnDeleteAll?.isHidden = true
        self.tblList?.alpha = 0.0
        // Do any additional setup after loading the view.
        self.tblList?.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //self.lblMessage?.alpha = 0.0
        self.getNotificationList()
    }

   func getNotificationList()
   {
    
    API_HELPER.sharedInstance.postDataInJsonUsingNS(header: K.ApiName.notificationList, withParameter: ["user_id" : Helper.shared.getUserID()], inVC: self, showHud: false, addAuthHeader: true) { (result, message, status) in
           DispatchQueue.main.async {
                if status
                {
                     self.tblList?.alpha = 1.0
                    let dict : NSMutableDictionary = APP_DELEGATE.convertAllDictionaryValueToNil(result.mutableCopy() as! NSMutableDictionary) as! NSMutableDictionary
                    self.notificationData = dict as! [String : Any]
                    self.notificationList.removeAll()
                    self.notificationList = self.notificationData["notif_data"] as! [[String : Any]]
                    if self.notificationList.count > 0
                    {
                        self.btnDeleteAll?.isHidden = false
                        self.tblList?.alpha = 1.0
                    }
                    else
                    {
                        self.btnDeleteAll?.isHidden = true
                        self.tblList?.alpha = 0.0
                        
                    }
                    self.tblList?.reloadData()
                }
                else
                {
                    self.tblList?.alpha = 0.0
                    //self.showToast(message)
                }
            }
        }
    }
    func readNotification(row : Int)
    {
        let param = ["user_id" : Helper.shared.getUserID(),
                     "is_read" : "1",
                     "notification_id" : self.notificationList[row]["notification_id"] as? String ?? ""]
        API_HELPER.sharedInstance.postDataInJsonUsingNS(header: K.ApiName.notificationRead, withParameter: param as NSDictionary, inVC: self, showHud: false, addAuthHeader: true) { (result, message, status) in
               DispatchQueue.main.async {
                    if status
                    {
                        self.getNotificationList()
                    }
                    else
                    {
                        self.showToast(message)
                    }
                }
            
           }
    }
    
    @IBAction func onDeleteAllNotification(_ sender : UIButton)
    {
        self.showDoubleButtonAlert(title: kAppName, message: "Are you sure you want to delete all notification?", action1: "Yes", action2: "No", completion1: {
            self.deleteNotification(row: 0, type: "all")
            
        }) {
            
        }
        
    }
    
    func deleteNotification(row : Int , type : String)
        {
            let param = ["user_id" : Helper.shared.getUserID(),
                         "type" : type,
                         "notification_id" : self.notificationList[row]["notification_id"] as? String ?? ""]
            API_HELPER.sharedInstance.postDataInJsonUsingNS(header: K.ApiName.notificationDelete, withParameter: param as NSDictionary, inVC: self, showHud: true, addAuthHeader: true) { (result, message, status) in
                    DispatchQueue.main.async {
                        if status
                        {
                            self.getNotificationList()
                        }
                        else
                        {
                            self.showToast(message)
                        }
                    }
                
               }
        }
    

}
extension NotificationVC : UITableViewDataSource,UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.notificationList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell : NotificationCell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath) as! NotificationCell
        cell.selectionStyle = .none
        let notiData = self.notificationList[indexPath.row]
        cell.lblTitle?.text = notiData["title"] as? String ?? ""
        cell.lblSubTitle?.text = notiData["notification"] as? String ?? ""
        cell.lblDate?.text = notiData["created_at"] as? String ?? ""
        cell.lblAgoDays?.text = notiData["ago_time"] as? String ?? ""
        if notiData["is_read"] as? String ?? "0" == "1"
        {
            cell.imgMessage?.image = UIImage(named : "circleunselect")
        }
        else
        {
            cell.imgMessage?.image = UIImage(named : "circleselect")
        }
        self.selectedRow == indexPath.row ? (cell.imgDetail?.image = UIImage(named : "Ddown-arrow")) : (cell.imgDetail?.image = UIImage(named : "Dright-arrow"))
        
            return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.selectedRow == indexPath.row
        {
            return UITableView.automaticDimension
        }
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.selectedRow == indexPath.row ? (self.selectedRow = -100) : (self.selectedRow = indexPath.row)
        let notiData = self.notificationList[indexPath.row]
        if notiData["is_read"] as? String ?? "0" == "0"
        {
            self.readNotification(row: indexPath.row)
        }
        self.tblList?.reloadData()
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete
        {
            self.deleteNotification(row: indexPath.row, type: "manual")
            
        }
    }
   
   
    
}
