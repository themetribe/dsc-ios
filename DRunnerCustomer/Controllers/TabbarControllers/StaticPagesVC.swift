//
//  StaticPagesVC.swift
//  DRunnerCustomer
//
//  Created by mac306 on 05/08/20.
//  Copyright © 2020 mac306. All rights reserved.
//

import UIKit
import WebKit

class StaticPagesVC: UIViewController,  WKNavigationDelegate {

    @IBOutlet var webK: WKWebView?
    var urlString : String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = ""
        let url = URL(string: String(format :urlString))!
        webK?.navigationDelegate = self
        webK?.load(URLRequest(url: url))
       // LOADER.show(views: self.view)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    @IBAction func onBackButton( _ sender : UIButton)
       {
           self.navigationController?.popViewController(animated: true)
       }

      func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
            LOADER.hide(delegate: self)
            print(error.localizedDescription)
      }
      func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        LOADER.hide(delegate: self)
        //  print("Strat to load")
      }
      func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
          LOADER.hide(delegate: self)
          //print("finish to load")
      }

}
