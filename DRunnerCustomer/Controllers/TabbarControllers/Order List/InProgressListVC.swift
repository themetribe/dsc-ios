//
//  InProgressListVC.swift
//  DRunnerCustomer
//
//  Created by mac306 on 27/07/20.
//  Copyright © 2020 mac306. All rights reserved.
//

import UIKit

class InProgressListVC: UIViewController {

    @IBOutlet weak var tblList : UITableView?
    var arrProcessingList : Array  = [Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tblList?.tableFooterView = UIView()
        self.arrProcessingList.removeAll()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
      
         self.getRequestList()
    }
    override func viewWillDisappear(_ animated: Bool) {
         DispatchQueue.main.async {
        self.arrProcessingList.removeAll()
        self.tblList?.reloadData()
        }
    }
    
    func getRequestList()
        {
            API_HELPER.sharedInstance.postDataInJsonUsingNS(header: K.ApiName.RequestList, withParameter: ["user_id" : Helper.shared.getUserID(), "type" : "processing"], inVC: self, showHud: true, addAuthHeader: true) { (result, message, status) in
                DispatchQueue.main.async {
                    if status
                    {
                        self.tblList?.isHidden = false
                        self.arrProcessingList.removeAll()
                        self.arrProcessingList = result["data"] as! Array
                        if self.arrProcessingList.count > 0
                        {
                            self.tblList?.reloadData()
                        }
                        else
                        {
                            self.tblList?.isHidden = true
                        }
                    }
                    else
                    {
                       
                        self.showToast(message)
                    }
                }
           
        }
            
    }
    
    @objc  func onCancelledJob(_ sender : UIButton)
       {
           self.showDoubleButtonAlert(title: kAppName, message: "Are you sure you want cancel this request?", action1: "No", action2: "Yes", completion1: {
           }) {
               let requestDetail = self.arrProcessingList[sender.tag] as! [String : Any]
               let waitingVC = BOOKTASK_SB.instantiateViewController(identifier: "CancelRequestVC") as! CancelRequestVC
                     waitingVC.request_id = requestDetail["request_id"] as? String ?? ""
               self.navigationController?.pushViewController(waitingVC, animated: true)
           }
           
       }

}
extension InProgressListVC : UITableViewDataSource,UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrProcessingList.count
    }
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let requestDetail = self.arrProcessingList[indexPath.row] as! [String : Any]
        let requestType = requestDetail["service_name"] as? String ?? ""
        
        if requestType != "Help Me Queue"
        {
            let cell : CurrentOrderCell = tableView.dequeueReusableCell(withIdentifier: "CurrentOrderCell", for: indexPath) as! CurrentOrderCell
            let vehicalType = requestDetail["transport_category"] as? String ?? ""
            cell.lblServiceName?.text = requestDetail["service_name"] as? String ?? ""
           // cell.lblVehicalType?.text = "DStar - \(vehicalType)"
            cell.lblJobDate?.text = requestDetail["service_name"] as? String ?? ""
            cell.lblPicUpName?.text = ( requestType == "Help Me Buy" ? ("Buying location") : ("Pick up location") )
            
            
            cell.lblJobDate?.text =  Helper.shared.convertDateFormate(value : requestDetail["schedule_date_time"] as? String ?? "0")
            let pickUpAddress = requestDetail["pickup_info"] as! Dictionary<String, Any>
            
            cell.lblPickUpAddress?.text = pickUpAddress["address"] as? String ?? ""
            
            let dropAddress = requestDetail["drop_info"] as! [Dictionary<String, Any>]
            cell.lblDropOffName?.text = "Drop Off location"
            if dropAddress.count > 1
            {
                cell.lblDropOffName?.text = "Drop Off Location (\(dropAddress.count) Stops)"
            }
            else {
                cell.lblDropOffName?.text = "Drop Off Location"
            }
            cell.lblDropAddress?.text = (dropAddress[dropAddress.count - 1] )["address"] as? String ?? ""
           
           
            if vehicalType  != "" {
             
                 cell.lblVehicalType?.text = "DStar - \(vehicalType)"
                 if vehicalType  == "motorcycle"
                 {
                     cell.imgRuneer?.image =  UIImage(named : "scooter")
                 }
                 else if vehicalType == "car"
                 {
                     cell.imgRuneer?.image =  UIImage(named : "car")
                 }
                 else
                 {
                     cell.imgRuneer?.image =  UIImage(named : "bike")
                 }
             }
             else
             {
                 cell.lblVehicalType?.text = "No Runner Assign"
                 cell.imgRuneer?.image =  UIImage(named : "footerLogo")
             }
            let status = requestDetail["status"] as? String ?? ""
            cell.btnStatus?.setTitle(status, for: .normal)
            cell.btnCancel?.tag = indexPath.row
            cell.btnCancel?.addTarget(self, action: #selector(self.onCancelledJob(_:)), for: .touchUpInside)
            return cell
        }
        else
        {
            let cell : CurrentOrderCell = tableView.dequeueReusableCell(withIdentifier: "CurrentOrderCell1", for: indexPath) as! CurrentOrderCell
            let vehicalType = requestDetail["transport_category"] as? String ?? ""
            cell.lblServiceName?.text = requestDetail["service_name"] as? String ?? ""
            cell.lblVehicalType?.text = "DStar - \(vehicalType)"
            cell.lblJobDate?.text = requestDetail["service_name"] as? String ?? ""
            cell.lblPicUpName?.text = "Queue Location"
             cell.lblJobDate?.text =  Helper.shared.convertDateFormate(value : requestDetail["schedule_date_time"] as? String ?? "0")
            let pickUpAddress = requestDetail["pickup_info"] as! Dictionary<String, Any>
            cell.lblPickUpAddress?.text = pickUpAddress["address"] as? String ?? ""
           // cell.imgRuneer?.image =  UIImage(named : "bike")
           

            if vehicalType  != "" {
             
                 cell.lblVehicalType?.text = "DStar - \(vehicalType)"
                 if vehicalType  == "motorcycle"
                 {
                     cell.imgRuneer?.image =  UIImage(named : "scooter")
                 }
                 else if vehicalType == "car"
                 {
                     cell.imgRuneer?.image =  UIImage(named : "car")
                 }
                 else
                 {
                     cell.imgRuneer?.image =  UIImage(named : "bike")
                 }
             }
             else
             {
                 cell.lblVehicalType?.text = "No Runner Assign"
                 cell.imgRuneer?.image =  UIImage(named : "footerLogo")
             }
            let status = requestDetail["status"] as? String ?? ""
            cell.btnStatus?.setTitle(status, for: .normal)
            
            cell.btnCancel?.tag = indexPath.row
            cell.btnCancel?.addTarget(self, action: #selector(self.onCancelledJob(_:)), for: .touchUpInside)
            return cell
        }
            
    }
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
           let requestDetail = self.arrProcessingList[indexPath.row] as! [String : Any]
           let requestType = requestDetail["service_name"] as? String ?? ""
           if requestType != "Help Me Queue"
           {
               return 205.0
           }
           else
           {
               return 150.0
           }
       }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let requestDetail = self.arrProcessingList[indexPath.row] as! [String : Any]
          
        if requestDetail["service_name"] as? String ?? "" != "Help Me Queue"
        {
            let profileVC = RATING_SB.instantiateViewController(withIdentifier: "RideDetailVC") as! RideDetailVC
            profileVC.request_id = (requestDetail["request_id"] as? String)!
            self.navigationController?.pushViewController(profileVC, animated: true)
        }
        else
        {
            let profileVC = RATING_SB.instantiateViewController(withIdentifier: "RideHMQDetail") as! RideHMQDetail
            profileVC.request_id = (requestDetail["request_id"] as? String)!
            self.navigationController?.pushViewController(profileVC, animated: true)
        }
    }
}
