//
//  HomeVC.swift
//  DRunnerCustomer
//
//  Created by mac306 on 17/07/20.
//  Copyright © 2020 mac306. All rights reserved.
//

import UIKit


class HomeVC: UIViewController {
    
    @IBOutlet weak var tblData : UITableView?
    var userData = defaults.object(forKey: udUserInfo) as! Dictionary<String,Any>
    var newsData  : Array = [Dictionary<String,Any>()]
    var homeData = Dictionary<String,Any>()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblData?.tableFooterView = UIView()
        self.newsData.removeAll()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.getUserProfile()
        self.callHomeApi()
        self.navigationController?.navigationBar.isHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    override func viewDidAppear(_ animated: Bool) {
        
        guard kNotificationObject.count > 0 else {
            return
        }
        if (kNotificationObject["service_type"] as? String) != "Help Me Queue" {
            
            let profileVC = RATING_SB.instantiateViewController(withIdentifier: "RideDetailVC") as! RideDetailVC
            profileVC.request_id = kNotificationObject["request_id"] as? String ?? ""
            self.navigationController?.pushViewController(profileVC, animated: true)
        }
        else {
            let profileVC = RATING_SB.instantiateViewController(withIdentifier: "RideHMQDetail") as! RideHMQDetail
            profileVC.request_id = kNotificationObject["request_id"] as? String ?? ""
            self.navigationController?.pushViewController(profileVC, animated: true)
        }

    }
    
    @objc func onBooking(_ sender : UIButton)
    {
        if sender.tag == 2
        {
              let profileVC = BOOKTASK_SB.instantiateViewController(withIdentifier: "HMQBookingVC") as! HMQBookingVC
            self.navigationController?.pushViewController(profileVC, animated: true)
        }
        else
        {
            let profileVC = BOOKTASK_SB.instantiateViewController(withIdentifier: "ServiceBookingVC") as! ServiceBookingVC
            profileVC.serviceType = sender.tag
            self.navigationController?.pushViewController(profileVC, animated: true)
        }
    }
    
    func callHomeApi()
    {
        API_HELPER.sharedInstance.postDataInJsonUsingNS(header: K.ApiName.Home , withParameter: ["user_id" : Helper.shared.getUserID()], inVC: self, showHud: true, addAuthHeader: true) { (resultData, message, status) in
            if status
            {
                self.homeData = resultData as! [String : Any]
                self.newsData = resultData["news_data"] as! Array
                DispatchQueue.main.async {
                    self.tblData?.reloadData()
                }
            }
        }
    }
    
    func getUserProfile()
    {
        API_HELPER.sharedInstance.postDataInJsonUsingNS(header: K.ApiName.Profile, withParameter: ["user_id": Helper.shared.getUserID()], inVC: self, showHud: false, addAuthHeader: true) { (resultData, message, status) in
            DispatchQueue.main.async {
                if !status
                {
                    Helper.shared.showAlert(msg: message)
               }
                else{
                    let dict : NSMutableDictionary = APP_DELEGATE.convertAllDictionaryValueToNil(resultData.mutableCopy() as! NSMutableDictionary) as! NSMutableDictionary
                    defaults.set(dict, forKey: udUserInfo)
                    self.userData = defaults.object(forKey: udUserInfo) as! Dictionary<String,Any>
                    Helper.shared.setUserPoints(value: dict["reward_points"] as? String ?? "0")
                    Helper.shared.setUserWallet(value: dict["wallet_amount"] as? String ?? "0")
                }
            }
        }
    }
  
    @objc func onWalletButton(_ sender : UIButton)
    {

        let profileVC = PAYMENT_SB.instantiateViewController(withIdentifier: "WalletVC") as! WalletVC
        self.navigationController?.pushViewController(profileVC, animated: true)
    }

    @objc func onPointsButton(_ sender : UIButton)
    {
        let profileVC = POINTS_SB.instantiateViewController(withIdentifier: "PointsVC") as! PointsVC
        self.navigationController?.pushViewController(profileVC, animated: true)
    }
}
extension HomeVC: UITableViewDelegate,UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3

    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 2
        {
            return (self.view.frame.size.height / 3)
        }
        if indexPath.row == 1
        {
            return self.view.frame.size.height / 3.7
        }
        return 160.0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0
        {
            let cell : HomeTableCell = tableView.dequeueReusableCell(withIdentifier: "HomeTableCellTop", for: indexPath) as! HomeTableCell
            cell.lblName?.text = "Welcome, \(userData["first_name"] as? String ?? "") \(userData["last_name"] as? String ?? "")"
            cell.lblPoints?.text = "\(Helper.shared.getUserPoints()) Points"
            cell.lblWallet?.text = Helper.shared.getUserWallet()
            cell.imgPoints?.image = Helper.shared.getRankingImage(userRank: userData["ranking"] as? String ?? "")
            cell.btnPoints?.addTarget(self, action: #selector(self.onPointsButton(_:)), for: .touchUpInside)
            cell.btnWallet?.addTarget(self, action: #selector(self.onWalletButton(_:)), for: .touchUpInside)
            return cell
        }
        else if indexPath.row == 1
        {
            let cell : HomeTableCell = tableView.dequeueReusableCell(withIdentifier: "HomeTableCellMiddle", for: indexPath) as! HomeTableCell
            cell.clView?.dataSource = self
            cell.clView?.delegate = self
            cell.clView?.tag = indexPath.row
            cell.clView?.reloadData()
            return cell
        }
        else{
            let cell : HomeTableCell = tableView.dequeueReusableCell(withIdentifier: "HomeTableCellLast", for: indexPath) as! HomeTableCell
            cell.clView?.dataSource = self
            cell.clView?.delegate = self
            cell.clView?.tag = indexPath.row
            cell.clView?.reloadData()
            return cell
        }
        
    }
}
extension HomeVC: UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 1
        {
            return 3
        }
        else
        {
            return self.newsData.count
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView.tag == 1
        {
            let cell : HomeCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionCell", for: indexPath) as! HomeCollectionCell
            
            if indexPath.row == 0
            {
                cell.lblServiceName?.text = "Help Me Buy"
                
                cell.imgService?.image = UIImage(named: "iconeone")
            }
            else if indexPath.row == 1
            {
                cell.lblServiceName?.text = "Help Me Send"
                cell.imgService?.image = UIImage(named: "icontwo")
            }
            else
            {
                cell.lblServiceName?.text = "Help Me Queue"
                cell.imgService?.image = UIImage(named: "iconthree")
            }
            
            cell.btnBook?.tag = indexPath.row
            cell.btnBook?.addTarget(self, action: #selector(onBooking(_:)), for: .touchUpInside)
            return cell
        }
        else{
            let cell : HomeCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionCellLast", for: indexPath) as! HomeCollectionCell
            
            cell.lblServiceName?.text = self.newsData[indexPath.item]["title"] as? String ?? ""
            
            DispatchQueue.global(qos: .background).async {
                
                let des = self.newsData[indexPath.item]["description"] as? String ?? ""
                let htmlText = des.htmlToAttributedString
                DispatchQueue.main.async {
                    cell.lblDescription?.attributedText = htmlText
                    cell.lblDescription?.font = UIFont(name: "Poppins-Light", size: 11.0)
                    cell.lblDescription?.textColor = .darkGray
                   }
            }
            if self.newsData[indexPath.item]["image"] as? String ?? "" != ""
            {
                cell.imgService?.loadImageUsingCache(withUrl: self.newsData[indexPath.item]["image"] as? String ?? "")
            }
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.tag != 1
        {
            
            let profileVC = RATING_SB.instantiateViewController(withIdentifier: "NewsDetailVC") as! NewsDetailVC
            profileVC.newzDetail = self.newsData[indexPath.item]
            self.navigationController?.pushViewController(profileVC, animated: true)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView.tag == 1
        {
            return CGSize.init(width: collectionView.frame.size.width - 40 , height: (self.view.frame.size.height / 3.7) - 30)
        }
        else
        {
            return CGSize.init(width: collectionView.frame.size.width / 1.7 , height: (self.view.frame.size.height / 3.7))
        }
        
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt SECTION: Int) -> CGFloat {
        return 8.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8.0
    }
}
