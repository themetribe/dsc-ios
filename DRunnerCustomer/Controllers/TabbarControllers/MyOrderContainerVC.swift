//
//  MyOrderVC.swift
//  DRunnerCustomer
//
//  Created by mac306 on 17/07/20.
//  Copyright © 2020 mac306. All rights reserved.
//

import UIKit

class MyOrderContainerVC: UIViewController {

    @IBOutlet var btnCurrent: UIButton?
    @IBOutlet var btnFuture: UIButton?
    @IBOutlet var btnCompleted: UIButton?
    
    @IBOutlet var containView: UIView?
    
    var pageViewController: UIPageViewController?
       var viewControllers: [UIViewController]?
       var currentpage = 0
       
    override func viewDidLoad() {
        super.viewDidLoad()
       self.setupPageController()
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
       
    }
    
    
    private func setupPageController() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
            self.pageViewController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
         self.pageViewController?.delegate = self
            self.viewControllers = [
                HOME_SB.instantiateViewController(withIdentifier: "InProgressListVC"),
                HOME_SB.instantiateViewController(withIdentifier: "FutureListVC"),
                HOME_SB.instantiateViewController(withIdentifier: "CompletedListVC")
               ]
            
         self.pageViewController?.setViewControllers([ self.viewControllerAtIndex(0)!], direction: .forward, animated: true, completion: nil)
         self.pageViewController?.dataSource = self
         if let scrollView =  self.pageViewController?.view.subviews.filter({$0.isKind(of: UIScrollView.self)}).first as? UIScrollView {
              scrollView.backgroundColor = UIColor.clear
                scrollView.isScrollEnabled = false
            }
            
          self.pageViewController!.view.frame = self.containView?.frame as! CGRect
         self.addChild(self.pageViewController!)
          
          self.view?.addSubview((self.pageViewController?.view!)!)
         
          self.view?.gestureRecognizers = self.pageViewController?.gestureRecognizers
        }
       
    }
    
    
    
//
//    func setButtonUI(ctPage : Int)
//      {
//        self.btnFuture?.backgroundColor = AppColor
//        self.btnFuture?.setTitleColor(.black, for: .normal)
//        self.btnCurrent?.backgroundColor = AppColor
//        self.btnCurrent?.setTitleColor(.black, for: .normal)
//        self.btnCompleted?.backgroundColor = AppColor
//        self.btnCompleted?.setTitleColor(.black, for: .normal)
//          if ctPage == 0
//          {
//            self.btnCurrent?.backgroundColor = .black
//            self.btnCurrent?.setTitleColor(AppColor, for: .normal)
//
//          }
//          else if ctPage == 1
//          {
//            self.btnFuture?.backgroundColor = .black
//            self.btnFuture?.setTitleColor(AppColor, for: .normal)
//
//          }
//          else{
//            self.btnCompleted?.backgroundColor = .black
//            self.btnCompleted?.setTitleColor(AppColor, for: .normal)
//        }
//      }
//
//    @IBAction func onCurrentButton(_ sender: Any) {
//
//
//                if currentpage == 0{
//                    return
//                }
//                else{
//
//                     self.changePage(.reverse)
//                     currentpage = 0
//                  }
//                 self.handlePageChange()
//                 self.setButtonUI(ctPage: currentpage)
//            }
//    @IBAction func onFuturButton(_ sender: Any) {
//
//        if currentpage == 1{
//                   return
//                }
//                else{
//                    currentpage = 1
//
//                    if currentpage == 0
//                    {
//                     self.changePage(.forward);
//                    }
//                     else{
//                         self.changePage(.reverse);
//                    }
//
//                 }
//                 self.handlePageChange()
//                 self.setButtonUI(ctPage: currentpage)
//             }
//
//
//    @IBAction func onCompletedButton(_ sender: Any) {
//
//            // self.setButtonUI(ctPage: 1)
//                if currentpage == 2{
//                   return
//                }
//                else{
//
//                     self.changePage(.forward);
//                    currentpage = 2
//                 }
//                 self.handlePageChange()
//                 self.setButtonUI(ctPage: currentpage)
//             }
    
    
    @IBAction func onSegmentC ( _ sender : UISegmentedControl)
     {
         if sender.selectedSegmentIndex == 0
                {
                    if self.currentpage != 0{
                        self.currentpage = 0
                        self.changePage(.reverse)
                        
                    }
                }
         else if sender.selectedSegmentIndex == 1
            {
                if self.currentpage != 1
                {
                    self.currentpage = 1
                    if self.currentpage == 0
                    {
                        self.changePage(.reverse)
                    }
                    else{
                        self.changePage(.forward)
                    }
                }
            }
                else
                {
                    if self.currentpage != 2{
                        self.currentpage = 2
                        self.changePage(.forward)
                        
                    }
                }
     }
}
extension MyOrderContainerVC: UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        var index = indexOfViewController(viewController)
        
        if (index == 0) || (index == NSNotFound) {
            return nil
        }
        
        index -= 1
        
        return viewControllerAtIndex(index)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        var index = indexOfViewController(viewController)
        
        if index == NSNotFound {
            return nil
        }
        
        index += 1
        
        if index == viewControllers?.count {
            return nil
        }
        
        return viewControllerAtIndex(index)
    }
    
    
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        guard let indexPage = viewControllers?.firstIndex(of: (pageViewController.viewControllers?.first)!) else{
            return
        }
        print("current page=== ",indexPage)
        currentpage = indexPage
        self.handlePageChange()
        
    }
    
}

// MARK: - Helpers
extension MyOrderContainerVC {
    func changePage(_ direction: UIPageViewController.NavigationDirection) {
        let viewController = viewControllerAtIndex(currentpage)
        if viewController == nil {
            return
        }
        pageViewController?.setViewControllers([viewController!], direction: direction, animated: true, completion: nil)
    }
    
    @objc fileprivate func handlePageChange(){
        
        if currentpage == 0{
            
        }else if currentpage == 1{
            
        }else{
            
        }
        DispatchQueue.main.async {
            
            
        }
    }
    
    fileprivate func viewControllerAtIndex(_ index: Int) -> UIViewController? {
        if viewControllers?.count == 0 || index >= viewControllers!.count {
            return nil
        }
        
        return viewControllers?[index]
    }
    
    fileprivate func indexOfViewController(_ viewController: UIViewController) -> Int {
        return viewControllers?.firstIndex(of: viewController) ?? NSNotFound
    }
}
