//
//  ServiceBookingVC.swift
//  DRunnerCustomer
//
//  Created by mac306 on 01/09/20.
//  Copyright © 2020 mac306. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import GoogleMaps
import Alamofire
import GooglePlaces

class ServiceBookingVC: UIViewController, GMSMapViewDelegate,UITextViewDelegate {
    
    @IBOutlet weak var tblData : UITableView?
    
    
    @IBOutlet weak var tblVehical : UITableView?
    @IBOutlet weak var viewVehical : UIView?
    @IBOutlet weak var viewVehicalSub : UIView?
    @IBOutlet weak var imgLocation : UIImageView?
    @IBOutlet weak var btnSchedule : UIButton?
    @IBOutlet weak var btnUrgent : UIButton?
    @IBOutlet weak var lblTotalPrice : UILabel?
    @IBOutlet weak var viewConfirm : UIView?
    @IBOutlet weak var lblScheduleTime : UILabel?
    @IBOutlet weak var lblWallet : UILabel?
    @IBOutlet weak var lblPromo : UILabel?
    @IBOutlet weak var viewAfterPromo : UIView?
    @IBOutlet weak var lblBeforePromo : UILabel?
    @IBOutlet weak var lblAfterPromo : UILabel?
    @IBOutlet weak var mapV : GMSMapView?
    @IBOutlet weak var lblCategory : UILabel?
    @IBOutlet weak var lblWeight : UILabel?
    @IBOutlet weak var btnItemWeight : UIButton?
    @IBOutlet weak var btnItemcategory : UIButton?
    @IBOutlet weak var viewItemCategory : UIView?
    @IBOutlet weak var viewItemWeight : UIView?
    @IBOutlet weak var viewItemDetail : UIView?
    @IBOutlet weak var lblTxtItemDetail : UILabel?
    @IBOutlet weak var txtDetail : UITextView?
    @IBOutlet weak var lblLocation : UILabel?
    @IBOutlet var chkHelpMeBuy: CheckBox!
    @IBOutlet weak var lblItemIsWithin : UILabel?
    
    @IBOutlet weak var btn_dlvr_now: UIButton!
    
    @IBOutlet weak var btn_dlvr_ltr: UIButton!
    
    @IBOutlet weak var scroll_Booking: UIScrollView!
    
    var selectedCategory : Int = 0
    var selectedItemWeight : Int = 0
    var address : String = ""
    var pincodes : String = ""
    var state : String = ""
    var pincode : String = ""
    var city : String = ""
    var country : String = ""
    var recieptype  : Int = 0
    var userAddressLat : String = "0.0"
    var userAddressLong : String = "0.0"
    var selectedRow : Int = 0
    var categoryList : Array = [Dictionary<String,Any>()]
    var timer : Timer!
    
    var isEdit : Bool = false
    
    var runner_id : String = ""
    var serviceData : Dictionary = Dictionary<String,Any>()
    var locationData : Array = [Dictionary<String,Any>()]
    var serviceType : Int = 0
    var itemWeigth : String = "0"
    var zone_id : String = ""
    var itemCategoryData : Dictionary = Dictionary<String,Any>()
    var itemDetail : String = ""
    var vehicalData : Array = [Dictionary<String,Any>()]
    var selectedVehical : Int = 0
    var isdeliveryTypeSchedule : Int = 0
    var scheduleTime : String = ""
    
    var promoDetail : Dictionary = ["offer_id":"","offer_type":""]
    var pickUpLocation = ["name" : "","contact_number" : "" ,"address" : "", "latitude" : "" , "longitude" : "","landmark":"", "status" :"pending"]
    
    var dropUpLocation = ["name" : "","contact_number" : "" ,"address" : "","landmark" :"", "latitude" : "" , "longitude" : "", "status" :"pending"]

    var requestData : Dictionary  = Dictionary<String,Any>()
    
 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblBeforePromo?.text = Helper.shared.getUserWallet()
        self.txtDetail?.text = "Add your item details as follow: Item, Brand, Quantity, Instruction"
        self.txtDetail?.textColor = UIColor.lightGray
        
        self.txtDetail?.delegate = self
        
        self.txtDetail?.layer.borderWidth = 1.0;
        self.txtDetail?.layer.borderColor =  UIColor.lightGray.cgColor
        
       
        
        
        self.viewVehical?.alpha = 0
        self.viewVehicalSub?.alpha = 0
        chkHelpMeBuy.style = .tick
        chkHelpMeBuy.borderStyle = .roundedSquare(radius: 2.24)
        chkHelpMeBuy.layer.cornerRadius = 5.0;
        
        self.lblItemIsWithin?.text = "Item’s value do not exceed SGD 100"
        
        
        self.imgLocation?.bringSubviewToFront(self.mapV!)
        
        let camera = GMSCameraPosition.camera(withLatitude: APP_DELEGATE.UserLocation.latitude, longitude: APP_DELEGATE.UserLocation.longitude, zoom: 20.0)
        self.mapV?.camera = camera
        self.mapV?.delegate = self
        
        self.mapV?.settings.consumesGesturesInView = false
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(self.panHandler(_:)))
        self.mapV?.addGestureRecognizer(panGesture)
        
        self.initData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.getCategoryData()
        }
        
        
        
        
        
        
        
        
        
        
        self.locationData.removeAll()
        self.locationData.append(pickUpLocation)
        self.locationData.append(dropUpLocation)
       
        self.viewAfterPromo?.alpha = 0.0
        
        let pickup_info = self.locationData[0]
        var drop_info : Array = [Dictionary<String,Any>]()
        for i in 0..<locationData.count
        {
            if i != 0
            {
                drop_info.append(self.locationData[i])
            }
        }
        
        requestData = ["runner_id":runner_id,"user_id":Helper.shared.getUserID(),"zone_id" : zone_id,"service_id": (self.serviceType == 0 ? ("1") : ("2")),"product_category_id" : self.itemCategoryData["id"] as? String ?? "","item_detail" : "","pickup_info" : pickup_info,"drop_info" : drop_info,"transport_category_id" : self.vehicalData[self.selectedVehical]["transport_category_id"] as? String ?? "0","request_type" : self.isdeliveryTypeSchedule == 0 ? ("now") : ("schedule"),"schedule_date_time":self.scheduleTime,"distance":self.vehicalData[self.selectedVehical]["distance"] as? String ?? "","weight" : itemWeigth,"offer_type" : self.promoDetail["offer_type"] ?? "" ,"offer_id" : self.promoDetail["offer_id"] ?? ""] as [String : Any]
        
        
        //self.title = (self.serviceType == 0 ? ("Help Me Buy") : ("Help Me Send"))
        
        if self.serviceType == 0
        {
            self.title = "Help Me Buy"
            self.btnUrgent?.setTitle(" Buy now", for: .normal)
            self.btnSchedule?.setTitle(" Buy later", for: .normal)
            self.btnUrgent?.setTitle(" Buy now", for: .selected)
            self.btnSchedule?.setTitle(" Buy later", for: .selected)
            
           
            
            
            self.chkHelpMeBuy.isHidden = false
            self.lblItemIsWithin?.isHidden = false
        }
        else
        {
            self.title = "Help Me Send"
            
            self.chkHelpMeBuy.isHidden = true
            self.lblItemIsWithin?.isHidden = true
            
            self.chkHelpMeBuy.isChecked = true
//            self.btnUrgent?.setTitle("Queue now", for: .normal)
//            self.btnSchedule?.setTitle("Queue Later", for: .normal)
        }
       
        self.tblData?.tableFooterView = UIView()
        
        
       
        
        NotificationCenter.default.addObserver(forName: NotificatioName.userLocation, object: nil, queue: .main) { (notification) in
            
            let userInfo = notification.userInfo
            
            let locationDetail = ["name" : userInfo?["name"] as? String ?? "", "contact_number" : userInfo?["contact_number"] as? String ?? "" ,"address" : userInfo?["address"] as? String ?? "","landmark" :userInfo?["landmark"] as? String ?? "", "latitude" : userInfo?["latitude"] as? String ?? "" , "longitude" : userInfo?["longitude"] as? String ?? "","status" :"pending"]
            
            let receiverType = userInfo?["recieptype"] as? Int ?? 0
            let isEdit = userInfo?["isEdit"] as! Bool
            //let selectedRow =  userInfo?["selectedRow"] as! Int
            if receiverType == 0
            {
                self.requestData["weight"] = userInfo?["weight"] as? String ?? ""
                //self.itemCategoryData = userInfo?["category"] as! Dictionary
                self.locationData[0]  = locationDetail
                self.requestData["item_detail"] = userInfo?["itemDetail"] as? String ?? ""
                 self.requestData["zone_id"] = userInfo?["zone_id"] as? String ?? ""
            }
            else
            {
               
                if isEdit || receiverType == 1
                {
                   
                    self.locationData[receiverType]  = locationDetail
                }
                else
                {
                    self.locationData.append(locationDetail)
                }
                
            }
            
            
            
            DispatchQueue.main.async {
                self.tblData?.reloadData()
            }
            
            
        }
         NotificationCenter.default.addObserver(forName: NotificatioName.appliedCoupons, object: nil, queue: .main) { (notification) in
                    let userInfo = notification.userInfo
                   self.promoDetail["offer_id"] = userInfo?["offer_id"] as? String ?? ""
                   self.promoDetail["offer_type"] = userInfo?["offer_type"] as? String ?? ""
                   self.viewAfterPromo?.alpha = 1.0
                   self.lblPromo?.text = "Coupon Applied"
                   self.lblBeforePromo?.text = "S$\(userInfo?["amount"] as? String ?? "")"
                   self.lblAfterPromo?.text = "S$\(userInfo?["remaining_amount"] as? String ?? "")"
               }
        
        self.btnUrgent?.isSelected = true
              self.isdeliveryTypeSchedule = 0
              self.scheduleTime = ""
              self.lblScheduleTime?.alpha = 0
              self.lblWallet?.text = Helper.shared.getUserWallet()
        // Do any additional setup after loading the view.
        
        
        
        
        
      
        
      
        
        
//        if tableView == self.tblData
//        {
//            let locationDetail = self.locationData[1]
//            if locationDetail["latitude"] as? String ?? "" == ""
//            {
//                return self.locationData.count
//            }
//            else
//            {
//                return 2
//                //return self.locationData.count + 1
//            }
//        }
//        else
//        {
//            return self.vehicalData.count
//        }
    }
    
    func initData()
    {
        
        if isEdit
        {
            if self.selectedItemWeight < 1
            {
                self.lblWeight?.text = "Less than 1 kg"
            }
            else{
                self.lblWeight?.text = "\(self.selectedItemWeight) Kg"
            }
            
            let lat : Double = Double(userAddressLat)!
            let lng : Double = Double(self.userAddressLong)!
            self.txtDetail?.textColor = .black
            self.txtDetail?.text = itemDetail
            
            let camera = GMSCameraPosition.camera(withLatitude: lat, longitude:  lng, zoom: 20.0)
            self.mapV?.camera = camera
            
            self.lblCategory?.text = self.itemCategoryData["name"] as? String ?? ""
        }
        else
        {
            
        }
        
        
        
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Add your item details as follow: Item, Brand, Quantity, Instruction"
            textView.textColor = UIColor.lightGray
        }
    }
    
    
    func getCategoryData()
    {
        let param = ["user_id": Helper.shared.getUserID(), "service_id": (self.serviceType == 0 ? ("1") : ("2"))] as [String : Any]
        
        API_HELPER.sharedInstance.postDataInJsonUsingNS(header: K.ApiName.category, withParameter: param as NSDictionary, inVC: self, showHud: true, addAuthHeader: true) { (result, messge, status) in
            DispatchQueue.main.async {
                if status
                {
                    self.categoryList = result["data"] as! [[String : Any]]
                    
                }
            }
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
      
        print("self.locationData.count \(self.locationData.count)")
        if self.locationData.count > 1 {
            let locationDetail = self.locationData[1]
            
            print("locationDetail[latitude]  \(locationDetail["latitude"] as? String ?? "")")
            
            
            
        }
    }
    
    @IBAction func onBackButton( _ sender : UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onItemCategory( _ sender : UIButton)
    {
        
       
        var itemCategoryName = [String]()
        for item in self.categoryList
        {
            itemCategoryName.append(item["name"] as? String ?? "")
        }
        
        ActionSheetStringPicker.show(withTitle: "Select product category", rows: itemCategoryName, initialSelection: 0, doneBlock: {
            picker, value, index in
            
            self.lblCategory?.text = itemCategoryName[value]
            self.selectedCategory = value
            self.itemCategoryData = self.categoryList[value]
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
    }
    
    @IBAction func onItemWeight( _ sender : UIButton)
    {
        let itemWeight = ["Less than 1 kg", "1 kg","2 kg","3 kg","4 kg","5 kg","6 kg","7 kg","8 kg","9 kg","10 kg","11 kg","12 kg","13 kg","14 kg","15 kg","16 kg","17 kg","18 kg","19 kg","20 kg",]
        
        
        ActionSheetStringPicker.show(withTitle: "Select item weight", rows: itemWeight, initialSelection: 0, doneBlock: {
            picker, value, index in
            
            self.lblWeight?.text = itemWeight[value]
            self.selectedItemWeight = value
            
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
    }
    
    @IBAction func onSubViewHIde( _ sender : UIButton)
    {
        self.viewVehical?.alpha = 0.0
        self.viewVehicalSub?.alpha = 0.0
        self.viewConfirm?.alpha = 0.0
        self.vehicalData.removeAll()
        self.lblPromo?.text = "Promo"
        self.viewAfterPromo?.alpha = 0.0
        self.promoDetail["offer_id"] =  ""
        self.promoDetail["offer_type"] =  ""
    }
    @IBAction func onVehicalProceesed( _ sender : UIButton)
    {
        if vehicalData.count > 0
        {
            self.viewVehicalSub?.alpha = 0.0
            self.viewConfirm?.alpha = 1.0
            self.lblTotalPrice?.text = "S$ \(self.vehicalData[self.selectedVehical]["price"] as? String ?? "")"
        }
        else
        {
            self.showToast("Please select a vehical for job request.")
        }
    }
    @IBAction func onPricePayment(_ sender : UIButton)
    {
        let profileVC = STATIC_SB.instantiateViewController(withIdentifier: "StaticPagesVC") as! StaticPagesVC
        profileVC.urlString = Price_Payment
        self.navigationController?.pushViewController(profileVC, animated: true)
    }
    
    @IBAction func onUrgentDeliveryType(_ sender : UIButton)
    {
        self.btnUrgent?.isSelected = true
        self.btnSchedule?.isSelected = false
        self.isdeliveryTypeSchedule = 0
        self.scheduleTime = ""
        self.lblScheduleTime?.alpha = 0
    }
    
    @IBAction func onSchdeulDeliveryType(_ sender : UIButton)
    {

       
        
        let date = Date().addingTimeInterval(TimeInterval(2.0 * 60.0 * 60.0))

       
            let datePicker = ActionSheetDatePicker(title: "Date", datePickerMode: UIDatePicker.Mode.dateAndTime, selectedDate: date, doneBlock: {
                    picker, value, index in
                self.btnUrgent?.isSelected = false
                       self.btnSchedule?.isSelected = true
                       self.isdeliveryTypeSchedule = 1
                       
                       self.lblScheduleTime?.alpha = 1
                    //print(value)
                    let formatter = DateFormatter()
                 
                    formatter.dateFormat = "yyyy-MM-dd hh:mm:ss a"
                    formatter.timeZone = NSTimeZone.local
                    
                    self.lblScheduleTime?.text = "Delivery schedule :\(formatter.string(from: value! as! Date))"
                
                
                    formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                    self.scheduleTime = formatter.string(from: value! as! Date)
                    return
                }, cancel: { ActionStringCancelBlock in return }, origin: (sender as AnyObject).superview!?.superview)
            datePicker?.minimumDate = Date()
        
                datePicker?.show()
      
    }
    @IBAction func onConfirProceesed1( _ sender : UIButton)
    {
        let pickup_info = self.locationData[0]
        
        var drop_info : Array = [Dictionary<String,Any>]()
        for i in 0..<locationData.count
        {
            if i != 0
            {
                drop_info.append(self.locationData[i])
            }
        }
        
        let param = [ "runner_id" : runner_id,
                      "user_id" : Helper.shared.getUserID(),
                      "zone_id" : self.requestData["zone_id"] as? String ?? "",
                      "service_id": (self.serviceType == 0 ? ("1") : ("2")),
                      "product_category_id" : self.itemCategoryData["id"] as? String ?? "",
                      "item_detail" : self.requestData["item_detail"] as? String ?? "",
                      "pickup_info" : pickup_info,
                      "drop_info" : drop_info,
                      "transport_category_id" : self.vehicalData[self.selectedVehical]["transport_category_id"] as? String ?? "0",
                      "request_type" : self.isdeliveryTypeSchedule == 0 ? ("now") : ("schedule"),
                      "schedule_date_time":self.scheduleTime,
                      "distance":self.vehicalData[self.selectedVehical]["distance"] as? String ?? "",
                      "weight" : self.requestData["weight"] as? String ?? "",
                      "offer_type" : self.promoDetail["offer_type"] ?? "" ,
                      "offer_id" : self.promoDetail["offer_id"] ?? ""] as [String : Any]
                
//        let waitingVC = BOOKTASK_SB.instantiateViewController(identifier: "WaitingRequiestVC") as! WaitingRequiestVC
//        waitingVC.requestData = param
//        waitingVC.price = "\(self.vehicalData[self.selectedVehical]["price"] as? String ?? "")"
//        self.navigationController?.pushViewController(waitingVC, animated: true)
        self.requestForJob(param: param)
    }
    
    @IBAction func btn_Next_click(_ sender: UIButton) {
        
        self.viewVehical?.alpha = 1
        self.viewVehicalSub?.alpha = 0
        self.viewConfirm?.alpha = 1
    }
    @IBAction func btn_close_vehical_sub_click(_ sender: UIButton) {
        
        self.viewVehical?.alpha = 0
        self.viewVehicalSub?.alpha = 0
    }
    @objc func requestForJob(param : Dictionary<String,Any>)
        {
        
   //(K.ApiName.QueueRequestSend)
            API_HELPER.sharedInstance.postDataInJsonUsingNS(header: K.ApiName.requestJob, withParameter: param as NSDictionary, inVC: self, showHud: true, addAuthHeader: true) { (result, message, status) in
                DispatchQueue.main.async {
                    if status
                    {
                        let requestId = result["request_id"] as? String ?? ""
                        let profileVC = RATING_SB.instantiateViewController(withIdentifier: "RideDetailVC") as! RideDetailVC
                        profileVC.request_id = requestId
                        self.navigationController?.pushViewController(profileVC, animated: true)
                    }
                    else
                    {
                        self.showToast(message)
                    }
                }
                
            }
        }
    
    
    @IBAction func btn_deliver_now_click(_ sender: UIButton) {
        
        self.btn_dlvr_now.setImage(UIImage(named : "radiobuttonselect"), for: .normal);
        
        self.btn_dlvr_ltr.setImage(UIImage(named : "radiobuttonunselect"), for: .normal);
    }
    
    @IBAction func btn_deliver_ltr_click(_ sender: UIButton) {
        
        self.btn_dlvr_now.setImage(UIImage(named : "radiobuttonunselect"), for: .normal);
        
        self.btn_dlvr_ltr.setImage(UIImage(named : "radiobuttonselect"), for: .normal);
        
        
        
        
    }
    
    @IBAction func btn_confirm_book_click(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btn_close_confirm_view_click(_ sender: UIButton) {
        
        self.viewVehical?.alpha = 0.0
        self.viewVehicalSub?.alpha = 0.0
        self.viewConfirm?.alpha = 0.0
    }
    @IBAction func onProcessedButton(_ sender : UIButton)
    {
        self.viewVehical?.alpha = 0.0
        self.viewVehicalSub?.alpha = 0.0
        self.viewConfirm?.alpha = 0.0
        
       // self.viewVehical?.frame = CGRect.init(x: 0, y: self.view.frame.height-self.viewVehical?.frame.size.height ?? 200, width: self.view.frame.width, height: self.viewVehical?.frame.size.height ?? 200)
        
        //self.viewVehicalSub?.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.width, height: self.viewVehicalSub?.frame.size.height ?? 200)
        
     //   self.viewConfirm?.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.width, height: self.viewConfirm?.frame.size.height ?? 200)
        
        if self.locationData[0]["latitude"] as? String ?? "" == ""
        {
            self.showToast("Please enter buying/pick up location.")
            return
        }
        
        if self.locationData[1]["latitude"] as? String ?? "" == ""
        {
            self.showToast("Please enter drop off location.")
            return
        }
        if self.lblCategory?.text?.trim() == "Item category"
        {
            self.showToast(AppMessages.CATEGORY_BLANK)
            return
        }
        if self.lblWeight?.text?.trim() == "Item weight"
        {
            self.showToast(AppMessages.WEIGHT_BLANK)
            return
        }
        if !self.chkHelpMeBuy.isChecked {
            self.showToast("Please check Item’s value do not exceed SGD 100")
            return
        }
        
        let pickup_info = self.locationData[0]
        
        var drop_info : Array = [Dictionary<String,Any>]()
        for i in 0..<locationData.count
        {
            if i != 0
            {
                drop_info.append(self.locationData[i])
            }
        }
        
        let param = ["user_id" : Helper.shared.getUserID(), "weight" : "1" ,"pickup_info" :  pickup_info , "drop_info" : drop_info, "runner_id" : runner_id] as [String : Any]
        
        
        API_HELPER.sharedInstance.postDataInJsonUsingNS(header: K.ApiName.checkVehicalPrice, withParameter: param as NSDictionary, inVC: self, showHud: true, addAuthHeader: true) { (result, message, status) in
            DispatchQueue.main.async {
                
                if status
                {
                   // self.showToast("data is coming")
                    self.vehicalData = result["data"] as! [[String : Any]]
                    self.viewVehical?.alpha = 1.0
                    self.viewVehicalSub?.alpha = 1.0
                    self.tblVehical?.reloadData()
                    
                        //self.showToast(message)
                }
                else
                {
                    self.showToast(message)
                }
                self.lblAfterPromo?.text =  "S$\(self.vehicalData[0]["price"] as? String ?? "")" ;
            }
            
        }
    }
    
    @IBAction func onPromoCode(_ sender : UIButton)
    {
        DispatchQueue.main.async {
            let waitingVC = BOOKTASK_SB.instantiateViewController(identifier: "PromoListVC") as! PromoListVC
            waitingVC.amount = "\(self.vehicalData[self.selectedVehical]["price"] as? String ?? "")"
            self.navigationController?.pushViewController(waitingVC, animated: true)
        }
        
    }
   

}
extension ServiceBookingVC : UITableViewDelegate,UITableViewDataSource
{
    @objc private func panHandler(_ pan : UIPanGestureRecognizer){
        
        if timer != nil {
            timer.invalidate()
        }
        
        if pan.state == .ended {
            let mapSize = self.mapV?.frame.size
            let point = CGPoint(x: mapSize?.width ?? 0.0/2, y: mapSize?.height ?? 0.0/2)
            let newCoordinate = self.mapV?.projection.coordinate(for: point)
            //print(newCoordinate)
            
            timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: false) { timer in
                self.checkZoneAvailability(lat: (newCoordinate?.latitude.description)!, long:  (newCoordinate?.longitude.description)!, isSearchResult: false)
            }
        }
    }
    
    func checkZoneAvailability(lat : String , long : String, isSearchResult : Bool)
    {
        API_HELPER.sharedInstance.postDataInJsonUsingNS(header: K.ApiName.checkLocation, withParameter: ["user_id" : Helper.shared.getUserID() , "latitude" : lat , "longitude" : long], inVC: self, showHud: true, addAuthHeader: true) { (result, message, status) in
            
            DispatchQueue.main.async {
                if status
                {
                    self.userAddressLat = lat
                    self.userAddressLong = long
                    self.zone_id = result["zone_id"] as? String ?? ""
                    if !isSearchResult
                    {
                        
                    }
                    
                }
                else
                {
                    self.userAddressLat = ""
                    self.userAddressLong = ""
                    self.zone_id = ""
                    self.showToast(message)
                    self.lblLocation?.text = ""
                }
            }
            
        }
        
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        if (gesture){
            // print("dragged")
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.tblData
        {
            let locationDetail = self.locationData[1]
            if locationDetail["latitude"] as? String ?? "" == ""
            {
                let height  = tableView.frame.origin.y + 120 + 8
                if scroll_Booking != nil {
                    scroll_Booking.frame = CGRect.init(x: scroll_Booking.frame.origin.x, y: height, width: scroll_Booking.frame.size.width, height: scroll_Booking.frame.size.height)
                }
                
                return self.locationData.count
            }
            else
            {
               
                    
                
                
                return 2
                //return self.locationData.count + 1
            }
        }
        else
        {
            return self.vehicalData.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == self.tblData
        {
        
            let cell : ServiceBookingCell = self.tblData?.dequeueReusableCell(withIdentifier: "ServiceBookingCell", for: indexPath) as! ServiceBookingCell
        
            
            if indexPath.row == self.locationData.count
            {
                
                cell.lblTxt?.text = "Add another drop location"
                cell.lblTxt?.alpha = 1.0
                cell.lblTitle?.alpha = 0.0
                cell.lblAddress?.alpha = 0.0
                 cell.imageLocation?.setImageColor(color: .red)
            }
            else
            {
                if indexPath.row != 0
                {
                    cell.imageLocation?.setImageColor(color: .red)
                }
                
                let locationDetail = self.locationData[indexPath.row]
                if locationDetail["latitude"] as? String ?? "" == ""
                {
                    cell.lblTxt?.alpha = 1.0
                    cell.lblTitle?.alpha = 0.0
                    cell.lblAddress?.alpha = 0.0
            
                    if indexPath.row == 0
                    {
                        cell.lblTxt?.text = (self.serviceType == 0 ? ("Enter buying location") : ("Enter pick up location"))
                    }
                    else
                    {
                        cell.lblTxt?.text = "Enter drop off location"
                    }
                }
                else
                {
                    cell.lblTxt?.alpha = 0.0
                    cell.lblTitle?.alpha = 1.0
                    cell.lblAddress?.alpha = 1.0
                    cell.lblTitle?.text = locationDetail["name"] as? String ?? ""
                    cell.lblAddress?.text = locationDetail["address"] as? String ?? ""
            
                }
            }
            return cell
        }
        else
        {
            let cell : VehicalCell = tableView.dequeueReusableCell(withIdentifier: "VehicalCell", for: indexPath) as! VehicalCell
            let vehicalD = self.vehicalData[indexPath.row]
            cell.lblSize?.text = "Up to \(vehicalD["size"] as? String ?? ""), \(vehicalD["weight"] as? String ?? "")"
            cell.lblTitle?.text = "\(vehicalD["transport_category_name"] as? String ?? "")"
            cell.lblPrice?.text = "S$ \(vehicalD["price"] as? String ?? "")"
            cell.lblTime?.text = "\(vehicalD["duration"] as? String ?? "")"
            
            if vehicalD["transport_category_id"] as? String ?? "" == "1"
            {
                cell.imgVehical?.image =  UIImage(named : "scooter")
            }
            else if vehicalD["transport_category_id"] as? String ?? "" == "2"
            {
                cell.imgVehical?.image =  UIImage(named : "car")
            }
            else
            {
                cell.imgVehical?.image =  UIImage(named : "bike")
            }
            
            if indexPath.row == selectedVehical
            {
                cell.imgSelection?.image = UIImage(named : "radiobuttonselect")
            }
            else
            {
                cell.imgSelection?.image = UIImage(named : "radiobuttonunselect")
            }
            
            
            return cell
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //let locationDetail = self.locationData[indexPath.row]
               
        if tableView == self.tblData
        {
            if indexPath.row != 0
             {
                 
                 if self.locationData[0]["address"] as? String ?? "" == ""
                 {
                     self.showToast("Please enter first buying/drop off location.")
                     return
                 }
                 
             }
            
            let profileVC = BOOKTASK_SB.instantiateViewController(withIdentifier: "ServiceBookingAddressVC") as! ServiceBookingAddressVC
                 profileVC.serviceType = self.serviceType
                 profileVC.recieptype = indexPath.row
                if indexPath.row == 0
                {
                    profileVC.locationData = self.locationData[0]
                    if self.locationData[0]["address"] as? String ?? "" != ""
                    {
                        profileVC.isEdit = true
                        profileVC.zone_id = self.requestData["zone_id"] as? String ?? ""
                        profileVC.selectedItemWeight = Int(self.requestData["weight"] as? String ?? "0")!
                        profileVC.itemCategoryData = self.itemCategoryData
                        profileVC.itemDetail = self.requestData["item_detail"] as? String ?? ""
                        
                        profileVC.selectedRow = indexPath.row
                    }
                }
                else
                {
                    if indexPath.row == self.locationData.count - 1
                    {
                        profileVC.locationData = self.locationData[indexPath.row]
                        if self.locationData[indexPath.row]["address"] != nil{
                            if self.locationData[indexPath.row]["address"] as? String ?? "" != ""
                            {
                                profileVC.isEdit = true
                                profileVC.selectedRow = indexPath.row
                            }
                        }
                    }
                   
                }
                
            self.navigationController?.pushViewController(profileVC, animated: true)
            
        }
        else
        {
            self.selectedVehical = indexPath.row
            
            let dict_selectedVehical = vehicalData[indexPath.row];
            
            
            if let price =  dict_selectedVehical["price"]{
                
               // self.lblBeforePromo?.text =  "S$\(price as? String ?? "")" ;
                self.lblAfterPromo?.text =  "S$\(price as? String ?? "")" ;
            }
           
            
            self.tblVehical?.reloadData()
            
        }
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == self.tblVehical
        {
            return 50.0
            
        }
        else
        {
            return 60.0
        }
    }
    
}
