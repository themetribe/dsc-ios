//
//  PaymentConfirmVC.swift
//  DRunnerCustomer
//
//  Created by mac306 on 06/08/20.
//  Copyright © 2020 mac306. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire

class PaymentConfirmVC: UIViewController {

    @IBOutlet weak var mapV : GMSMapView?
    @IBOutlet weak var viewConfirmation : UIView?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let camera = GMSCameraPosition.camera(withLatitude: 26.8549, longitude: 75.8243, zoom: 6.0)
        self.mapV?.camera = camera
        
        self.drawMap(SourceCordinate: CLLocationCoordinate2D(latitude: 26.8549, longitude: 75.8243), destinationcordinate: CLLocationCoordinate2D(latitude: 26.8043, longitude: 75.8561))

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
    
    func drawMap(SourceCordinate : CLLocationCoordinate2D, destinationcordinate :CLLocationCoordinate2D)
        {
            self.mapV?.clear()
            let str = String(format:"https://maps.googleapis.com/maps/api/directions/json?origin=\(SourceCordinate.latitude),\(SourceCordinate.longitude)&destination=\(destinationcordinate.latitude),\(destinationcordinate.longitude)&key=\(kGoogleServiceKey)")
            print(str)
            
            
            var request = URLRequest(url: URL(string: str as String)!)
            request.httpMethod = "POST"
           
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
           
               
            let session = URLSession.shared
            let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
            if response != nil
              {
            //print(response!)
               do {
                let json = try JSONSerialization.jsonObject(with: data!) as! Dictionary<String, AnyObject>
                let routes : NSArray = json["routes"] as! NSArray
                               if(json["status"] as! String == "ZERO_RESULTS"){}
                               else if(json["status"] as! String  == "NOT_FOUND"){}
                               else if routes.count == 0{}
                               else{
                                   let routes : NSArray = json["routes"] as! NSArray
                  
                                   let markerEnd = GMSMarker()
                                   markerEnd.position = CLLocationCoordinate2D(latitude: 26.8549, longitude: 75.8243)
                                   markerEnd.map = self.mapV
                                   let pathv : NSArray = routes.value(forKey: "overview_polyline") as! NSArray
                                   let paths : NSArray = pathv.value(forKey: "points") as! NSArray
                                   let newPath = GMSPath.init(fromEncodedPath: paths[0] as! String)
                                   let polyLine = GMSPolyline(path: newPath)
                                   polyLine.strokeWidth = 5
                                   polyLine.strokeColor =  .black
                                   let ThemeOrange = GMSStrokeStyle.solidColor( .blue)
                                   let OrangeToBlue = GMSStrokeStyle.gradient(from:  .blue, to:  .blue)
                                   polyLine.spans = [GMSStyleSpan(style: ThemeOrange),
                                                     GMSStyleSpan(style: ThemeOrange),
                                                     GMSStyleSpan(style: OrangeToBlue)]
                                   polyLine.map = self.mapV

                               }
                print(json)
                }
                catch {
                 print("error")
                 DispatchQueue.main.async {
                     //Helper.shared.showAlert(msg: Error_MSG)
                    }
                }
                }
             })
             task.resume()
            
        }
    
    @IBAction func onBackButton( _ sender : UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onConfirmButton( _ sender : UIButton)
    {
        let profileVC = HOME_SB.instantiateViewController(withIdentifier: "RunnerLocationVC") as! RunnerLocationVC
        
         self.navigationController?.pushViewController(profileVC, animated: true)
        
    }
    
   
}
