//
//  BookingVC.swift
//  DRunnerCustomer
//
//  Created by mac306 on 29/07/20.
//  Copyright © 2020 mac306. All rights reserved.
//

import UIKit

class BookingProductVC: UIViewController {

    @IBOutlet weak var lblProduct : UILabel?
    @IBOutlet weak var txtQuantity: UITextField?
    @IBOutlet weak var txtNote: UITextView?
    var serviceType : Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = (self.serviceType == 0 ? ("Help Me Buy") : ("Help Me Send"))
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onBackButton( _ sender : UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onProductSelectButton( _ sender : UIButton)
    {
        
    }
    @IBAction func onContinue( _ sender : UIButton)
    {
        let profileVC = HOME_SB.instantiateViewController(withIdentifier: "BookingAddressVC") as! BookingAddressVC
        profileVC.serviceType = self.serviceType
        self.navigationController?.pushViewController(profileVC, animated: true)
    }

}
