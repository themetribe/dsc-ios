//
//  PromoListVC.swift
//  DRunnerCustomer
//
//  Created by mac306 on 21/09/20.
//  Copyright © 2020 mac306. All rights reserved.
//

import UIKit

class PromoListVC: UIViewController {
    
    @IBOutlet weak var tblCouponList : UITableView?
    @IBOutlet weak var txtCoupon : UITextField?
    
    var arrCouponCode = Array<Any>()
    var arrVoucherCode = Array<Any>()
    
    var amount : String = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tblCouponList?.tableFooterView = UIView()
            self.getPromoList()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onBackButton(_ sender : UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func getPromoList()
    {
        API_HELPER.sharedInstance.postDataInJsonUsingNS(header: K.ApiName.OfferList , withParameter: ["user_id" : Helper.shared.getUserID(), "amount":amount], inVC: self, showHud: true, addAuthHeader: true) { (result, message, status) in
            DispatchQueue.main.async {
                if status
                {
                    self.arrCouponCode = result["coupon_code"] as! Array<Any>
                    self.arrVoucherCode = result["voucher_code"] as! Array<Any>
                    self.tblCouponList?.reloadData()
                    
                    if self.arrVoucherCode.count > 0 || self.self.arrCouponCode.count > 0
                    {
                        self.tblCouponList?.reloadData()
                    }
                    else
                    {
                        self.tblCouponList?.isHidden = true
                    }
                }
                else
                {
                    self.showToast(message)
                }
            }
            
        }
    }
    
    @IBAction func onApplytxtCoupon(_ sender : UIButton)
    {
        
    }
    
    @objc func onApplyButtton(_ sender : TwoTagButton)
    {
        var detailData : Dictionary = Dictionary<String,Any>()
        if sender.section == 0
        {
            detailData = self.arrCouponCode[sender.row!] as! [String : Any]
        }
        else
        {
            detailData = self.arrVoucherCode[sender.row!] as! [String : Any]
        }
        
        API_HELPER.sharedInstance.postDataInJsonUsingNS(header: K.ApiName.CheckOffer , withParameter: ["user_id" : Helper.shared.getUserID(), "amount":amount,"offer_type":detailData["offer_type"] as? String ?? "","offer_id" : detailData["offer_id"] as? String ?? ""], inVC: self, showHud: true, addAuthHeader: true) { (result, message, status) in
            DispatchQueue.main.async {
                if status
                {
                    let userD =  ["offer_type" : detailData["offer_type"] as? String ?? "","offer_amount" : result["offer_amount"] as? String ?? "", "remaining_amount" : result["remaining_amount"] as? String ?? "","amount" : result["amount"] as? String ?? "" , "offer_id" : detailData["offer_id"] as? String ?? ""] as [String : Any]
                    
                    // NotificationCenter.default.post(name: Notification.Name("APPLIED_COUPON"), object: nil, userInfo: userD)
                    NotificationCenter.default.post(name: NotificatioName.appliedCoupons, object: nil, userInfo: userD)
                    DispatchQueue.main.async {
                        self.navigationController?.popViewController(animated: true)
                    }
                    
                }
                else
                {
                    self.showToast(message)
                }
            }
            
        }
    }
    
}

extension PromoListVC : UITableViewDelegate, UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0
        {
            return self.arrCouponCode.count
        }
        else
        {
            return self.arrVoucherCode.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : PromoCell = self.tblCouponList?.dequeueReusableCell(withIdentifier: "PromoCell", for: indexPath) as! PromoCell
        cell.selectionStyle  = .none
        
        var detailData : Dictionary = Dictionary<String,Any>()
        
        if indexPath.section == 0
        {
            detailData = self.arrCouponCode[indexPath.row] as! [String : Any]
            cell.lblOfferAmount?.text = "S$\(detailData["offer_amount"] as? String ?? "") Off on min job amount \(detailData["min_order_amount"] as? String ?? "")"
        }
        else
        {
            detailData = self.arrVoucherCode[indexPath.row] as! [String : Any]
            cell.lblOfferAmount?.text = "Flat S$\(detailData["offer_amount"] as? String ?? "") Off"
        }
        cell.lblPromoCode?.text = detailData["offer_code"] as? String ?? ""
        cell.btnApply?.section = indexPath.section
        cell.btnApply?.row = indexPath.row
        cell.btnApply?.addTarget(self, action: #selector(self.onApplyButtton(_:)), for: .touchUpInside)
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var detailData : Dictionary = Dictionary<String,Any>()
        if indexPath.section == 0
        {
            detailData = self.arrCouponCode[indexPath.row] as! [String : Any]
            let profileVC = RATING_SB.instantiateViewController(withIdentifier: "NewsDetailVC") as! NewsDetailVC
            profileVC.newzDetail = ["description" : detailData["coupon_description"] as? String ?? "", "image" : detailData["offer_image"] as? String ?? "", "title" : detailData["offer_code"] as? String ?? ""]
            self.navigationController?.pushViewController(profileVC, animated: true)
            
        }
        else
        {
            detailData = self.arrVoucherCode[indexPath.row] as! [String : Any]
            
        }
        
        
        
    }
}

