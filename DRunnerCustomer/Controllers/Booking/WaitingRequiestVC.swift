//
//  WaitingRequiestVC.swift
//  DRunnerCustomer
//
//  Created by mac306 on 02/09/20.
//  Copyright © 2020 mac306. All rights reserved.
//

import UIKit
import GoogleMaps

class WaitingRequiestVC: UIViewController {

    var requestData : Dictionary  = Dictionary<String,Any>()
    var price : String = ""
    @IBOutlet weak var mapV : GMSMapView?
    var requestId : String = ""
    @IBOutlet weak var lblPickup : UILabel?
    @IBOutlet weak var lblDrop : UILabel?
    @IBOutlet weak var lblPrice : UILabel?
    var isHMQ : Bool = false
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
            let pickup_info = self.requestData["pickup_info"] as! [String : Any]
            self.lblPickup?.text = pickup_info["address"] as? String ?? ""
        
        if isHMQ == false
        {
        let dropArray = self.requestData["drop_info"] as! Array<Dictionary<String,Any>>
        
        self.lblDrop?.text = dropArray[0]["address"] as? String ?? ""
        }
        self.lblPrice?.text = "Total fare   S$ \(price)"
        
        
        let camera = GMSCameraPosition.camera(withLatitude: Double(pickup_info["latitude"] as? String ?? "0.0")!, longitude: Double(pickup_info["longitude"] as? String ?? "0.0")!, zoom: 16.0)
        self.mapV?.camera = camera
        
        let position = CLLocationCoordinate2DMake(Double(pickup_info["latitude"] as? String ?? "0.0")!, Double(pickup_info["longitude"] as? String ?? "0.0")!)
        
        let marker = GMSMarker(position: position)
        
        marker.map = self.mapV
        
         self.requestForJob()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
      
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    @IBAction func onBackButton(_ sender : UIButton)
    {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @objc func requestForJob()
    {
//        DispatchQueue.main.async {
//            LOADER.show(views: self.view)
//        }
        API_HELPER.sharedInstance.postDataInJsonUsingNS(header: self.isHMQ == true ? (K.ApiName.QueueRequestSend) : (K.ApiName.requestJob), withParameter: self.requestData as NSDictionary, inVC: self, showHud: true, addAuthHeader: true) { (result, message, status) in
            DispatchQueue.main.async {
                self.requestId = result["request_id"] as? String ?? ""
//                if status
//                {
//                    self.vehicalData = result["data"] as! [[String : Any]]
//                    self.viewVehical?.alpha = 1.0
//                }
//                else
//                {
//                    self.showToast(message)
//                }
            }
            
        }
    }
    
    @IBAction func onCancelButton(_ sender : UIButton)
    {
        let waitingVC = BOOKTASK_SB.instantiateViewController(identifier: "CancelRequestVC") as! CancelRequestVC
        waitingVC.request_id = self.requestId
        self.navigationController?.pushViewController(waitingVC, animated: true)
    }
    
    

  

}
