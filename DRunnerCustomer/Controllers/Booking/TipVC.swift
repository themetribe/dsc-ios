//
//  TipVC.swift
//  DRunnerCustomer
//
//  Created by admin on 09/12/20.
//  Copyright © 2020 mac306. All rights reserved.
//

import UIKit

class TipVC: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var collectionTip : UICollectionView?
    @IBOutlet weak var txtAmount : TextField?
    var selectionTip : Int = -100
    var tipAmount : String = ""
    var request_id : String = ""

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onBackButton ( _ sender : UIButton)
    {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    
    @IBAction func btnSubmit(_ sender : Any) {
        
        
        self.tipAmount = txtAmount?.text?.replacingOccurrences(of: "S$ ", with: "") ?? "0"
        
        guard Double(self.tipAmount) ?? 0 >= 0.50 else {
            self.showToast("Sorry the minimum amount to be raised is S$ 0.50")
            return
        }
        
        let param = ["request_id" : request_id, "tip" : self.tipAmount ,"user_id" : Helper.shared.getUserID()]
        
        API_HELPER.sharedInstance.postDataInJsonUsingNS(header: K.ApiName.giveTip, withParameter: param as NSDictionary, inVC: self, showHud: true, addAuthHeader: true) { (result, message, status) in
            DispatchQueue.main.async {
                if status
                {
                    self.showSingleButtonAlert(title: kAppName, message: message) {
                        self.navigationController?.popToRootViewController(animated: true)
                    }
                }
                else
                {
                    self.showToast(message)
                }
            }
        }
        
    }
    
    @IBAction func txtfieldChanged(_ textField: UITextField) {
        tipAmount = textField.text ?? ""
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let allowedCharacters = CharacterSet(charactersIn:"0123456789.")//Here change this characters based on your requirement
        let characterSet = CharacterSet(charactersIn: string)
        return allowedCharacters.isSuperset(of: characterSet)
        
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension TipVC : UICollectionViewDelegate , UICollectionViewDataSource
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell : TipCollectioCell = collectionView.dequeueReusableCell(withReuseIdentifier: "TipCollectioCell", for: indexPath) as! TipCollectioCell
        if indexPath.row == 0
        {
            cell.lblTip?.text = "S$ 5"
        }
        else if indexPath.row == 1
        {
            cell.lblTip?.text = "S$ 10"
        }
        else{
            cell.lblTip?.text = "S$ 15"
        }
        if self.selectionTip == indexPath.row
        {
            cell.lblTip?.layer.borderColor = UIColor.green.cgColor
        }
        else
        {
            cell.lblTip?.layer.borderColor = UIColor.clear.cgColor
        }
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.item == self.selectionTip
        {
            self.selectionTip = -100
            self.tipAmount = ""
        }
        else
        {
            if indexPath.item == 0
            {
                self.tipAmount = "5"
            }
            else if indexPath.item == 1
            {
                self.tipAmount = "10"
            }
            else
            {
                self.tipAmount = "15"
            }
            self.selectionTip = indexPath.item
        }
        
        txtAmount?.text = "S$ \(self.tipAmount)"
        collectionView.reloadData()
        
    }
}
