//
//  HMQBookingVC.swift
//  DRunnerCustomer
//
//  Created by mac306 on 23/09/20.
//  Copyright © 2020 mac306. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire
import ActionSheetPicker_3_0
import GooglePlaces

class HMQBookingVC: UIViewController, UITextViewDelegate {
    
    @IBOutlet weak var mapV : GMSMapView?
    @IBOutlet weak var imgLocation : UIImageView?
    @IBOutlet weak var lblLocation : UILabel?
    @IBOutlet weak var txtLocationDetail : UITextField?
    @IBOutlet weak var txtPlaceName : UITextField?
    @IBOutlet weak var txtLandmark : UITextField?
    @IBOutlet weak var txtDetail : UITextView?
    @IBOutlet weak var lblScheduleTime : UILabel?
    @IBOutlet weak var lblWallet : UILabel?
    @IBOutlet weak var lblPromo : UILabel?
    @IBOutlet weak var btnSchedule : UIButton?
    @IBOutlet weak var btnUrgent : UIButton?
    @IBOutlet weak var lblTotalPrice : UILabel?
    @IBOutlet weak var lblQueueTime : UILabel?
    @IBOutlet weak var viewPrice : UIView?
    @IBOutlet weak var viewSubPrice : UIView?
    @IBOutlet weak var viewAfterPromo : UIView?
    @IBOutlet weak var lblBeforePromo : UILabel?
    @IBOutlet weak var lblAfterPromo : UILabel?
    
    
    var address : String = ""
    var pincodes : String = ""
    var state : String = ""
    var pincode : String = ""
    var city : String = ""
    var country : String = ""
    var userAddressLat : String = "1.2822"
    var userAddressLong : String = "103.8505"
    var zone_id : String = ""
    var queueTime : Int = 0
    var runner_id : String = ""
    var serviceData : Dictionary = Dictionary<String,Any>()
    var isdeliveryTypeSchedule : Int = 0
    var scheduleTime : String = ""
    var pickUpLocation = ["address" : "", "latitude" : "" , "longitude" : ""]
    var offer_id = ""
    var offer_type = ""
    var amount : Float = 0.0
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.imgLocation?.bringSubviewToFront(self.mapV!)
        
        let camera = GMSCameraPosition.camera(withLatitude: APP_DELEGATE.UserLocation.latitude, longitude: APP_DELEGATE.UserLocation.longitude, zoom: 20.0)
        self.mapV?.camera = camera
        self.mapV?.delegate = self
        
        self.mapV?.settings.consumesGesturesInView = false
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(self.panHandler(_:)))
        self.mapV?.addGestureRecognizer(panGesture)
        
        self.btnUrgent?.isSelected = true
        self.btnSchedule?.isSelected = false
        
        
        self.isdeliveryTypeSchedule = 0
        self.scheduleTime = ""
        self.lblScheduleTime?.alpha = 0
        DispatchQueue.main.async {
            self.checkZoneAvailability(lat: "\(APP_DELEGATE.UserLocation.latitude)", long: "\(APP_DELEGATE.UserLocation.longitude)")
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(reloadData), name: NOTIFICATION_GPS_DATA, object: nil)
        
        NotificationCenter.default.addObserver(forName: NotificatioName.appliedCoupons, object: nil, queue: .main) { (notification) in
            let userInfo = notification.userInfo
            self.offer_id = userInfo?["offer_id"] as? String ?? ""
            self.offer_type = userInfo?["offer_type"] as? String ?? ""
            self.viewAfterPromo?.alpha = 1.0
            self.lblPromo?.text = "Coupon Applied"
            self.lblBeforePromo?.text = "S$\(userInfo?["amount"] as? String ?? "")"
            self.lblAfterPromo?.text = "S$\(userInfo?["remaining_amount"] as? String ?? "")"
        }
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.lblWallet?.text = Helper.shared.getUserWallet()
    }
    
   
    @objc private func reloadData() {
        
        print("Location Get-->",APP_DELEGATE.UserLocation.latitude)
        self.mapV?.clear()
        
        let camera = GMSCameraPosition.camera(withLatitude: APP_DELEGATE.UserLocation.latitude, longitude: APP_DELEGATE.UserLocation.longitude, zoom: 20.0)
        self.mapV?.camera = camera
        
        DispatchQueue.main.async {
            self.checkZoneAvailability(lat: "\(APP_DELEGATE.UserLocation.latitude)", long: "\(APP_DELEGATE.UserLocation.longitude)")
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NOTIFICATION_GPS_DATA, object: nil)
    }
    
    @IBAction func onBackButton( _ sender : UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onClosePriceViewButton( _ sender : UIButton)
    {
        self.viewPrice?.isHidden = true
        self.viewAfterPromo?.alpha = 0.0
    }
    
    @IBAction func onSelectTime( _ sender : UIButton)
    {
        let date = Date()
        
        let datePicker = ActionSheetDatePicker(title: "Time", datePickerMode: UIDatePicker.Mode.countDownTimer, selectedDate: date, doneBlock: {
            picker, duration, origin in
            let hours = duration as! Int / 60 / 60
            let minut = (duration as! Int / 60) % 60
            self.lblQueueTime?.text = String(format : "Queue Time : %d hours %d min",hours,minut)
            self.queueTime = duration as! Int / 60
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: (sender as AnyObject).superview!?.superview)
        datePicker?.minimumDate = Date()
        datePicker?.show()
    }
    @IBAction func onUrgentDeliveryType(_ sender : UIButton)
    {
        self.btnUrgent?.isSelected = true
        self.btnSchedule?.isSelected = false
        self.isdeliveryTypeSchedule = 0
        self.scheduleTime = ""
        self.lblScheduleTime?.alpha = 0
    }
    @IBAction func onSchdeulDeliveryType(_ sender : UIButton)
    {
        let date = Date().addingTimeInterval(TimeInterval(2.0 * 60.0 * 60.0))
        
        let datePicker = ActionSheetDatePicker(title: "Date", datePickerMode: UIDatePicker.Mode.dateAndTime, selectedDate: date, doneBlock: {
            picker, value, index in
            
            let formatter = DateFormatter()
            
            
            formatter.dateFormat = "yyyy-MM-dd hh:mm:ss a"
            formatter.timeZone = NSTimeZone.local
            
            self.lblScheduleTime?.text = "Queue schedule :\(formatter.string(from: value! as! Date))"
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            self.scheduleTime = formatter.string(from: value! as! Date)
            //                    formatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
            //                    formatter.timeZone = NSTimeZone.local
            //                    self.scheduleTime = formatter.string(from: value! as! Date)
            //
            //
            //                    formatter.dateFormat = "yyyy-MM-dd hh:mm:ss a"
            //
            // self.lblScheduleTime?.text = "Queue schedule :\(formatter.string(from: value! as! Date))"
            self.btnUrgent?.isSelected = false
            self.btnSchedule?.isSelected = true
            self.isdeliveryTypeSchedule = 1
            self.lblScheduleTime?.alpha = 1
            
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            self.scheduleTime = formatter.string(from: value! as! Date)
            
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: (sender as AnyObject).superview!?.superview)
        datePicker?.minimumDate = Date()

        datePicker?.show()
        
    }
    
    @IBAction func onPromoCode(_ sender : UIButton)
    {
        DispatchQueue.main.async {
            let waitingVC = BOOKTASK_SB.instantiateViewController(identifier: "PromoListVC") as! PromoListVC
            waitingVC.amount = "\(self.amount)"
            
            self.navigationController?.pushViewController(waitingVC, animated: true)
        }
        
    }
    
    @IBAction func autocompleteClicked(_ sender: UIButton) {
        let autocompletecontroller = GMSAutocompleteViewController()
        autocompletecontroller.delegate = self
        let filter = GMSAutocompleteFilter()
        filter.type = .noFilter  //suitable filter type
        filter.country = "SG"  //appropriate country code
        autocompletecontroller.autocompleteFilter = filter
        self.present(autocompletecontroller, animated: true, completion: nil)
        
    }
    
    
    @IBAction func onQueueReuest(_ sender : UIButton)
    {
        if self.isValidField()
        {
            let param = [   "user_id" : Helper.shared.getUserID(),
                            "duration" : self.queueTime
            ] as [String : Any]
            
            API_HELPER.sharedInstance.postDataInJsonUsingNS(header: K.ApiName.PriceCalHMQ, withParameter: param as NSDictionary, inVC: self, showHud: true, addAuthHeader: true) { (result, message, status) in
                DispatchQueue.main.async {
                    if status
                    {
                        self.viewAfterPromo?.alpha = 0.0
                        self.lblTotalPrice?.text = "S$ \(result["amount"] as? String ?? "0.0")"
                        //self.lblBeforePromo?.text = "S$ \(result["amount"] as? String ?? "0.0")"
                        self.amount = Float(result["amount"] as? String ?? "0.0") ?? 0.0
                        self.viewPrice?.isHidden = false
                        
                    }
                    else
                    {
                        self.showToast(message)
                    }
                }
                
            }
        }
    }
    
    @IBAction func onSubmitButton(_ sender : UIButton)
    {
       
        self.pickUpLocation["address"] = "\(address),\(self.txtLocationDetail?.text ?? "")"
        self.pickUpLocation["latitude"] = userAddressLat
        self.pickUpLocation["longitude"] = userAddressLong
        self.pickUpLocation["username"] = self.txtPlaceName?.text ?? ""
        self.pickUpLocation["landmark"] = self.txtLocationDetail?.text ?? ""
        self.pickUpLocation["status"] = "pending"
        
        let param = [ "runner_id" : runner_id,
                      "user_id" : Helper.shared.getUserID(),
                      "zone_id" : zone_id,
                      "item_detail" : self.txtDetail?.text ?? "",
                      "pickup_info" :  self.pickUpLocation,
                      "request_type" : self.isdeliveryTypeSchedule == 0 ? ("now") : ("schedule"),
                      "schedule_date_time":self.scheduleTime,
                      "duration":"\(self.queueTime)",
                      "offer_type" : self.offer_type,
                      "offer_id" : self.offer_id] as [String : Any]
        
        //        let waitingVC = BOOKTASK_SB.instantiateViewController(identifier: "WaitingRequiestVC") as! WaitingRequiestVC
        //        waitingVC.requestData = param
        //        waitingVC.price = "\(self.amount)"
        //        waitingVC.isHMQ = true
        //        self.navigationController?.pushViewController(waitingVC, animated: true)
        
        self.requestForJob(param: param)
    }
    
    @objc func requestForJob(param : Dictionary<String,Any>)
    {
        
        API_HELPER.sharedInstance.postDataInJsonUsingNS(header: K.ApiName.QueueRequestSend, withParameter: param as NSDictionary, inVC: self, showHud: true, addAuthHeader: true) { (result, message, status) in
            DispatchQueue.main.async {
                if status
                {
                    let requestId = result["request_id"] as? String ?? ""
                    let profileVC = RATING_SB.instantiateViewController(withIdentifier: "RideHMQDetail") as! RideHMQDetail
                    profileVC.request_id = requestId
                    self.navigationController?.pushViewController(profileVC, animated: true)
                }
                else
                {
                    self.showToast(message)
                }
            }
            
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView.text == "Enter queue for (Ex. Ticket/Passes etc)"
        {
            textView.text = ""
            textView.textColor = .black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text == ""
        {
            textView.text = "Enter queue for (Ex. Ticket/Passes etc)"
            textView.textColor = .placeholderText
        }
    }
}
extension HMQBookingVC: GMSAutocompleteViewControllerDelegate {
    
    //Enter queue for (Ex. Ticket/Passes etc)
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        self.checkZoneAvailability(lat: "\(place.coordinate.latitude)", long: "\(place.coordinate.longitude)")
        //self.getAddressFromLatLong(latitude: place.coordinate.latitude, longitude: place.coordinate.longitude)
        let camera = GMSCameraPosition.camera(withLatitude: place.coordinate.latitude, longitude: place.coordinate.longitude, zoom: 20.0)
        self.mapV?.camera = camera
        
        dismiss(animated: true, completion: nil)
        
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}
extension HMQBookingVC : GMSMapViewDelegate
{
    @objc private func panHandler(_ pan : UIPanGestureRecognizer){
        
        if pan.state == .ended{
            let mapSize = self.mapV?.frame.size
            let point = CGPoint(x: mapSize?.width ?? 0.0/2, y: mapSize?.height ?? 0.0/2)
            let newCoordinate = self.mapV?.projection.coordinate(for: point)
            //print(newCoordinate)
            self.checkZoneAvailability(lat: (newCoordinate?.latitude.description)!, long:  (newCoordinate?.longitude.description)!)
            //do task here
        }
    }
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        if (gesture){
            // print("dragged")
        }
    }
    
    
    func checkZoneAvailability(lat : String , long : String)
    {
        API_HELPER.sharedInstance.postDataInJsonUsingNS(header: K.ApiName.checkLocation, withParameter: ["user_id" : Helper.shared.getUserID() , "latitude" : lat , "longitude" : long], inVC: self, showHud: true, addAuthHeader: true) { (result, message, status) in
            
            DispatchQueue.main.async {
                if status
                {
                    self.userAddressLat = lat
                    self.userAddressLong = long
                    self.zone_id = result["zone_id"] as? String ?? ""
                    self.getAddressFromLatLong(latitude: Double(lat)!, longitude: Double(long)!)
                }
                else
                {
                    self.userAddressLat = ""
                    self.userAddressLong = ""
                    self.zone_id = ""
                    self.showToast(message)
                }
            }
            
        }
        
    }
    
    
    
    
    func getAddressFromLatLong(latitude: Double, longitude : Double) {
        let url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(latitude),\(longitude)&key=\(kGoogleServiceKey)"
        
        Alamofire.request(url).validate().responseJSON { response in
            switch response.result {
            case .success:
                
                let responseJson = response.result.value! as! NSDictionary
                
                if let results = responseJson.object(forKey: "results")! as? [NSDictionary] {
                    if results.count > 0 {
                        if let addressComponents = results[0]["address_components"]! as? [NSDictionary] {
                            
                            self.address = results[0]["formatted_address"] as? String ?? ""
                            for component in addressComponents {
                                if let temp = component.object(forKey: "types") as? [String] {
                                    if (temp[0] == "postal_code") {
                                        self.pincode = component["long_name"] as? String ?? ""
                                    }
                                    if (temp[0] == "locality") {
                                        self.city = component["long_name"] as? String ?? ""
                                    }
                                    if (temp[0] == "administrative_area_level_1") {
                                        self.state = component["long_name"] as? String ?? ""
                                    }
                                    if (temp[0] == "country") {
                                        self.country = component["long_name"] as? String ?? ""
                                    }
                                }
                            }
                            DispatchQueue.main.async {
                                self.lblLocation?.text = "\(self.address),\(self.city),\(self.state),\(self.country),\(self.pincode)"
                            }
                            
                        }
                    }
                }
            case .failure(let error):
                print(error)
            }
        }
    }
}
extension HMQBookingVC
{
    func isValidField() -> Bool {
        DispatchQueue.main.async {
            self.view.endEditing(true)
        }
        //LOCATION_BLANK
        //   if self.txtPlaceName?.text
        if self.queueTime < 10
        {
            self.showToast(AppMessages.QUEUE_TIME_BLANK)
            return false
        }
        
        
        return true
    }
}
