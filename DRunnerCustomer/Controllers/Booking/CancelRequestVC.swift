//
//  CancelRequestVC.swift
//  DRunnerCustomer
//
//  Created by mac306 on 09/09/20.
//  Copyright © 2020 mac306. All rights reserved.
//

import UIKit

class CancelRequestVC: UIViewController, UITableViewDelegate,UITableViewDataSource, UITextViewDelegate {

    @IBOutlet weak var tblList : UITableView?
    @IBOutlet weak var viewOther : UIView?
    @IBOutlet weak var txtReason : UITextView?
    
    var request_id : String = ""
    var reason : String = ""
    let arrayReason = ["Runner was taking longer than expected.","Couldn’t find runner.","Incorrect location.","Runner ask to cancel.","Wanted to book a different runner.","Other"]
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewOther?.isHidden = true
        tblList?.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        
    }

    @IBAction func onBackButton (_ sender : UIButton)
    {
       self.navigationController?.popToRootViewController(animated: true)
    }
    @IBAction func onCancelRequest( _ sender : UIButton)
    {
        self.cancelRequest()
        
    }
    
     @objc func onOpenOtherView()
     {
        DispatchQueue.main.async {
        self.viewOther?.isHidden = false
        }
    }
    
    @IBAction func onCloseOtherView( _ sender : UIButton)
     {
        DispatchQueue.main.async {
        self.viewOther?.isHidden = true
            self.view.endEditing(true)
        }
        
    }
    
    
    
    func cancelRequest()
    {
        if self.reason == ""
        {
            Helper.shared.showAlert(msg: "Please select a reason for cancel this request.")
            return
        }
        API_HELPER.sharedInstance.postDataInJsonUsingNS(header: K.ApiName.CancelRequest, withParameter: ["user_id" : Helper.shared.getUserID(), "request_id": request_id, "reason" :reason], inVC: self, showHud: true, addAuthHeader: true) { (result, message, status) in
            DispatchQueue.main.async {
                if status
                {
                    self.showSingleButtonAlert(title: kAppName, message: message) {
                
                        self.navigationController?.popToRootViewController(animated: true)
                    }
                }
                else
                {
                    self.showToast(message)
                }
            }
                
        }
    }
    
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayReason.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : CancelReuqestCell = tableView.dequeueReusableCell(withIdentifier: "CancelReuqestCell", for: indexPath) as! CancelReuqestCell
        cell.lblTitle?.text = self.arrayReason[indexPath.row] as? String ?? ""
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.reason = self.arrayReason[indexPath.row]
        if indexPath.row == self.arrayReason.count - 1
        {
            self.onOpenOtherView()
//            let alert = UIAlertController(title: "", message: "Please enter your reason", preferredStyle: .alert)
//
//                   //2. Add the text field. You can configure it however you need.
//                   alert.addTextField { (textField) in
//
//                       textField.placeholder = "Please enter your reason"
//
//                   }
//
//                   alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { [weak alert] (_) in
//                       let textField = alert?.textFields![0] // Force unwrapping because we know it exists.
//                       let name = (textField?.text)?.trimmingCharacters(in: .whitespaces)
//
//                    self.reason = name ?? ""
//                    self.cancelRequest()
//
//                   }))
//
//                   alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { [weak alert] (_) in
//
//                   }))
//
//                   self.present(alert, animated: true, completion: nil)
        }
    }

//MARK: - Textview Delegate
    
    func textViewDidChange(_ textView: UITextView) {
        self.reason = textView.text
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView.text == "Please enter your reason"
        {
            textView.text = ""
            self.txtReason?.textColor = .black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
       
        if textView.text == ""
        {
            textView.text = "Please enter your reason"
            self.txtReason?.textColor = .placeholderText
        }
    }
    
}
