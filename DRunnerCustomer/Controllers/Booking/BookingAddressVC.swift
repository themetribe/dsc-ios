//
//  BookingAddressVC.swift
//  DRunnerCustomer
//
//  Created by mac306 on 29/07/20.
//  Copyright © 2020 mac306. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class BookingAddressVC: UIViewController {
    
 @IBOutlet weak var tblData : UITableView?
    var serviceType : Int = 0
    var isdeliveryTypeSchedule : Int = 0
    var scheduleTime : String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = (self.serviceType == 0 ? ("Help Me Buy") : ("Help Me Send"))
        self.tblData?.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
   
    @IBAction func onBackButton( _ sender : UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    @objc func onSendButton(_ sender : UIButton)
    {
        let profileVC = HOME_SB.instantiateViewController(withIdentifier: "PaymentConfirmVC") as! PaymentConfirmVC
       
        self.navigationController?.pushViewController(profileVC, animated: true)
    }
    
    @objc func onPricePayment(_ sender : UIButton)
    {
        let profileVC = STATIC_SB.instantiateViewController(withIdentifier: "StaticPagesVC") as! StaticPagesVC
        profileVC.urlString = Price_Payment
        self.navigationController?.pushViewController(profileVC, animated: true)
    }

    @objc func onDeliverType(_ sender : UIButton)
    {
        if sender.tag == 0
        {
            self.isdeliveryTypeSchedule = 0
        }
        else
        {
            self.isdeliveryTypeSchedule = 1
        }
        
       
            let datePicker = ActionSheetDatePicker(title: "Date", datePickerMode: UIDatePicker.Mode.dateAndTime, selectedDate: NSDate() as Date?, doneBlock: {
                    picker, value, index in
                    print(value)
                    let formatter = DateFormatter()
                    formatter.dateStyle = .short
                    formatter.timeStyle = .short
                    self.scheduleTime = formatter.string(from: value! as! Date)
                    self.tblData?.reloadRows(at: [IndexPath.init(row: 3, section: 0)], with: .automatic)
                    return
                }, cancel: { ActionStringCancelBlock in return }, origin: (sender as AnyObject).superview!?.superview)
        datePicker?.minimumDate = Date()
        
                datePicker?.show()
      
    }
  

}
extension BookingAddressVC: UITableViewDelegate,UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 3
        {
            return 270.0
        }
        return 70.0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 3
        {
        let cell : DeliveryType = tableView.dequeueReusableCell(withIdentifier: "DeliveryType", for: indexPath) as! DeliveryType
            
            if isdeliveryTypeSchedule == 0
            {
                cell.btnUrgentDelivery?.isSelected = true
                cell.btnScheduleDelivery?.isSelected = false
                cell.lblDeliveryTime?.alpha = 0.0
            }
            else
            {
                cell.lblDeliveryTime?.text = "Delivery schedule : \(self.scheduleTime)"
                cell.lblDeliveryTime?.alpha = 1.0
                cell.btnUrgentDelivery?.isSelected = false
                cell.btnScheduleDelivery?.isSelected = true
            }
            cell.btnUrgentDelivery?.tag = 0
            cell.btnScheduleDelivery?.tag = 1
            cell.btnUrgentDelivery?.addTarget(self, action: #selector(self.onDeliverType(_:)), for: .touchUpInside)
            cell.btnScheduleDelivery?.addTarget(self, action: #selector(self.onDeliverType(_:)), for: .touchUpInside)
            cell.btnViewPrice?.addTarget(self, action: #selector(self.onPricePayment(_:)), for: .touchUpInside)
            cell.btnSend?.addTarget(self, action: #selector(self.onSendButton(_:)), for: .touchUpInside)
        return cell
        }
        else
        {
            let cell : DeliveryLocationCell = tableView.dequeueReusableCell(withIdentifier: "DeliveryLocationCell\(indexPath.row)", for: indexPath) as! DeliveryLocationCell
           
            
            return cell
        }
        
        
    }
}
