//
//  RatingVC.swift
//  DRunnerCustomer
//
//  Created by mac306 on 28/09/20.
//  Copyright © 2020 mac306. All rights reserved.
//

import UIKit

class RatingVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UITextViewDelegate {
    
    @IBOutlet weak var tblRating : UITableView?
    var rating : Int = 1
    var driver_id : String = ""
    var selectionTip : Int = -100
    var tipAmount : String = ""
    var comment : String = ""
    var request_id : String = ""

    var runnerDetail : Dictionary = Dictionary<String,Any>()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func onBackButton ( _ sender : UIButton)
     {
         self.navigationController?.popToRootViewController(animated: true)
     }
    
    @objc func onSubmitButton ( _ sender : UIButton)
    {
        
        self.tipAmount = tipAmount.replacingOccurrences(of: "S$ ", with: "")
        
        let param = ["accept_by" : runnerDetail["runner_id"] as? String ?? "" , "given_by" : Helper.shared.getUserID(), "request_id" : request_id, "tip" : self.tipAmount , "rating" : "\(rating)", "review_title" : "","review" : self.comment ]
        
        API_HELPER.sharedInstance.postDataInJsonUsingNS(header: K.ApiName.Rating, withParameter: param as NSDictionary, inVC: self, showHud: true, addAuthHeader: true) { (result, message, status) in
            DispatchQueue.main.async {
                if status
                {
                    self.showSingleButtonAlert(title: kAppName, message: message) {
                        self.navigationController?.popToRootViewController(animated: true)
                    }
                }
                else
                {
                    self.showToast(message)
                }
            }
        }
         
    }
    @objc func textFieldDidChange(_ textField: UITextField) {
        tipAmount = textField.text ?? ""
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
           
        let allowedCharacters = CharacterSet(charactersIn:"0123456789")//Here change this characters based on your requirement
        let characterSet = CharacterSet(charactersIn: string)
        return allowedCharacters.isSuperset(of: characterSet)
          
          
       }
    func textViewDidChange(_ textView: UITextView) {
        self.comment = textView.text
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 2
        {
            return 450.0
        }
        else
        {
            return 200.0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0
        {
            let cell : RatingTopCell = self.tblRating?.dequeueReusableCell(withIdentifier: "RatingTopCell", for: indexPath) as! RatingTopCell
            cell.viewRating?.ratingDidChange = { ratingValue in
                self.rating = Int(ratingValue)
            }
         //   cell.imgRunner?.loadImageUsingCache(withUrl: self.runnerDetail["runner_image"] as? String ?? "")

            cell.selectionStyle = .none
            return cell
        }
        else if indexPath.row == 1
        {
            let cell : RatingMiddleCell = self.tblRating?.dequeueReusableCell(withIdentifier: "RatingMiddleCell", for: indexPath) as! RatingMiddleCell
            cell.txtAmount?.delegate = self
            cell.clTip?.delegate = self
            cell.clTip?.dataSource = self
            cell.clTip?.tag = 0
            cell.clTip?.reloadData()
            cell.selectionStyle = .none
            if self.tipAmount != ""
            {
                cell.txtAmount?.text = "S$ \(self.tipAmount)"
            }
            else
            {
                cell.txtAmount?.text = ""
            }
            cell.txtAmount?.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
            return cell
        }
        else
        {
            let cell : RatingBottomCell = self.tblRating?.dequeueReusableCell(withIdentifier: "RatingBottomCell", for: indexPath) as! RatingBottomCell
            cell.txtComment?.delegate = self
            cell.selectionStyle = .none
            cell.clImprove?.delegate = self
            cell.clImprove?.dataSource = self
            cell.clImprove?.tag = 1
            cell.clImprove?.reloadData()
            cell.btnSubmit?.addTarget(self, action: #selector(self.onSubmitButton(_:)), for: .touchUpInside)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }

}
extension RatingVC : UICollectionViewDelegate , UICollectionViewDataSource
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 0
        {
            return 3
        }
        else
        {
            return 3
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView.tag == 0
        {
            let cell : TipCollectioCell = collectionView.dequeueReusableCell(withReuseIdentifier: "TipCollectioCell", for: indexPath) as! TipCollectioCell
            if indexPath.row == 0
            {
                cell.lblTip?.text = "S$ 5"
            }
            else if indexPath.row == 1
            {
                cell.lblTip?.text = "S$ 10"
            }
            else{
                cell.lblTip?.text = "S$ 15"
            }
            if self.selectionTip == indexPath.row
            {
                cell.lblTip?.layer.borderColor = UIColor.green.cgColor
            }
            else
            {
                cell.lblTip?.layer.borderColor = UIColor.clear.cgColor
            }
            return cell
        }
        else
        {
            let cell : RateCollectioCell = collectionView.dequeueReusableCell(withReuseIdentifier: "RateCollectioCell", for: indexPath) as! RateCollectioCell
            if indexPath.row == 0
            {
                cell.lblText?.text = "Great Personality"
                cell.imgRate?.image = UIImage.init(named: "gp")
            }
            else if indexPath.row == 1
            {
                cell.lblText?.text = "Helpful"
                cell.imgRate?.image = UIImage.init(named: "helpful")
            }
            else{
                cell.lblText?.text = "Sincere"
                cell.imgRate?.image = UIImage.init(named: "sincere")
            }
        
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.tag == 0
        {
            if indexPath.item == self.selectionTip
            {
                self.selectionTip = -100
                self.tipAmount = ""
            }
            else
            {
                if indexPath.item == 0
                {
                    self.tipAmount = "5"
                }
                else if indexPath.item == 1
                {
                    self.tipAmount = "10"
                }
                else
                {
                    self.tipAmount = "15"
                }
                self.selectionTip = indexPath.item
            }
            collectionView.reloadData()
            self.tblRating?.reloadRows(at: [IndexPath.init(row: 1, section: 0)], with: .automatic)
        }
        else
        {
            
        }
    }
}
class TextField: UITextField {

    let padding = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)

    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
}
