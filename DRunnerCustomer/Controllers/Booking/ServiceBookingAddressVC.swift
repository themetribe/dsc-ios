//
//  ServiceBookingAddressVC.swift
//  DRunnerCustomer
//
//  Created by mac306 on 01/09/20.
//  Copyright © 2020 mac306. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire
import ActionSheetPicker_3_0
import GooglePlaces

class ServiceBookingAddressVC: UIViewController, UITextViewDelegate {
    
    var categoryList : Array = [Dictionary<String,Any>()]
    var itemCategoryData : Dictionary = Dictionary<String,Any>()
    var requestData : Dictionary  = Dictionary<String,Any>()
    @IBOutlet weak var mapV : GMSMapView?
    @IBOutlet weak var imgLocation : UIImageView?
    
    @IBOutlet weak var lblLocation : UILabel?
    @IBOutlet weak var txtLocationDetail : UITextField?
    @IBOutlet weak var txtUserName : UITextField?
    @IBOutlet weak var txtMobileNumber : UITextField?
    @IBOutlet var chkHelpMeBuy: CheckBox!
    @IBOutlet weak var lblItemIsWithin : UILabel?
    @IBOutlet weak var txtShopName : UITextField?
     
    
    @IBOutlet weak var lblCategory : UILabel?
    @IBOutlet weak var lblWeight : UILabel?
    @IBOutlet weak var btnItemWeight : UIButton?
    @IBOutlet weak var btnItemcategory : UIButton?
    @IBOutlet weak var viewItemCategory : UIView?
    @IBOutlet weak var viewItemWeight : UIView?
    @IBOutlet weak var viewItemDetail : UIView?
    @IBOutlet weak var lblTxtItemDetail : UILabel?
    @IBOutlet weak var txtDetail : UITextView?
    @IBOutlet weak var constraint_AddButton_Top: NSLayoutConstraint?
    
    @IBOutlet weak var lbl_senderName: UILabel!
    
    var selectedCategory : Int = 0
    var selectedItemWeight : Int = 0
    var address : String = ""
    var pincodes : String = ""
    var state : String = ""
    var pincode : String = ""
    var city : String = ""
    var country : String = ""
    var serviceType : Int = 0
    var recieptype  : Int = 0
    var userAddressLat : String = "0.0"
    var userAddressLong : String = "0.0"
    var selectedRow : Int = 0
    
    var zone_id : String = ""
    var itemDetail : String = ""
    var timer : Timer!
    var locationData : Dictionary = Dictionary<String,Any>()
    
    var isEdit : Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.serviceType == 0
        {
            if self.recieptype == 0
            {
                self.title = "Buying location"
                self.txtUserName?.placeholder = "Recipient's Name"
                lbl_senderName?.text = "Shop Name"
                self.constraint_AddButton_Top?.constant = 307
                
            }
            else
            {
                self.title = "Drop off location"
                self.constraint_AddButton_Top?.constant = 30
                lbl_senderName?.text = "Recipient's Name"
                txtUserName?.placeholder = "Recipient's Name"
            }
        }
        else
        {
            if self.recieptype == 0
            {
                self.title = "Pick up location"
                self.constraint_AddButton_Top?.constant = 307
                lbl_senderName?.text = "Sender's Name"
                txtUserName?.placeholder = "Sender's Name"
            }
            else
            {
                self.title = "Drop off location"
                self.constraint_AddButton_Top?.constant = 30
                lbl_senderName?.text = "Recipient's Name"
                txtUserName?.placeholder = "Recipient's Name"
            }
        }
        
        
        
        if self.recieptype != 0
        {
            self.viewItemDetail?.alpha = 0.0
            self.viewItemCategory?.alpha = 0.0
            self.viewItemWeight?.alpha = 0.0
            //self.lblTxtItemDetail?.alpha = 0.0
            self.chkHelpMeBuy?.alpha = 0.0
            self.lblItemIsWithin?.alpha = 0.0
            
        }
        
        self.imgLocation?.bringSubviewToFront(self.mapV!)
        
        let camera = GMSCameraPosition.camera(withLatitude: APP_DELEGATE.UserLocation.latitude, longitude: APP_DELEGATE.UserLocation.longitude, zoom: 20.0)
        self.mapV?.camera = camera
        self.mapV?.delegate = self
        
        self.mapV?.settings.consumesGesturesInView = false
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(self.panHandler(_:)))
        self.mapV?.addGestureRecognizer(panGesture)
        
        self.initData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.getCategoryData()
        }
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    
    func initData()
    {
        
        if isEdit
        {
            if self.selectedItemWeight < 1
            {
                self.lblWeight?.text = "Less than 1 kg"
            }
            else{
                self.lblWeight?.text = "\(self.selectedItemWeight) Kg"
            }
            self.lblLocation?.text = locationData["address"] as? String ?? ""
            self.txtMobileNumber?.text = locationData["contact_number"] as? String ?? ""
            self.txtLocationDetail?.text = locationData["landmark"] as? String ?? ""
            self.txtUserName?.text = locationData["name"] as? String ?? ""
            self.userAddressLat = locationData["latitude"] as? String ?? ""
            self.userAddressLong = locationData["longitude"] as? String ?? ""
            let lat : Double = Double(userAddressLat)!
            let lng : Double = Double(self.userAddressLong)!
            self.txtDetail?.textColor = .black
            self.txtDetail?.text = itemDetail
            
            let camera = GMSCameraPosition.camera(withLatitude: lat, longitude:  lng, zoom: 20.0)
            self.mapV?.camera = camera
            
            self.lblCategory?.text = self.itemCategoryData["name"] as? String ?? ""
        }
        else
        {
            self.checkZoneAvailability(lat: "\(APP_DELEGATE.UserLocation.latitude)", long: "\(APP_DELEGATE.UserLocation.longitude)", isSearchResult: false)
        }
        
        
        
    }
    
    func getCategoryData()
    {
        let param = ["user_id": Helper.shared.getUserID(), "service_id": (self.serviceType == 0 ? ("1") : ("2"))] as [String : Any]
        
        API_HELPER.sharedInstance.postDataInJsonUsingNS(header: K.ApiName.category, withParameter: param as NSDictionary, inVC: self, showHud: true, addAuthHeader: true) { (result, messge, status) in
            DispatchQueue.main.async {
                if status
                {
                    self.categoryList = result["data"] as! [[String : Any]]
                    
                }
            }
            
        }
    }
    
    
    
    @IBAction func onBackButton( _ sender : UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onItemCategory( _ sender : UIButton)
    {
        self.showToast("Please category.")
        var itemCategoryName = [String]()
        for item in self.categoryList
        {
            itemCategoryName.append(item["name"] as? String ?? "")
        }
        
        ActionSheetStringPicker.show(withTitle: "Select product category", rows: itemCategoryName, initialSelection: 0, doneBlock: {
            picker, value, index in
            
            self.lblCategory?.text = itemCategoryName[value]
            self.selectedCategory = value
            self.itemCategoryData = self.categoryList[value]
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
    }
    
    @IBAction func onItemWeight( _ sender : UIButton)
    {
        let itemWeight = ["Less than 1 kg", "1 kg","2 kg","3 kg","4 kg","5 kg","6 kg","7 kg","8 kg","9 kg","10 kg","11 kg","12 kg","13 kg","14 kg","15 kg","16 kg","17 kg","18 kg","19 kg","20 kg",]
        
        
        ActionSheetStringPicker.show(withTitle: "Select item weight", rows: itemWeight, initialSelection: 0, doneBlock: {
            picker, value, index in
            
            self.lblWeight?.text = itemWeight[value]
            self.selectedItemWeight = value
            
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
    }
    
    @IBAction func onAddDetail( _ sender : UIButton)
    {
        if isValidField()
        {
            // let category = self.categoryList[self.selectedCategory]
            let data = ["name" :self.txtUserName?.text ?? "", "contact_number" : self.txtMobileNumber?.text ?? "" , "address" : self.lblLocation?.text ?? "", "weight" : "\(self.selectedItemWeight)", "landmark" : self.txtLocationDetail?.text ?? "","latitude" : self.userAddressLat, "longitude" : self.userAddressLong, "recieptype" : self.recieptype, "itemDetail": self.txtDetail?.text ?? "", "zone_id":zone_id, "category" : self.itemCategoryData, "isEdit" : isEdit , "selectedRow" : self.selectedRow,  ] as [String : Any]
            
            NotificationCenter.default.post(name: NotificatioName.userLocation, object: nil, userInfo: data)
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    //Add your item details as follow: Item, Brand, Quantity, Instruction
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView.text == "Add your item details as follow: Item, Brand, Quantity, Instruction"
        {
            textView.text = ""
            textView.textColor = .black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text == ""
        {
            textView.text = "Add your item details as follow: Item, Brand, Quantity, Instruction"
            textView.textColor = .placeholderText
        }
    }
    
    @IBAction func autocompleteClicked(_ sender: UIButton) {
        
        let autocompletecontroller = GMSAutocompleteViewController()
        autocompletecontroller.delegate = self
        let filter = GMSAutocompleteFilter()
        filter.type = .noFilter  //suitable filter type
        filter.country = "SG"  //appropriate country code
        autocompletecontroller.autocompleteFilter = filter
        self.present(autocompletecontroller, animated: true, completion: nil)
        
    }
    
}
extension ServiceBookingAddressVC: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        print("Place name: \(place.name)")
        print("Place address: \(String(describing: place.formattedAddress))")
        self.lblLocation?.text = place.formattedAddress
        self.checkZoneAvailability(lat: "\(place.coordinate.latitude)", long: "\(place.coordinate.longitude)", isSearchResult: true)
        // self.getAddressFromLatLong(latitude: place.coordinate.latitude, longitude: place.coordinate.longitude)
        let camera = GMSCameraPosition.camera(withLatitude: place.coordinate.latitude, longitude: place.coordinate.longitude, zoom: 20.0)
        self.mapV?.camera = camera
        
        dismiss(animated: true, completion: nil)
        
    }
    
    //    private func getFullName() -> String {
    //
    //        return self.city! + ", " + self.state! + ", " + self.country!
    //    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}
extension ServiceBookingAddressVC : GMSMapViewDelegate
{
    @objc private func panHandler(_ pan : UIPanGestureRecognizer){
        
        if timer != nil {
            timer.invalidate()
        }
        
        if pan.state == .ended {
            let mapSize = self.mapV?.frame.size
            let point = CGPoint(x: mapSize?.width ?? 0.0/2, y: mapSize?.height ?? 0.0/2)
            let newCoordinate = self.mapV?.projection.coordinate(for: point)
            //print(newCoordinate)
            
            timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: false) { timer in
                self.checkZoneAvailability(lat: (newCoordinate?.latitude.description)!, long:  (newCoordinate?.longitude.description)!, isSearchResult: false)
            }
        }
    }
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        if (gesture){
            // print("dragged")
        }
    }
    
    //    func mapView(_ mapView: GMSMapView, didEndDragging marker: GMSMarker) {
    //       let latitude = mapView.camera.target.latitude
    //       let longitude = mapView.camera.target.longitude
    //        self.getAddressFromLatLong(latitude: latitude, longitude: longitude)
    //
    //    }
    
    
    func checkZoneAvailability(lat : String , long : String, isSearchResult : Bool)
    {
        API_HELPER.sharedInstance.postDataInJsonUsingNS(header: K.ApiName.checkLocation, withParameter: ["user_id" : Helper.shared.getUserID() , "latitude" : lat , "longitude" : long], inVC: self, showHud: true, addAuthHeader: true) { (result, message, status) in
            
            DispatchQueue.main.async {
                if status
                {
                    self.userAddressLat = lat
                    self.userAddressLong = long
                    self.zone_id = result["zone_id"] as? String ?? ""
                    if !isSearchResult
                    {
                        self.getAddressFromLatLong(latitude: Double(lat)!, longitude: Double(long)!)
                    }
                    
                }
                else
                {
                    self.userAddressLat = ""
                    self.userAddressLong = ""
                    self.zone_id = ""
                    self.showToast(message)
                    self.lblLocation?.text = ""
                }
            }
            
        }
        
    }
    
    

    func getAddressFromLatLong(latitude: Double, longitude : Double) {
        let url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(latitude),\(longitude)&key=\(kGoogleServiceKey)"
        
        Alamofire.request(url).validate().responseJSON { response in
            switch response.result {
            case .success:
                
                let responseJson = response.result.value! as! NSDictionary
                
                if let results = responseJson.object(forKey: "results")! as? [NSDictionary] {
                    if results.count > 0 {
                        if let addressComponents = results[0]["address_components"]! as? [NSDictionary] {
                            
                            self.address = results[0]["formatted_address"] as? String ?? ""
                            for component in addressComponents {
                                if let temp = component.object(forKey: "types") as? [String] {
                                    if (temp[0] == "postal_code") {
                                        self.pincode = component["long_name"] as? String ?? ""
                                    }
                                    if (temp[0] == "locality") {
                                        self.city = component["long_name"] as? String ?? ""
                                    }
                                    if (temp[0] == "administrative_area_level_1") {
                                        self.state = component["long_name"] as? String ?? ""
                                    }
                                    if (temp[0] == "country") {
                                        self.country = component["long_name"] as? String ?? ""
                                    }
                                }
                            }
                            DispatchQueue.main.async {
                                self.lblLocation?.text = "\(self.address),\(self.city),\(self.state),\(self.country),\(self.pincode)"
                            }
                            
                        }
                    }
                }
            case .failure(let error):
                print(error)
            }
        }
    }
}
extension ServiceBookingAddressVC
{
    func isValidField() -> Bool {
        DispatchQueue.main.async {
            self.view.endEditing(true)
        }
        //LOCATION_BLANK
        if self.lblLocation?.text?.trim().count == 0
        {
            self.showToast(AppMessages.LOCATION_BLANK)
            return false
        }
        
        
        
        
       
        if self.txtUserName?.text?.trim().count == 0 {
            self.showToast("Enter shop name")
            return false
        }
        if (self.txtUserName?.text?.trim().count)! < 2   || (self.txtUserName?.text?.trim().count)! > 80 {
            self.showToast(AppMessages.NAME_VALID)
            return false
        }
        
        if self.recieptype != 0
        {
            if self.txtMobileNumber?.text?.trim().count == 0 {
                self.showToast(AppMessages.USERPHONE_BLANK)
                return false
            }
            
            if (self.txtMobileNumber?.text?.trim().count)! < 6 || (self.txtMobileNumber?.text?.trim().count)! > 13  {
                self.showToast(AppMessages.PHONE_VALID)
                return false
            }
        }
        else
        {
            if (self.txtMobileNumber?.text?.trim().count)! > 0 {
                
                if (self.txtMobileNumber?.text?.trim().count)! < 6 || (self.txtMobileNumber?.text?.trim().count)! > 13  {
                    self.showToast(AppMessages.PHONE_VALID)
                    return false
                }
            }
        }
        
        if self.recieptype == 0
        {
            if self.lblCategory?.text?.trim() == "Item category"
            {
                self.showToast(AppMessages.CATEGORY_BLANK)
                return false
            }
            if self.lblWeight?.text?.trim() == "Item weight"
            {
                self.showToast(AppMessages.WEIGHT_BLANK)
                return false
            }
            
        }
        
        return true
    }
}
