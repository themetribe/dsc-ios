//
//  LoginVC.swift
//  DRunnerCustomer
//
//  Created by mac306 on 13/07/20.
//  Copyright © 2020 mac306. All rights reserved.
//

import UIKit
import FRHyperLabel
import GoogleSignIn
import AuthenticationServices
//import TwilioVoice

let baseURLString = "https://3b57e324.ngrok.io"
let accessTokenEndpoint = "/accessToken"
let identity = "alice"
let twimlParamTo = "to"

class LoginVC: UIViewController,ASAuthorizationControllerDelegate,GIDSignInDelegate,ASAuthorizationControllerPresentationContextProviding {
    
    @IBOutlet weak var lblSignup : UILabel?
    @IBOutlet weak var txtEmail : UITextField?
    @IBOutlet weak var txtPassword : UITextField?
    @IBOutlet weak var btnAppleLogin : UIButton?
    
    let google = Google()
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            self.btnAppleLogin?.isHidden = false
        }else{
            self.btnAppleLogin?.isHidden = true
        }
        
    }
    
    @IBAction func onFacebookButton( _ sender : UIButton)
    {
        Global.facebookLogin(controller: self) { (response) in
            print(response)
            
            let parameters = ["social_id":response["id"]!,
                              "social_type":"facebook",
                              "email":response["email"] ?? "",
                              "first_name":"\(response["name"] ?? "")",
                              "last_name" : "",
                              "mobile":"\(response["phone"] ?? "")",
                              "user_type":"0",
                              "device_type":kDeviceType,
                              "profile_pic":"http://graph.facebook.com/\(response["id"]!)/picture?type=large",
                              "device_id":APP_DELEGATE.deviceId,
                              "location_lat":"\(APP_DELEGATE.UserLocation.latitude)",
                              "location_lng":"\(APP_DELEGATE.UserLocation.longitude)"
                              
                              
            ]
            
            //self.loginApiCalling(parameter: parameters, header: LOGIN_SOCIAL)
            print(parameters)
            
            
            self.SocialLogin(param: parameters)
            
        }
    }
    @IBAction func onGoogleButton( _ sender : UIButton)
    {
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance()?.restorePreviousSignIn()
        GIDSignIn.sharedInstance()?.shouldFetchBasicProfile = true
        GIDSignIn.sharedInstance()?.delegate = self
        GIDSignIn.sharedInstance().signIn()
    }
    @IBAction func onAppleButton( _ sender : UIButton)
    {
        if #available(iOS 13.0, *) {
            let appleIDProvider = ASAuthorizationAppleIDProvider()
            let request = appleIDProvider.createRequest()
            request.requestedScopes = [.fullName, .email]
            
            let authorizationController = ASAuthorizationController(authorizationRequests: [request])
            authorizationController.delegate = self
            authorizationController.presentationContextProvider = self
            authorizationController.performRequests()
        } else {
            // Fallback on earlier versions
        }
    }
    @IBAction func onSignUP (_ sender : UIButton)
    {
        NotificationCenter.default.post(name: NotificatioName.gotoLogin, object: nil, userInfo: ["currentPage" : 1])
    }
    @IBAction func onSignButton( _ sender : UIButton)
    {
        
        DispatchQueue.main.async {
            self.view.endEditing(true)
        }
        
        if self.txtEmail?.text?.trim().count == 0 {
            self.showToast(AppMessages.EMAIL_BLANK)
            return
        }
        if self.txtEmail?.text?.trim().isValidEmailId == false {
            self.showToast(AppMessages.EMAIL_VALID)
            return
        }
        if self.txtPassword?.text?.trim().count == 0 {
            self.showToast(AppMessages.PASS_BLANK)
            return
        }
        let param = [    "user_type":"0",
                         "login_type" : "email",
                         "email" : self.txtEmail?.text ?? "",
                         "password" : self.txtPassword?.text ?? "",
                         "device_id" : APP_DELEGATE.deviceId,
                         "device_type" : kDeviceType,
                         "latitude" :  String(format: "%f",APP_DELEGATE.UserLocation.latitude),
                         "longitude" : String(format: "%f",APP_DELEGATE.UserLocation.longitude)]
        
        self.onNormalLogin(param: param)
        
    }
    
    @IBAction func onForgotPassword( _ sender :UIButton)
    {
        let alert = UIAlertController(title: "Forgot Passowrd", message: "Enter your registered email.", preferredStyle: .alert)
        
        //2. Add the text field. You can configure it however you need.
        alert.addTextField { (textField) in
            
            textField.placeholder = "Email"
            
        }
        
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { [weak alert] (_) in
            let textField = alert?.textFields![0] // Force unwrapping because we know it exists.
            let name = (textField?.text)?.trimmingCharacters(in: .whitespaces)
            
            
            if  (name?.replacingOccurrences(of: " ", with: "").trim().isValidEmailId)! == false
            {
                self.showToast("Please enter valid email.")
                
            }
            else
            {
                self.forgotPasswordApiCalling(email: name ?? "")
                
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { [weak alert] (_) in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
            // Create an account in your system.
            let userIdentifier = appleIDCredential.user
            let userFirstName = appleIDCredential.fullName?.givenName
            let userLastName = appleIDCredential.fullName?.familyName
            var email = ""
            if appleIDCredential.email != nil{
                email = appleIDCredential.email ?? ""
            }
            
            let parameters = ["social_id":userIdentifier ,
                              "email":email ,
                              "first_name":userFirstName ?? "Guest",
                              "last_name":userLastName ?? "",
                              "mobile":"",
                              "social_type":"apple",
                              "profile_pic":"",
                              "user_type":"0",
                              "device_type":kDeviceType,
                              "device_id":APP_DELEGATE.deviceId,
                              "location_lat":"\(APP_DELEGATE.UserLocation.latitude)",
                              "location_lng":"\(APP_DELEGATE.UserLocation.longitude)"]
            
            
            self.SocialLogin(param: parameters)
            
            //Navigate to other view controller
        } else if let passwordCredential = authorization.credential as? ASPasswordCredential {
            //            // Sign in using an existing iCloud Keychain credential.
            //            let username = passwordCredential.user
            //            let password = passwordCredential.password
            //
            //Navigate to other view controller
        }
    }
    
    /// - Tag: did_complete_error
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        // Handle error.
        print(error.localizedDescription)
    }
    
    /// - Tag: provide_presentation_anchor
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
    
    
    public func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if (error == nil) {
            
            let userId = user.userID
            let givenName = user.profile.givenName
            let familyName = user.profile.familyName
            let email = user.profile.email
            let parameters = ["social_id":userId ?? "",
                              "email":email ?? "",
                              "first_name":givenName ?? "",
                              "last_name":familyName ?? "",
                              "mobile":"",
                              "social_type":"google",
                              "profile_pic":"",
                              "user_type":"0",
                              "device_type":kDeviceType,
                              "device_id":APP_DELEGATE.deviceId,
                              "location_lat":"\(APP_DELEGATE.UserLocation.latitude)",
                              "location_lng":"\(APP_DELEGATE.UserLocation.longitude)"]
            
            self.SocialLogin(param: parameters)
        } else {
            print("\(error.localizedDescription)")
        }
    }
    
}

// API CALLING
extension LoginVC
{
    func forgotPasswordApiCalling(email : String)
    {
        let param = [  "email" : email
        ]
        API_HELPER.sharedInstance.postDataInJsonUsingNS(header: K.ApiName.ForgotPassword, withParameter: param as NSDictionary, inVC: self, showHud: true, addAuthHeader: false) { (result, message, status) in
            DispatchQueue.main.async {
                
                self.showToast(message)
                
            }
        }
        
    }
    func onNormalLogin(param : Dictionary<String,Any>)
    {
        
        API_HELPER.sharedInstance.postDataInJsonUsingNS(header: K.ApiName.login, withParameter: param as NSDictionary, inVC: self, showHud: true, addAuthHeader: false) { (result, message, status) in
            DispatchQueue.main.async {
                if status
                {
                    
                    let dict : NSMutableDictionary = APP_DELEGATE.convertAllDictionaryValueToNil(result.mutableCopy() as! NSMutableDictionary) as! NSMutableDictionary
                    defaults.set(dict, forKey: udUserInfo)
                    Helper.shared.setUserID(value: dict["user_id"] as? String ?? "0")
                    Helper.shared.setUserPoints(value: dict["reward_points"] as? String ?? "0")
                    Helper.shared.setUserWallet(value: dict["wallet_amount"] as? String ?? "0")
                    
                    defaults.setValue("1", forKey: udLogin)
                    
                    if dict["auth_token"] != nil
                    {
                        Helper.shared.setAuthToken(value: dict.object(forKey: "auth_token") as! String)
                    }
                    if dict["mobile"] as? String == ""
                    {
                        let otpVC = MAIN_SB.instantiateViewController(withIdentifier: "AddMobileNumberVC") as! AddMobileNumberVC
                        self.navigationController?.pushViewController(otpVC, animated: true)
                        
                    }
                    else if dict["is_mobile_verify"] as? String != "1"
                    {
                        let otpVC = MAIN_SB.instantiateViewController(withIdentifier: "OTPVerificationVC") as! OTPVerificationVC
                        self.navigationController?.pushViewController(otpVC, animated: true)
                    }
                    else
                    {
                        
                        APP_DELEGATE.setViewControllers()
                    }
                    
                }
                else
                {
                    self.showToast(message)
                    
                }
            }
            
        }
    }
    func SocialLogin(param : Dictionary<String,Any>)
    {
        
        API_HELPER.sharedInstance.postDataInJsonUsingNS(header: K.ApiName.socialSignUp, withParameter: param as NSDictionary, inVC: self, showHud: true, addAuthHeader: false) { (result, message, status) in
            DispatchQueue.main.async {
                if status
                {
                    
                    let dict : NSMutableDictionary = APP_DELEGATE.convertAllDictionaryValueToNil(result.mutableCopy() as! NSMutableDictionary) as! NSMutableDictionary
                    defaults.set(dict, forKey: udUserInfo)
                    Helper.shared.setUserID(value: dict["user_id"] as? String ?? "0")
                    Helper.shared.setUserPoints(value: dict["reward_points"] as? String ?? "0")
                    Helper.shared.setUserWallet(value: dict["wallet_amount"] as? String ?? "0")
                    defaults.setValue("1", forKey: udLogin)
                    
                    if dict["auth_token"] != nil
                    {
                        Helper.shared.setAuthToken(value: dict.object(forKey: "auth_token") as! String)
                    }
                    
                    if dict["mobile"] as? String == ""
                    {
                        let otpVC = MAIN_SB.instantiateViewController(withIdentifier: "AddMobileNumberVC") as! AddMobileNumberVC
                        self.navigationController?.pushViewController(otpVC, animated: true)
                    }
                    else {
                        APP_DELEGATE.setViewControllers()
                    }
                    
                }
                else
                {
                    self.showToast(message)
                    
                }
            }
            
        }
    }
}
