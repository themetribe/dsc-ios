//
//  LoginContaineVC.swift
//  DRunnerCustomer
//
//  Created by mac306 on 13/07/20.
//  Copyright © 2020 mac306. All rights reserved.
//

import UIKit


class LoginContainerVC: UIViewController {
    
    @IBOutlet var btnLogin: UIButton?
    @IBOutlet var btnSignUp: UIButton?
    @IBOutlet var containView: UIView?
    @IBOutlet var mainContainView: UIView?
    
    var pageViewController: UIPageViewController?
    var viewControllers: [UIViewController]?
    var currentpage = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupPageController()
        self.setButtonUI(ctPage: 0)
        self.navigationController?.navigationBar.isHidden = true
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(updateVC(notification:)), name: NotificatioName.gotoLogin, object: nil)
    }
    
    private func setupPageController() {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
            
            self.pageViewController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
            self.pageViewController?.delegate = self
            self.viewControllers = [
                MAIN_SB.instantiateViewController(withIdentifier: "LoginVC"),
                MAIN_SB.instantiateViewController(withIdentifier: "SignUpVC"),
            ]
            
            self.pageViewController?.setViewControllers([ self.viewControllerAtIndex(0)!], direction: .forward, animated: true, completion: nil)
            self.pageViewController?.dataSource = self
            if let scrollView =  self.pageViewController?.view.subviews.filter({$0.isKind(of: UIScrollView.self)}).first as? UIScrollView {
                scrollView.backgroundColor = UIColor.clear
                scrollView.isScrollEnabled = false
            }
            
            self.pageViewController!.view.frame = self.containView?.frame as! CGRect
            self.addChild(self.pageViewController!)
            
            self.mainContainView?.addSubview((self.pageViewController?.view!)!)
            
            self.mainContainView?.gestureRecognizers = self.pageViewController?.gestureRecognizers
        }
        
    }
    
    @objc func updateVC(notification: Notification)
    {
        let data = notification.userInfo as! [String : Any]
        currentpage = data["currentPage"] as? Int ?? 0
        if currentpage == 0
        {
            self.setButtonUI(ctPage: 0)
            self.changePage(.reverse)
            
        }
        else{
            self.setButtonUI(ctPage: 1)
            self.changePage(.forward)
            
        }
        
    }
    func setButtonUI(ctPage : Int)
    {
        if ctPage == 0
        {
            self.btnLogin?.layer.addBorder(edge: .bottom, color: AppColor, thickness: 1.0)
            self.btnSignUp?.layer.addBorder(edge: .bottom, color: .white, thickness: 1.0)
            self.btnLogin?.setTitleColor(.black, for: .normal)
            self.btnSignUp?.setTitleColor(UIColor.lightGray, for: .normal)
            
        }
        else{
            self.btnLogin?.layer.addBorder(edge: .bottom, color: .white, thickness: 1.0)
            self.btnSignUp?.layer.addBorder(edge: .bottom, color: AppColor, thickness: 1.0)
            self.btnSignUp?.setTitleColor(.black, for: .normal)
            self.btnLogin?.setTitleColor(UIColor.lightGray, for: .normal)
            
        }
        
    }
    @IBAction func onLoginButton(_ sender: Any) {
        
        self.setButtonUI(ctPage: 0)
        if currentpage == 0{
            return
        }
        else{
            currentpage = 0
            self.changePage(.reverse);
        }
        self.handlePageChange()
        
    }
    
    @IBAction func onRegisterButton(_ sender: Any) {
        
        self.setButtonUI(ctPage: 1)
        if currentpage == 1{
            return
        }
        else{
            currentpage = 1
            self.changePage(.forward);
        }
        self.handlePageChange()
    }
    
}
extension LoginContainerVC: UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        var index = indexOfViewController(viewController)
        
        if (index == 0) || (index == NSNotFound) {
            return nil
        }
        
        index -= 1
        
        return viewControllerAtIndex(index)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        var index = indexOfViewController(viewController)
        
        if index == NSNotFound {
            return nil
        }
        
        index += 1
        
        if index == viewControllers?.count {
            return nil
        }
        
        return viewControllerAtIndex(index)
    }
    
    
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        guard let indexPage = viewControllers?.firstIndex(of: (pageViewController.viewControllers?.first)!) else{
            return
        }
        print("current page=== ",indexPage)
        currentpage = indexPage
        self.handlePageChange()
        
    }
    
}

// MARK: - Helpers
extension LoginContainerVC {
    func changePage(_ direction: UIPageViewController.NavigationDirection) {
        let viewController = viewControllerAtIndex(currentpage)
        if viewController == nil {
            return
        }
        pageViewController?.setViewControllers([viewController!], direction: direction, animated: true, completion: nil)
    }
    
    @objc fileprivate func handlePageChange(){
        
        if currentpage == 0{
            
        }else if currentpage == 1{
            
        }else{
            
        }
        DispatchQueue.main.async {
            
            
        }
    }
    
    fileprivate func viewControllerAtIndex(_ index: Int) -> UIViewController? {
        if viewControllers?.count == 0 || index >= viewControllers!.count {
            return nil
        }
        
        return viewControllers?[index]
    }
    
    fileprivate func indexOfViewController(_ viewController: UIViewController) -> Int {
        return viewControllers?.firstIndex(of: viewController) ?? NSNotFound
    }
}
