//
//  AddMobileNumberVC.swift
//  DRunnerCustomer
//
//  Created by mac306 on 20/08/20.
//  Copyright © 2020 mac306. All rights reserved.
//

import UIKit
import SKCountryPicker

class AddMobileNumberVC: UIViewController {
 
    @IBOutlet weak var btnCountry : UIButton?
    @IBOutlet weak var txtPhone : UITextField?
    var isMobileNumberChange : Bool = false
    
    var userData = defaults.object(forKey: udUserInfo) as! Dictionary<String,Any>
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.userData = defaults.object(forKey: udUserInfo) as! Dictionary<String,Any>
        if self.isMobileNumberChange
        {
            self.navigationController?.navigationBar.isHidden = true
        }
    }
    
    
    
    @IBAction func onBackButton(_ sender : UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onCountryButton( _ sender : UIButton)
    {
        let countryController = CountryPickerWithSectionViewController.presentController(on: self) { [weak self] (country: Country) in

          guard let self = self else { return }
            
            self.btnCountry?.setTitle(country.dialingCode, for: .normal)

        }

        countryController.detailColor = .black
    }
    
    @IBAction func onSubmitButton( _ sender : UIButton)
    {
        if self.txtPhone?.text?.trim().count == 0 {
            self.showToast(AppMessages.PHONE_BLANK)
            return
        }
        if (self.txtPhone?.text?.trim().count)! < 6 || (self.txtPhone?.text?.trim().count)! > 13  {
            self.showToast(AppMessages.PHONE_VALID)
            return
        }
        
        var countryCode = self.btnCountry?.titleLabel?.text
        countryCode = countryCode?.replacingOccurrences(of: "+", with: "")
        
        
        
        let param = [       "user_id": Helper.shared.getUserID(),
                            "first_name" : userData["first_name"] as? String ?? "",
                            "last_name" : userData["last_name"] as? String ?? "",
                            "email" : userData["email"] as? String ?? "",
                            "country_code" : countryCode ?? "65",
                            "mobile" : self.txtPhone?.text ?? "",
                           ] as [String : Any]
        API_HELPER.sharedInstance.editUserProfile(parameter: param as NSDictionary, imageArray: [], inVC: self, showHud: true) { (status) in
                if status
                {
                /*    if self.isMobileNumberChange
                    {
                        DispatchQueue.main.async {
                            let otpVC = MAIN_SB.instantiateViewController(withIdentifier: "OTPVerificationVC") as! OTPVerificationVC
                            otpVC.isMobileNumberChange = true
                            otpVC.mobile  = self.txtPhone?.text ?? ""
                            otpVC.countryCode = self.btnCountry?.titleLabel?.text as? String ?? ""
                            self.navigationController?.pushViewController(otpVC, animated: true)
                        }
                    }
                    else{
                       APP_DELEGATE.setViewControllers()
                    }*/
                    
                    let otpVC = MAIN_SB.instantiateViewController(withIdentifier: "OTPVerificationVC") as! OTPVerificationVC
                    otpVC.isMobileNumberChange = self.isMobileNumberChange
                    otpVC.mobile  = self.txtPhone?.text ?? ""
                    otpVC.countryCode = self.btnCountry?.titleLabel?.text ?? ""
                    self.navigationController?.pushViewController(otpVC, animated: true)
                }
        }
        
        
//        API_HELPER.sharedInstance.uploadMultipartData(methodType: .post, header: K.ApiName.EditProfile, withParameter: param as NSDictionary, imageArray: [], inVC: self, showHud: true, addAuthHeader: true, videoAdd: false, audioAdd: false, fileAdd: false, gifAdd: false) { (result, message, status) in
//            DispatchQueue.main.async {
//                if status
//                {
//                    let dict : NSMutableDictionary = APP_DELEGATE.convertAllDictionaryValueToNil(result.mutableCopy() as! NSMutableDictionary) as! NSMutableDictionary
//                        defaults.set(dict, forKey: udUserInfo)
//                        Helper.shared.setUserID(value: dict["user_id"] as? String ?? "0")
//                        defaults.setValue("1", forKey: udLogin)
//
//                        if dict["auth_token"] != nil
//                        {
//                            Helper.shared.setAuthToken(value: dict.object(forKey: "auth_token") as! String)
//                        }
//                    APP_DELEGATE.setViewControllers()
//
//
//                }
//                else
//                {
//                    self.showToast(message as String)
//                }
//            }
//
//        }
        
    }

}
extension AddMobileNumberVC : UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //For mobile numer validation
        if textField == self.txtPhone {
            let allowedCharacters = CharacterSet(charactersIn:"0123456789")//Here change this characters based on your requirement
            let characterSet = CharacterSet(charactersIn: string)
            return allowedCharacters.isSuperset(of: characterSet)
        }
        return true
    }
}
