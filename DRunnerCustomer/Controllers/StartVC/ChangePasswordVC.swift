//
//  ChangePasswordVC.swift
//  DRunnerCustomer
//
//  Created by mac306 on 10/09/20.
//  Copyright © 2020 mac306. All rights reserved.
//

import UIKit

class ChangePasswordVC: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var txtOldPassword : UITextField?
    @IBOutlet weak var txtNewPassword : UITextField?
    @IBOutlet weak var txtConfirmPassword : UITextField?

    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    @IBAction func onBackButton(_ sender : UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func onChangePassword(_ sender : UIButton)
    {
        if isValidField()
        {
            API_HELPER.sharedInstance.postDataInJsonUsingNS(header: K.ApiName.ChangePassword, withParameter: ["user_id" : Helper.shared.getUserID() , "old_password" : txtOldPassword?.text, "password" : self.txtNewPassword?.text], inVC: self, showHud: true, addAuthHeader: true) { (result, message, status) in
                DispatchQueue.main.async {
                    if status
                    {
                        self.showSingleButtonAlert(title: kAppName, message: message) {
                             self.navigationController?.popViewController(animated: true)
                        }
                    }
                    else
                    {
                        self.showToast(message)
                    }
                }
            }
        }
        
    }
    
    func isValidField() -> Bool {
        DispatchQueue.main.async {
                   self.view.endEditing(true)
               }
        if self.txtOldPassword?.text?.trim().count == 0 {
            self.showToast(AppMessages.PASS_OLD_BLANK)
            return false
        }
        if self.txtNewPassword?.text?.trim().count == 0 {
            self.showToast(AppMessages.PASS_BLANK)
            return false
        }
        if self.txtNewPassword?.text?.trim().isValidPassword == false {
            self.showToast(AppMessages.PASS_VALID)
            return false
        }
        if self.txtConfirmPassword?.text?.trim().count == 0 {
            self.showToast(AppMessages.PASS_BLANK)
            return false
               }
        if self.txtNewPassword?.text?.trim() != self.txtConfirmPassword?.text?.trim() {
            self.showToast(AppMessages.CPASS_VALID)
            return false
        }
        return true
    }

}
