//
//  OTPVerificationVC.swift
//  DRunnerCustomer
//
//  Created by mac306 on 06/08/20.
//  Copyright © 2020 mac306. All rights reserved.
//

import UIKit
import SVPinView

class OTPVerificationVC: UIViewController {
    
    @IBOutlet weak var btnBack : UIButton?
    @IBOutlet weak var viewPin : SVPinView?
    @IBOutlet weak var btnResend : UIButton?
    @IBOutlet weak var btnSubmit : UIButton?
    @IBOutlet weak var lblCodeTxt : UILabel?
    var isMobileNumberChange : Bool = false
    
    var countryCode : String = ""
    var userData = defaults.object(forKey: udUserInfo) as! Dictionary<String,Any>

    var mobile : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.countryCode = self.userData["country_code"] as? String ?? ""
       /* if !isMobileNumberChange
        {
            self.mobile = self.userData["mobile"] as? String ?? ""
        }*/
        
        if mobile == "" {
            self.mobile = self.userData["mobile"] as? String ?? ""
        }
        
        self.lblCodeTxt?.text = String(format : "Please enter verification code sent to %@-%@", self.countryCode , self.mobile)
        
        //self.viewPin?.pastePin(pin: self.userData["otp"] as? String ?? "")
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.userData = defaults.object(forKey: udUserInfo) as! Dictionary<String,Any>
        if self.isMobileNumberChange
        {
            self.navigationController?.navigationBar.isHidden = true
        }
    }
    
    
    
    
    @IBAction func onBackButton(_ sender : UIButton)
    {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onResendButton(_ sender : UIButton)
    {
        countryCode = countryCode.replacingOccurrences(of: "+", with: "")
        let param = [  "user_id":self.userData["user_id"] as? String ?? "",
                       "country_code" : countryCode, "mobile" : self.mobile] as [String : Any]
        
        API_HELPER.sharedInstance.postDataInJsonUsingNS(header: K.ApiName.ResendOtp, withParameter: param as NSDictionary, inVC: self, showHud: true, addAuthHeader: true) { (result, message, status) in
            DispatchQueue.main.async {
                if status
                {
                    
                    let dict : NSMutableDictionary = APP_DELEGATE.convertAllDictionaryValueToNil(result.mutableCopy() as! NSMutableDictionary) as! NSMutableDictionary
                    defaults.set(dict, forKey: udUserInfo)
                    
                    
                    Helper.shared.setUserID(value: dict["user_id"] as? String ?? "0")
                    Helper.shared.setUserPoints(value: dict["reward_points"] as? String ?? "0")
                    Helper.shared.setUserWallet(value: dict["wallet_amount"] as? String ?? "0")
                    defaults.setValue("1", forKey: udLogin)
                    
                    if dict["auth_token"] != nil
                    {
                        Helper.shared.setAuthToken(value: dict.object(forKey: "auth_token") as! String)
                    }
                    
                    self.showToast(message)
                    
                    
                }
                else
                {
                    self.showToast(message)
                    
                }
            }
            
        }
    }
    
    
    @IBAction func onSubmitButton (_ sender : UIButton)
    {
        let pin = self.viewPin?.getPin()
        if pin?.count == 4
        {
            let param = [   "user_id":self.userData["user_id"] as? String ?? "",
                            "otp" : pin ?? "",
                            "mobile" : self.mobile,
                            "device_id" : APP_DELEGATE.deviceId,
                            "device_type" : kDeviceType,
                            "latitude" :  String(format: "%f",APP_DELEGATE.UserLocation.latitude),
                            "longitude" : String(format: "%f",APP_DELEGATE.UserLocation.longitude)] as [String : Any]
            
            
            API_HELPER.sharedInstance.postDataInJsonUsingNS(header: K.ApiName.OtpVerify, withParameter: param as NSDictionary, inVC: self, showHud: true, addAuthHeader: true) { (result, message, status) in
                DispatchQueue.main.async {
                    if status
                    {
                        
                        let dict : NSMutableDictionary = APP_DELEGATE.convertAllDictionaryValueToNil(result.mutableCopy() as! NSMutableDictionary) as! NSMutableDictionary
                        defaults.set(dict, forKey: udUserInfo)
                        if self.isMobileNumberChange
                        {
                            self.showSingleButtonAlert(title: kAppName, message: message) {
                                self.navigationController?.popToRootViewController(animated: true)
                            }
                            
                        }
                        else
                        {
                            Helper.shared.setUserID(value: dict["user_id"] as? String ?? "0")
                            Helper.shared.setUserPoints(value: dict["reward_points"] as? String ?? "0")
                            Helper.shared.setUserWallet(value: dict["wallet_amount"] as? String ?? "0")
                            defaults.setValue("1", forKey: udLogin)
                            
                            if dict["auth_token"] != nil
                            {
                                Helper.shared.setAuthToken(value: dict.object(forKey: "auth_token") as! String)
                            }
                            
                            APP_DELEGATE.setViewControllers()
                        }
                        
                    }
                    else
                    {
                        self.showToast(message)
                        
                    }
                }
                
            }
        }
        else
        {
            DispatchQueue.main.async {
                self.viewPin?.clearPin()
                Helper.shared.showAlert(msg: "Please enter otp")
            }
        }
        
    }
    
}
