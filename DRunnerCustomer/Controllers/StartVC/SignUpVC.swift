//
//  SignUpVC.swift
//  DRunnerCustomer
//
//  Created by mac306 on 13/07/20.
//  Copyright © 2020 mac306. All rights reserved.
//

import UIKit
import SKCountryPicker
import GoogleSignIn
import AuthenticationServices
import FRHyperLabel

class SignUpVC: UIViewController,ASAuthorizationControllerDelegate,GIDSignInDelegate, ASAuthorizationControllerPresentationContextProviding {
    
    @IBOutlet weak var scrollView : UIScrollView?
    @IBOutlet weak var scrollContrinerView : UIView?
    @IBOutlet weak var txtEmail : UITextField?
    @IBOutlet weak var txtPassword : UITextField?
    @IBOutlet weak var txtName : UITextField?
    @IBOutlet weak var txtReferralCode : UITextField?
    @IBOutlet weak var txtPhone : UITextField?
    @IBOutlet weak var btnCountry : UIButton?
    @IBOutlet weak var btnAppleLogin : UIButton?
    @IBOutlet weak var lblHyper : FRHyperLabel!
    var isAgreementSelect : Bool = false
    @IBOutlet weak var btnAgreement : UIButton?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 13.0, *) {
            self.btnAppleLogin?.isHidden = false
        }else{
            self.btnAppleLogin?.isHidden = true
        }
        
        let str = "I agree to Terms of condition,Privacy policy and Code of conduct."
        // let attributes = [NSForegroundColorAttributeName: UIColor.blackColor(),
        //NSFontAttributeName: UIFont.systemFontOfSize(15)]
        lblHyper?.attributedText = NSAttributedString.init(string: str)
        
        let handler = {
            (hyperLabel: FRHyperLabel!, substring: String!) -> Void in
            let profileVC = STATIC_SB.instantiateViewController(withIdentifier: "StaticPagesVC") as! StaticPagesVC
            if substring == "Terms of condition"
            {
                profileVC.urlString = TS
            }
            else if substring == "Privacy policy"
            {
                
                profileVC.urlString = PP
                
            }
            else
            {
                profileVC.urlString = CC
            }
            self.navigationController?.pushViewController(profileVC, animated: true)
            print(substring)
        }
        self.lblHyper.setLinksForSubstrings(["Terms of condition","Privacy policy","Code of conduct"], withLinkHandler: handler)
        // self.scrollView?.translatesAutoresizingMaskIntoConstraints = false
        self.scrollView?.contentSize =   self.scrollContrinerView?.frame.size as! CGSize
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onAgreementSelection( _ sender : UIButton)
    {
        if isAgreementSelect == false{
            self.btnAgreement?.isSelected = true
            self.isAgreementSelect = true
        }
        else
        {
            self.btnAgreement?.isSelected = false
            self.isAgreementSelect = false
        }
    }
    
    @IBAction func onCountryButton( _ sender : UIButton)
    {
        let countryController = CountryPickerWithSectionViewController.presentController(on: self) { [weak self] (country: Country) in
            
            guard let self = self else { return }
            
            //self.btnCountry?.setImage(country.flag, for: .normal)
            //= country.flag
            self.btnCountry?.setTitle(country.dialingCode, for: .normal)
            
        }
        
        // can customize the countryPicker here e.g font and color
        countryController.detailColor = .black
    }
    
    @IBAction func onSignIn (_ sender : UIButton)
    {
        NotificationCenter.default.post(name: NotificatioName.gotoLogin, object: nil, userInfo: ["currentPage" : 0])
    }
    @IBAction func onFacebookButton( _ sender : UIButton)
    {
        Global.facebookLogin(controller: self) { (response) in
            print(response)
            
            let parameters = [
                "social_id":response["id"]!,
                "social_type":"facebook",
                "email":response["email"] ?? "",
                "first_name":"\(response["name"] ?? "")",
                "last_name" : "",
                "mobile":"",
                "device_type":kDeviceType,
                "profile_pic":"http://graph.facebook.com/\(response["id"]!)/picture?type=large",
                "device_id":APP_DELEGATE.deviceId,
                "location_lat":"\(APP_DELEGATE.UserLocation.latitude)",
                "location_lng":"\(APP_DELEGATE.UserLocation.longitude)",
                "user_type":"0",
                "transport_category" :  ""]
            
            print(parameters)
            self.SocialLogin(param: parameters)
            
        }
        
    }
    
    @IBAction func onGoogleButton( _ sender : UIButton)
    {
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance()?.restorePreviousSignIn()
        GIDSignIn.sharedInstance()?.shouldFetchBasicProfile = true
        GIDSignIn.sharedInstance()?.delegate = self
        GIDSignIn.sharedInstance().signIn()
    }
    
    @IBAction func onAppleButton( _ sender : UIButton)
    {
        
        if #available(iOS 13.0, *) {
            let appleIDProvider = ASAuthorizationAppleIDProvider()
            let request = appleIDProvider.createRequest()
            request.requestedScopes = [.fullName, .email]
            
            let authorizationController = ASAuthorizationController(authorizationRequests: [request])
            authorizationController.delegate = self
            authorizationController.presentationContextProvider = self
            authorizationController.performRequests()
        } else {
            // Fallback on earlier versions
        }
    }
    
    @IBAction func onSignupButton( _ sender : UIButton)
    {
        if self.isValidField()
        {
            if !isAgreementSelect
            {
                self.showToast("Please select our Terms of conditions, Privacy policy and Code of conduct")
                return
            }
            self.RegisterApiCalling()
        }
        //        let otpVC = MAIN_SB.instantiateViewController(withIdentifier: "OTPVerificationVC") as! OTPVerificationVC
        //        self.navigationController?.pushViewController(otpVC, animated: true)
    }
    
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
            // Create an account in your system.
            let userIdentifier = appleIDCredential.user
            let userFirstName = appleIDCredential.fullName?.givenName
            let userLastName = appleIDCredential.fullName?.familyName
            var email = ""
            if appleIDCredential.email != nil{
                email = appleIDCredential.email ?? ""
            }
            
            let parameters = ["social_id":userIdentifier ,
                              "email":email ,
                              "first_name":userFirstName ?? "Guest",
                              "last_name":userLastName ?? "",
                              "mobile":"",
                              "social_type":"apple",
                              "profile_pic":"",
                              "user_type" : "0",
                              "device_type":kDeviceType,
                              "device_id":APP_DELEGATE.deviceId,
                              "location_lat":"\(APP_DELEGATE.UserLocation.latitude)",
                              "location_lng":"\(APP_DELEGATE.UserLocation.longitude)"]
            
            
            self.SocialLogin(param: parameters)
            
            //Navigate to other view controller
        } else if let passwordCredential = authorization.credential as? ASPasswordCredential {
            //            // Sign in using an existing iCloud Keychain credential.
            //            let username = passwordCredential.user
            //            let password = passwordCredential.password
            //
            //Navigate to other view controller
        }
    }
    
    
    /// - Tag: did_complete_error
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        // Handle error.
        print(error.localizedDescription)
    }
    
    /// - Tag: provide_presentation_anchor
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
    
    
  
    public func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if (error == nil) {
            // Perform any operations on signed in user here.
            let userId = user.userID
            let givenName = user.profile.givenName
            let familyName = user.profile.familyName
            let email = user.profile.email
            let parameters = ["social_id":userId ?? "",
                              "email":email ?? "",
                              "first_name":givenName ?? "",
                              "last_name":familyName ?? "",
                              "mobile":"",
                              "social_type":"google",
                              "profile_pic":"",
                              "user_type" : "0",
                              "device_type":kDeviceType,
                              "device_id":APP_DELEGATE.deviceId,
                              "location_lat":"\(APP_DELEGATE.UserLocation.latitude)",
                              "location_lng":"\(APP_DELEGATE.UserLocation.longitude)","transport_category" :  ""]
            
            self.SocialLogin(param: parameters)
        } else {
            print("\(error.localizedDescription)")
        }
    }
    
}
extension SignUpVC : UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //For mobile numer validation
        if textField == self.txtPhone {
            let allowedCharacters = CharacterSet(charactersIn:"0123456789")//Here change this characters based on your requirement
            let characterSet = CharacterSet(charactersIn: string)
            return allowedCharacters.isSuperset(of: characterSet)
        }
        return true
    }
}
extension SignUpVC
{
    func isValidField() -> Bool {
        DispatchQueue.main.async {
            self.view.endEditing(true)
        }
        
        if self.txtName?.text?.trim().count == 0 {
            self.showToast(AppMessages.FNAME_BLANK)
            return false
        }
        if (self.txtName?.text?.trim().count)! < 2   || (self.txtName?.text?.trim().count)! > 80 {
            self.showToast(AppMessages.FNAME_VALID)
            return false
        }
        
        if self.txtEmail?.text?.trim().count == 0   {
            self.showToast(AppMessages.EMAIL_BLANK)
            return false
        }
        if self.txtEmail?.text?.trim().count ?? 0 < 2  || (self.txtEmail?.text?.trim().count)! > 120{
            self.showToast(AppMessages.EMAIL_VALID_CHAR)
            return false
        }
        if self.txtEmail?.text?.trim().isValidEmailId == false {
            self.showToast(AppMessages.EMAIL_VALID)
            return false
        }
        if self.txtPhone?.text?.trim().count == 0 {
            self.showToast(AppMessages.PHONE_BLANK)
            return false
        }
        if (self.txtPhone?.text?.trim().count)! < 6 || (self.txtPhone?.text?.trim().count)! > 13  {
            self.showToast(AppMessages.PHONE_VALID)
            return false
        }
        
        
        if self.txtPassword?.text?.trim().count == 0 {
            self.showToast(AppMessages.PASS_BLANK)
            return false
        }
        if self.txtPassword?.text?.trim().isValidPassword == false {
            self.showToast(AppMessages.PASS_VALID)
            return false
        }
        
        return true
    }
    func RegisterApiCalling()
    {
        let trimmedString = self.txtName?.text?.trimmingCharacters(in: .whitespaces)
        let fullNameArr = trimmedString?.components(separatedBy: " ")
        var fname    = fullNameArr?[0]
        var lName = ""
        if (fullNameArr?.count)! > 1
        {
            fname = trimmedString!
            lName = fullNameArr?.last as! String
            fname = fname?.replacingOccurrences(of: lName, with: "")
        }
        var countryCode = self.btnCountry?.titleLabel?.text
        countryCode = countryCode?.replacingOccurrences(of: "+", with: "")
        
        
        let param = [       "user_type":"0",
                            "transport_category" : "",
                            "first_name" : fname,
                            "last_name" : lName,
                            "email" : self.txtEmail?.text ?? "",
                            "password" : self.txtPassword?.text ?? "",
                            "country_code" : countryCode ?? "65",
                            "mobile" : self.txtPhone?.text ?? "",
                            "referral_code" : self.txtReferralCode?.text ?? "",
                            "device_id" : APP_DELEGATE.deviceId,
                            "device_type" : kDeviceType,
                            "latitude" :  String(format: "%f",APP_DELEGATE.UserLocation.latitude),
                            "longitude" : String(format: "%f",APP_DELEGATE.UserLocation.longitude)] as [String : Any]
        
        self.apiCalling(param: param)
        
    }
    
    func SocialLogin(param : Dictionary<String,Any>)
    {
        
        API_HELPER.sharedInstance.postDataInJsonUsingNS(header: K.ApiName.socialSignUp, withParameter: param as NSDictionary, inVC: self, showHud: true, addAuthHeader: false) { (result, message, status) in
            DispatchQueue.main.async {
                if status
                {
                    
                    let dict : NSMutableDictionary = APP_DELEGATE.convertAllDictionaryValueToNil(result.mutableCopy() as! NSMutableDictionary) as! NSMutableDictionary
                    defaults.set(dict, forKey: udUserInfo)
                    Helper.shared.setUserID(value: dict["user_id"] as? String ?? "0")
                    Helper.shared.setUserPoints(value: dict["reward_points"] as? String ?? "0")
                    Helper.shared.setUserWallet(value: dict["wallet_amount"] as? String ?? "0")
                    defaults.setValue("1", forKey: udLogin)
                    
                    if dict["auth_token"] != nil
                    {
                        Helper.shared.setAuthToken(value: dict.object(forKey: "auth_token") as! String)
                    }
                    if dict["mobile"] as? String == ""
                    {
                        let otpVC = MAIN_SB.instantiateViewController(withIdentifier: "AddMobileNumberVC") as! AddMobileNumberVC
                        self.navigationController?.pushViewController(otpVC, animated: true)
                    }
                    else {
                        APP_DELEGATE.setViewControllers()
                    }
                  /*  else if dict["is_mobile_verify"] as? String != "1"
                    {
                        let otpVC = MAIN_SB.instantiateViewController(withIdentifier: "OTPVerificationVC") as! OTPVerificationVC
                        self.navigationController?.pushViewController(otpVC, animated: true)
                    }*/
                   
                    
                }
                else
                {
                    self.showToast(message)
                    
                }
            }
            
        }
    }
    func apiCalling(param : Dictionary<String,Any>)
    {
        
        API_HELPER.sharedInstance.postDataInJsonUsingNS(header: K.ApiName.signup, withParameter: param as NSDictionary, inVC: self, showHud: true, addAuthHeader: false) { (result, message, status) in
            DispatchQueue.main.async {
                if status
                {
                    
                    let dict : NSMutableDictionary = APP_DELEGATE.convertAllDictionaryValueToNil(result.mutableCopy() as! NSMutableDictionary) as! NSMutableDictionary
                    defaults.set(dict, forKey: udUserInfo)
                    Helper.shared.setUserID(value: dict["user_id"] as? String ?? "0")
                    Helper.shared.setUserPoints(value: dict["reward_points"] as? String ?? "0")
                    Helper.shared.setUserWallet(value: dict["wallet_amount"] as? String ?? "0")
                    defaults.setValue("1", forKey: udLogin)
                    
                    if dict["auth_token"] != nil
                    {
                        Helper.shared.setAuthToken(value: dict.object(forKey: "auth_token") as! String)
                    }
                    if dict["mobile"] as? String == ""
                    {
                        let otpVC = MAIN_SB.instantiateViewController(withIdentifier: "AddMobileNumberVC") as! AddMobileNumberVC
                        self.navigationController?.pushViewController(otpVC, animated: true)
                    }
                    else if dict["is_mobile_verify"] as? String != "1"
                    {
                        let otpVC = MAIN_SB.instantiateViewController(withIdentifier: "OTPVerificationVC") as! OTPVerificationVC
                        self.navigationController?.pushViewController(otpVC, animated: true)
                    }
                    else
                    {
                        APP_DELEGATE.setViewControllers()
                    }
                    
                }
                else
                {
                    self.showToast(message)
                    
                }
            }
            
        }
    }
}
