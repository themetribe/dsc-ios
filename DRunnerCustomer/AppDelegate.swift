//
//  AppDelegate.swift
//  DRunnerCustomer
//
//  Created by mac306 on 13/07/20.
//  Copyright © 2020 mac306. All rights reserved.
//

import UIKit
import CoreData
import Firebase
import FirebaseMessaging
import GoogleSignIn
import GoogleMaps
import GooglePlaces
import FBSDKCoreKit
import IQKeyboardManagerSwift
import UserNotifications
import Stripe
import ChatSDK
import ChatProvidersSDK
import RxLocation
import RxSwift

public let NOT_SET = "not set"
public let NOTIFICATION_GPS_DATA = Notification.Name("NOTIFICATION_GPS_DATA")


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate, MessagingDelegate {
    
    var window: UIWindow?
    var nav  = UINavigationController()
    var deviceId = "abcd12345"
    
    private var rxLocation: RxLocation?
    let disposeBag = DisposeBag()
    
    var UserLocation = CLLocationCoordinate2D()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        IQKeyboardManager.shared.previousNextDisplayMode = .alwaysShow
        if #available(iOS 13.0, *) {
            self.window!.overrideUserInterfaceStyle = .light
        } else {
            // Fallback on earlier versions
        }
        Chat.initialize(accountKey: ZD_KEY)
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        Stripe.setDefaultPublishableKey(STRIPE_KEY)
        //UINavigationBar.appearance().tintColor = UIColor(named : "AppColor")
        self.nav.navigationBar.isTranslucent = false
        
        self.configureNotification()
        UNUserNotificationCenter.current().delegate = self
        
        self.setViewControllers()
        
        GMSServices.provideAPIKey(kGoogleServiceKey)
        GMSPlacesClient.provideAPIKey(kGoogleServiceKey)
        GIDSignIn.sharedInstance()?.clientID = kGoogleClientId
        // LocationManager.sharedInstance.StartLocation()
        
        rxLocation =  RxLocation(authorization: .authorizeAlways)
        
        if #available(iOS 11.0, *) {
            rxLocation?.locationManager.showsBackgroundLocationIndicator = true
        }
        
        self.getUserLocation()
        
        // Override point for customization after application launch.
        return true
    }
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        
        if url.scheme == kFbUrlScheme {
            return ApplicationDelegate.shared.application(app, open: url, sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplication.OpenURLOptionsKey.annotation])
        }
        
        else {
            GIDSignIn.sharedInstance()?.handle(url)
        }
        return false
    }
    
    func setViewControllers()
    {
        let userId = Helper.shared.getUserID()
        if userId != "0"
        {
            let userData = defaults.object(forKey: udUserInfo) as! NSDictionary
            if  (userData["is_mobile_verify"] as? String == "0") || (userData["mobile"] as? String == "")
            {
               /* let otpVC = MAIN_SB.instantiateViewController(withIdentifier: "AddMobileNumberVC") as! AddMobileNumberVC
                nav = UINavigationController.init(rootViewController: otpVC)
                self.window?.rootViewController = otpVC*/
                
                let loginVC = MAIN_SB.instantiateViewController(withIdentifier: "LoginContainerVC") as! LoginContainerVC
                nav = UINavigationController.init(rootViewController: loginVC)
                self.window?.rootViewController = nav
            }
            else{
                let homeTabBar = HOME_SB.instantiateViewController(withIdentifier: "CustomTabBar") as! CustomTabBar
                self.window?.rootViewController = homeTabBar
            }
        }
        else
        {
            let loginVC = MAIN_SB.instantiateViewController(withIdentifier: "LoginContainerVC") as! LoginContainerVC
            nav = UINavigationController.init(rootViewController: loginVC)
            self.window?.rootViewController = nav
            
        }
    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentCloudKitContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentCloudKitContainer(name: "DRunnerCustomer")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    
    func configureNotification() {
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            center.requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in }
            //center.delegate = notificationCustom
            center.delegate = self
            
        } else {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
        }
        UIApplication.shared.registerForRemoteNotifications()
    }
    
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        //print("FIREBASE_TOKEN: \(fcmToken )")
        //self.deviceId = fcmToken
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("Successfully registered for notifications!")
        Messaging.messaging().apnsToken = deviceToken
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("APNs registration failed: \(error)")
    }
    
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        //  let userInfo = notification.request.content.userInfo
        completionHandler([.alert, .badge, .sound])
        
        return
        
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        print(userInfo)
        
        if let customData = userInfo[AnyHashable("data")] as? String {
            if let notificationData = convertToDictionary(text: customData) as NSDictionary? {
                kNotificationObject = notificationData
                let homeTabBar = HOME_SB.instantiateViewController(withIdentifier: "CustomTabBar") as! CustomTabBar
                self.window?.rootViewController = homeTabBar
            }
        }
    }
    
    
    //MARK: --------- CONVERT DECTIONARY NULL TO NIL
    func convertAllDictionaryValueToNil(_ dict: NSMutableDictionary) -> NSDictionary
    {
        let arrayOfKeys = dict.allKeys as NSArray
        for i in 0..<arrayOfKeys.count
        {
            if (dict.object(forKey: arrayOfKeys.object(at: i))) is NSNull
            {
                dict .setObject("" as AnyObject, forKey: arrayOfKeys.object(at: i) as! String as NSCopying)
            }
        }
        
        return dict
    }
    
    //MARK: - Location Find
    func getUserLocation()
    {
        rxLocation?.requestCurrentLocation().subscribe(onNext: { location in
            print(location)
            
            self.UserLocation = location.coordinate
            NotificationCenter.default.post(name: NOTIFICATION_GPS_DATA, object: location, userInfo: nil)
            
        }).disposed(by: disposeBag)
        
    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
}

