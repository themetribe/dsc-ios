//
//  Validation.swift
//  
//
//  Created by Anil Jangir on 18/06/20.
//  Copyright © 2020 Anil Jangir. All rights reserved.
//

import Foundation

struct Regex {
    static let userNameRegex   = "^[a-zA-Z ]{2,30}$"
   // static let passwordRegex   = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z]).{6,20}$"
    static let passwordRegex   = "^(?=.*[0-9])(?=.*[a-zA-Z]).{6,20}$"
    static let emailRegex      = "^[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$"
    static let emailPhoneRegex = "^[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$|^([0-9]{10,13})$"
    static let panCardRegex    = "[A-Z]{5}[0-9]{4}[A-Z]{1}"
    static let ifscRegex = "^[A-Za-z]{4}[a-zA-Z0-9]{7}$"
    static let amountRegex = "^[0-9.]{1,}$"
}
extension String {
    
    var isValidUserName:Bool {
            return testThe(regex: Regex.userNameRegex)
    }
    
    
    var isValidPassword:Bool {
            return testThe(regex: Regex.passwordRegex)
    }
    
    var isValidEmailId:Bool {
            return testThe(regex: Regex.emailRegex)
    }
    
    var isValidEmailAndNumber:Bool {
        return testThe(regex: Regex.emailPhoneRegex)
    }
    
    var isValidPANCard:Bool {
        return testThe(regex: Regex.panCardRegex)
    }
    
    var isValidIFSCCode:Bool {
        return testThe(regex: Regex.ifscRegex)
    }
    
    var isValidAmount:Bool {
        return testThe(regex: Regex.amountRegex)
    }
    
    func testThe(regex:String) -> Bool {
     if self.count > 0 {
        let predicate = NSPredicate(format: "SELF MATCHES %@",regex)
        return predicate.evaluate(with: self.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines))
        }
       return false
    }
}
