//
//  MessagesConstant.swift
//  ComicHub
//
//  Created by Anil Jangir on 12/06/20.
//  Copyright © 2020 Anil Jangir. All rights reserved.
//

import Foundation

struct NotificatioName {
    
    static let gotoLogin            = Notification.Name("GO_TO_LOGIN_VIEW")
    static let userLocation         = Notification.Name("UserLocationDetail")
    static let appliedCoupons       = Notification.Name("APPLIED_COUPON")
    
}

struct AppMessages {
    static let PHONE_BLANK  = "Please enter your mobile number."
    static let EMAIL_BLANK = "Please enter email id."
    static let PASS_BLANK  = "Please enter password."
    static let CPASS_BLANK = "Please enter confirm password."
    static let EMAIL_VALID = "Please enter correct email."
    static let PASS_VALID  = "Please enter at least 6 characters password which contains one alphabet & one number."
    static let EMAIL_VALID_CHAR = "Email should be minimum 2 character and maximum 120 characters."
    static let CPASS_VALID = "Confirm password and password shoulb be same."
    static let FNAME_BLANK = "Please enter full name."
    static let FNAME_VALID = "Full name should be minimum 2 character and maximum 80 characters."
    static let LNAME_BLANK = "Please enter last name."
    static let LNAME_VALID = "Last name should be minimum 2 character and maximum 30 characters."
   
    static let PHONE_VALID = "Mobile number should be minimum 6 digit and maximum 13 digit."
    static let ADD_BLANK = "Please enter address."
    static let ADD_VALID = "First name should be minimum 6 Characers and maximum 100 caracters."
    static let CITY_BLANK = "Please enter city."
    static let CITY_VALID  = "city name should be minimum 2 Characers and maximum 60 caracters."
    static let STATE_BLANK = "Please select state."
    static let ZIP_BLANK = "Please enter Zip Code."
    static let ZIP_VALID = "Zip code should be 4 Characers and maximum 10 caracters."
    static let COUNTRY_BLANK = "Please select country."
    
    static let No_Internet : String        =  "Internet connection not available."
    
    static let CATEGORY_BLANK = "Please select item category."
    static let WEIGHT_BLANK   = "Please select item weight."
    
    static let NAME_BLANK = "Please enter user name."
    static let NAME_VALID = "Name should be minimum 2 character and maximum 80 characters."
    static let USERPHONE_BLANK  = "Please enter mobile number."
    
    static let LOCATION_BLANK  = "Please ensure that both Address and Recipient name are filled"
    static let PASS_OLD_BLANK  = "Please enter old password."
    
    static let QUEUE_TIME_BLANK  = "Please select minimum 10 min queue time."
}
