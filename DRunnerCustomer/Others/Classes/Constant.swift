//
//  Constant.swift
//  ComicHub
//
//  Created by Anil Jangir on 10/06/20.
//  Copyright © 2020 Anil Jangir. All rights reserved.
//

import UIKit

class Constant: NSObject {

}

let kGoogleClientId = "613962318313-vpbp9tovhcuh9atrfgjqdibfh4rves04.apps.googleusercontent.com"
let kGoogleServiceKey = "AIzaSyDm8M1KYBPXytNCvsYthSAmWfiPwjwjrtc"
let kFbUrlScheme = "fb727057621361663"
let Twillio_Secret_key = "Yyz6VD6k2mFBMQn5tgmARLRBKF34QbPM"
let Twillio_SID_Key = "SK1daf3ac7b54c630004faf7685023bcf8"
//let kGoogleApiKey = "AIzaSyDozeipONaQrejVH9ejd61s3x1QaQd4tNE"

let LAT     = "26.9124"
let LONG    = "75.7873"
let SCREEN_HEIGHT                                           =  UIScreen.main.bounds.size.height
let SCREEN_WIDTH                                            =  UIScreen.main.bounds.size.width
let APP_DELEGATE                                            =  UIApplication.shared.delegate as! AppDelegate


let defaults                                                =  UserDefaults.standard

let kAppName : String = "DStar Runner"
let kDeviceType : String = "iPhone"
let ZD_KEY   : String   = "AcLBLKHq68aaeieyddOyFxePgjq2tDAu"
let STRIPE_KEY : String = "pk_live_51HMAi1CGuoPEQAPQGE99xmhAimyxPolodK5OnCjtV0WbE7vm62hikDV2VWgR1wLN8iJLX4QVVHlK6LVPFtLTGvb200rERSkw0a"
    
    //"pk_test_51HMAi1CGuoPEQAPQxBIF8TTFNJNPo5bCtnXlvcYXLeXOWytZLgpl0kXyav5jxKcTqGzUuCmvi4xx8WRgJVoRPeqc005TtdMHX9"

let udUserInfo : String                     =  "User_Info"
let udLogin : String                        =  "isLogin"

let ButtonStartColor : UIColor          = UIColor.init(displayP3Red: 236.0/255.0, green: 36.0/255.0, blue: 36.0/255.0, alpha: 1.0)
let ButtonEndColor  : UIColor           = UIColor.init(displayP3Red: 109.0/255.0, green: 19.0/255.0, blue: 19.0/255.0, alpha: 1.0)
let AppColor  : UIColor                 = UIColor.init(displayP3Red: 252.0/255.0, green: 212.0/255.0, blue: 2.0/255.0, alpha: 1.0)

let CHAT_SB                           =  UIStoryboard(name: "Chat", bundle : nil)
let MAIN_SB                           =  UIStoryboard(name: "Main", bundle : nil)
let HOME_SB                           =  UIStoryboard(name: "Home", bundle : nil)
let POINTS_SB                         =  UIStoryboard(name: "Points", bundle : nil)
let PAYMENT_SB                        =  UIStoryboard(name: "Payment", bundle : nil)
let REWARDS_SB                        =  UIStoryboard(name: "Rewards", bundle : nil)
let BOOKTASK_SB                       =  UIStoryboard(name: "BookTask", bundle : nil)
let STATIC_SB                         =  UIStoryboard(name: "StaticPage", bundle: nil)
let RATING_SB                         =  UIStoryboard(name: "RateDriver", bundle: nil)
var kNotificationObject               =  NSDictionary()


let kInviteMsg:String                                       =  "You have been invited to connect with your friend using the \(kAppName) app."


let LOADER                                                  =  Loader()

let Price_Payment                                           = "https://dstar-runner.com/price-payment"
let AboutUs                                                 = "https://dstar-runner.com/page/about-us"
let TS                                                      = "https://dstar-runner.com/page/terms-and-condition"
let PP                                                      = "https://dstar-runner.com/page/privacy-policy"
let CC                                                      = "https://dstar-runner.com/page/code-of-conduct"
let No_Internet                             : String        =  "Internet connection not available."


class TwoTagActivityIndicator : UIActivityIndicatorView {
    
    var row : Int?
    var section : Int?
}
