//
//  Global.swift
//  ComicHub
//
//  Created by Anil Jangir on 12/06/20.
//  Copyright © 2020 Anil Jangir. All rights reserved.
//

import Foundation
import UIKit


enum AppStoryboard:String {
    
    case Main, Home, Chat
    
//    var instance:UIStoryboard {
//        UIStoryboard(name: self.rawValue, bundle: Bundle.main)
//    }
}

enum Notifications:String {
    case Chat           = "chat"
    case SendOffer      = "Send_Offer"
    case OrderDelivered = "Order_Deliverd"
    case AddProduct     = "Product_Add"
    case SendReqOffer   = "Send_Request_Offer"
    case OfferStatus    = "Offer_Status"
    case PaymentStatus  = "Payment_Status"
    case OtpVerify      = "Otp_Verify"
    case CancelOrder    = "Cancel_Order"
}


class Global:NSObject {
    
    static func facebookLogin(controller : UIViewController, completion:@escaping (_ response:[String:Any])->()) {
        let fb = Facebook()
        fb.controller = controller
        
        fb.getFields = ["fields": "id, name, first_name, last_name, relationship_status, email, cover, phone"]
        fb.logIn(success: { (token, response) in
            completion(response)
        }) { (errorDic) in
            print(errorDic)
        }
    }
    
    static func facebookLogout()
    {
         let fb = Facebook()
        fb.fbAccessToken = nil
    
        fb.logOut()
        
    }
    

    static func generateOrderID(withPrefix prefix: String?) -> String? {
        let randomNo = Int(arc4random())
        let orderID = String(format: "%@%ld", prefix ?? "DEF", randomNo)
        return orderID
    }
    static func getTopViewController() -> UIViewController? {
        if var topController = APP_DELEGATE.window?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            return topController
        }
        return nil
    }
}
