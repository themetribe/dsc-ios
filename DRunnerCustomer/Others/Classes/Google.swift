//
//  Google.swift
//  Shiop
//
//  Created by admin on 13/01/20.
//  Copyright © 2020 admin. All rights reserved.
//

import Foundation
import GoogleSignIn

protocol GoogleDelegate {
    func didSignIn(user:GIDGoogleUser)
}

class Google:NSObject {
        
    var delegate:GoogleDelegate?
    
    func initGoogle(controller:UIViewController) {
        if let instance = GIDSignIn.sharedInstance() {
            instance.delegate = self
            instance.presentingViewController = controller
        }
    }
    
    func signIn() {
        GIDSignIn.sharedInstance()?.signIn()
    }
    
    func signOut() {
        GIDSignIn.sharedInstance()?.signOut()
    }
}

extension Google:GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let delegate = self.delegate {
            if error == nil {
                delegate.didSignIn(user: user)
            } else {
                print("Google Error:\(error.localizedDescription)")
            }
        }
    }
}
