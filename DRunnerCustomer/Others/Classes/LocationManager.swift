//
//  AJLocationManager.swift
//  DRunnerCustomer
//
//  Created by mac306 on 17/08/20.
//  Copyright © 2020 mac306. All rights reserved.
//

import Foundation
import CoreLocation


class LocationManager: NSObject {
    
    static let sharedInstance = LocationManager()
    
    var latitude:Double = 0.0
    var longitude:Double = 0.0
    var centerLat:Double = 0.0
    var centerLng:Double = 0.0
    
    let locationManager = CLLocationManager()
    
    var isLocationPermissionOn:Bool {
        return ((CLLocationManager.authorizationStatus() == .authorizedAlways) || (CLLocationManager.authorizationStatus() == .authorizedWhenInUse))
    }
    
    override init() {
        super.init()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
    }
    
    func StartLocation()
    {
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
    }
    
//    func getAddressFrom(klatitute:Double, klongitute:Double, withCompletion completion:@escaping (_ address:String, _ latitute:Double, _ longitute:Double)->()) {
//        let coordinate = CLLocationCoordinate2D(latitude: klatitute, longitude: klongitute)
//        var addStr = ""
//        GMSGeocoder().reverseGeocodeCoordinate(coordinate) { (response, error) in
//            if error == nil {
//                if let response = response {
//                    if let address = response.firstResult() {
//                        if let throughFare = address.thoroughfare {
//                            addStr = addStr + throughFare + ", "
//                        }
//
//                        if let sublocality = address.subLocality {
//                            addStr = addStr + sublocality + ", "
//                        }
//
//                        if let locality = address.locality {
//                            addStr = addStr + locality + ", "
//                        }
//
//                        if let state = address.administrativeArea {
//                            addStr = addStr + state + ", "
//                        }
//
//                        if let country = address.country {
//                            addStr = addStr + country + ", "
//                        }
//
//                        if let postalCode = address.postalCode {
//                            addStr = addStr + postalCode
//                        }
//
//                        completion(addStr, klatitute, klongitute)
//                    }
//                }else {
//                    completion("",0.0,0.0)
//                }
//            }else {
//                print(error!.localizedDescription)
//                completion("",0.0,0.0)
//            }
//        }
//        return
//
//        let geoCoder = CLGeocoder()
//
//        let location = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
//       // Global.showHud()
//        geoCoder.reverseGeocodeLocation(location) { (placemarks, error) in
//            if error == nil {
//                if let placemarks = placemarks {
//                    if placemarks.count > 0 {
//                        let pm = placemarks[0]
//                        var address = ""
//
//                        if let sublocality = pm.subLocality {
//                            address = address + sublocality + ", "
//                        }
//
//                        if let throughFare = pm.thoroughfare {
//                            address = address + throughFare + ", "
//                        }
//
//                        if let locality = pm.locality {
//                            address = address + locality + ", "
//                        }
//
//                        if let country = pm.country {
//                            address = address + country + ", "
//                        }
//
//                        if let postalCode = pm.postalCode {
//                            address = address + postalCode
//                        }
//
//                        print(address)
//                        //Global.hideHud()
//                        completion(address, coordinate.latitude, coordinate.longitude)
//                    }else {
//                      //  Global.hideHud()
//                        completion("error",0.0,0.0)
//                    }
//                }else {
//                  //  Global.hideHud()
//                    completion("error",0.0,0.0)
//                }
//
//            }else {
//                print(error?.localizedDescription)
//                //Global.hideHud()
//                completion("error",0.0,0.0)
//
//            }
//        }
//    }
}

extension LocationManager:CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            latitude = location.coordinate.latitude
            longitude = location.coordinate.longitude
           // print("latitude:\(latitude),longitude:\(longitude)")
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Fail to update location error:\(error.localizedDescription)")
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        print("didChangeAuthorizationStatus")
        
        switch status {
        case .notDetermined:
            print(".NotDetermined")
            break
            
        case .authorizedAlways:
            print(".Authorized")
            locationManager.startUpdatingLocation()
            break
            
        case .denied:
            print(".Denied")
            break
            
        default:
            print("Unhandled authorization status")
            break
            
        }
    }
}
