////
////  AJNotificationManager.swift
////  DRunnerCustomer
////
////  Created by mac306 on 17/08/20.
////  Copyright © 2020 mac306. All rights reserved.
////
//
//import Foundation
//import UserNotifications
//
//class AJNotificationManager :NSObject, UNUserNotificationCenterDelegate {
//
//   }
//
//func configureNotification() {
//       if #available(iOS 10.0, *) {
//           let center = UNUserNotificationCenter.current()
//           center.requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in }
//           center.delegate = notificationCustom
//           center.delegate = self
//           
//       } else {
//           UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
//       }
//       UIApplication.shared.registerForRemoteNotifications()
//   }
//   
//   func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
//       let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
//       tokenString = deviceTokenString
//       
//       // VoiceMessageController.shared()?.setAPNDeviceToken(deviceToken)
//       print("APNs device token: \(deviceTokenString)")
//   }
//   
//   func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
//       print("APNs registration failed: \(error)")
//   }
//   
