//
//  Loader.swift
//  PickPack
//
//  Created by Octal on 03/01/17.
//  Copyright © 2017 Octal. All rights reserved.
//

import UIKit

class Loader: NSObject {

//    var load  = MBProgressHUD()
    func show(views: UIView)
    {
        let loadingNotification = MBProgressHUD.showAdded(to: views, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = "Loading"
    }
    
    func hide(delegate:UIViewController) {
        MBProgressHUD.hide(for: delegate.view, animated: true)
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}
