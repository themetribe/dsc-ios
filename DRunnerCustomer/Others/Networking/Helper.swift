//
//  Helper.swift
//  Rene_Harley
//
//  Created by Anil-Mac on 14/11/18.
//  Copyright © 2018 Octal. All rights reserved.
//

import UIKit
import Foundation
import AVFoundation
import Photos
//import TTTAttributedLabel


class Helper {
    
    static let shared = Helper()
    fileprivate init(){
        
    }
    func getAuthToken() -> String
    {
        return defaults.object(forKey: "token") as? String ?? ""
    }
    func setAuthToken(value:String){
        
        defaults.set(value, forKey: "token")
        defaults.synchronize()
    }
    
    func getUserPoints() -> String
    {
        return defaults.object(forKey: "reward_points") as? String ?? "0"
    }
    func setUserPoints(value:String){
        
        defaults.set(value, forKey: "reward_points")
        defaults.synchronize()
    }
    
    
    func getUserWallet() -> String
    {
        return defaults.object(forKey: "wallet_amount") as? String ?? "0.0"
    }
    func setUserWallet(value:String){
           
        defaults.set(value, forKey: "wallet_amount")
        defaults.synchronize()
    }
    
    func getUserID() -> String
    {
        return defaults.object(forKey: "id") as? String ?? "0"
    }
    func setUserID(value:String){
        
        defaults.set(value, forKey: "id")
        defaults.synchronize()
    }
    
    
    func getUserEmail() -> String
    {
        return defaults.object(forKey: "Email") as? String ?? ""
    }
    func setUserEmail(value:String){
        
        defaults.set(value, forKey: "Email")
        defaults.synchronize()
    }
    
    
    
    func getUserProfile() -> String
    {
           return defaults.object(forKey: "ProfileImage") as? String ?? ""
    }
       func setUserProfile(value:String){
           
           defaults.set(value, forKey: "ProfileImage")
           defaults.synchronize()
       }
    
    
    
    func getUserName() -> String
    {
        return defaults.object(forKey: "CustomerName") as? String ?? ""
        
    }
    func setUserName(value:String){
    
        defaults.set(value, forKey: "CustomerName")
        defaults.synchronize()
    }
    
    
    func getRankingImage(userRank : String) -> UIImage
    {
        //ranking
        if userRank == "Diamond"
        {
            return UIImage.init(named: "diamond")!
        }
        else if userRank == "Gold"
        {
            return UIImage.init(named: "gold")!
        }
        else if userRank == "Platinum"
        {
            return UIImage.init(named: "platinum")!
        }
        else
        {
            return UIImage.init(named: "silver")!
        }
        
    }
    
 
 //Email
    func documentsDirectory() -> String {
        let paths: [Any] = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        return paths.last! as! String
    }
    
  
    func showAlert(msg:String) {
        DispatchQueue.main.async {
        let alert = UIAlertController(title: kAppName, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        APP_DELEGATE.window?.rootViewController?.topMostViewController().present(alert, animated: true, completion: nil)
        }
    }
    
  
    
    func ConvertToFloat(value : Any) -> Float
    {
        var valueFloat : Float = 0.0
        
        if value is Float
        {
            valueFloat = value as? Float ?? 0.00
        }
        else if value is NSNumber
        {
            valueFloat = (value as! NSNumber).floatValue
        }
        else{
            valueFloat = Float(value as? String ?? "0.0") ?? 0.0
        }
        return valueFloat
    }
    
    
     func convertDateFormate(value : String) -> String
      
      {
           if value == ""
           {
               return "-"
           }
           let formatter = DateFormatter()
           //formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
           formatter.dateFormat = "dd/MM/yyyy hh:mm:ss a"
           //formatter.timeZone = TimeZone(abbreviation: "SGT")
           formatter.timeZone = NSTimeZone.system
           let estDate = formatter.date(from: value)
           if estDate != nil
           {
               formatter.timeZone = TimeZone.current
               formatter.dateFormat = "MMMM dd, yyyy hh:mm a"
               return formatter.string(from: estDate!)
           }
           else
           {
               return "-"
           }
       
      }
       
    
    func convertDateFormateAM(value : String) -> String
    
    {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy HH:mm:ss a"
        formatter.timeZone = TimeZone(abbreviation: "SGT")
        let estDate = formatter.date(from: value)
        formatter.timeZone = TimeZone.current
        formatter.dateFormat = "MMMM dd, yyyy hh:mm a"
        return formatter.string(from: estDate!)
    }
    
   
    //MARK: -------- !(PERMISSION ALLOW MESSAGE)
       
       func permissionAllowMessageWithController(controller : UIViewController, type : String)
       {
           let alert = UIAlertController(title: nil, message: "\(kAppName) does not have access to your \(type).  If you want to give \(kAppName) access visit Settings > \(kAppName) and slide the toggle button to allow access. ", preferredStyle: .alert)
           alert.addAction(UIAlertAction(title: "Open Settings", style: .default) { action in
               
               UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
           })
           alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { action in
               
           })
           DispatchQueue.main.async
               {
                   controller.present(alert, animated: true)
           }
           
       }

    
    func getUserProfile(vc : UIViewController , showHud : Bool , completion : @escaping (_ profileFeteched : Bool) -> ())
    {
        API_HELPER.sharedInstance.postDataInJsonUsingNS(header: K.ApiName.Profile, withParameter: ["user_id": Helper.shared.getUserID()], inVC: vc, showHud: showHud, addAuthHeader: true) { (resultData, message, status) in
            if !status
            {
                DispatchQueue.main.async {
                    Helper.shared.showAlert(msg: message)
                }
                
            }
            else{
                  let dict : NSMutableDictionary = APP_DELEGATE.convertAllDictionaryValueToNil(resultData.mutableCopy() as! NSMutableDictionary) as! NSMutableDictionary
                  defaults.set(dict, forKey: udUserInfo)
            }
            
            completion(status)
        }
    }

    func getTimeFromDateTimeStamp(timeSt : String) -> String
       {
           let number1: Double = (timeSt as NSString).doubleValue
           let date = NSDate(timeIntervalSince1970:(number1/1000))
           let dateFormatter = DateFormatter()
           dateFormatter.dateFormat = "dd MMM yyyy"
           return dateFormatter.string(from: date as Date)
           
       }
       
       func dateSortingAccordingTime(timeSt : Array<String>) ->Array<String>
       {
           let dateFormatter = DateFormatter()
           dateFormatter.dateFormat = "dd MMM yyyy"
           var convertedArray: [Date] = []
           for dat in timeSt {
               let date = dateFormatter.date(from: dat)
               if let date = date {
                   convertedArray.append(date)
               }
           }
           
           let ready = convertedArray.sorted(by: { $0.compare($1) == .orderedAscending })
           var arrayDate : Array<String> = []
           for dat in ready {
               let date = dateFormatter.string(from: dat)
               
               arrayDate.append(date)
               
           }
           return arrayDate
           
       }
       
    @objc func getTimeStamp() -> String
       {
           let timestamp = (NSDate().timeIntervalSince1970 ) * 1000
           
           return String(format : "%.f",Double(timestamp))
       }
       //MARK: --------- CAMERA PERMISSIONS
          
          func checkCameraVideoAccess() -> Int {
              var permissionCheck: Int = 0
              switch AVCaptureDevice.authorizationStatus(for: .video) {
              case .denied:
                  permissionCheck =  0
              case .restricted:
                  permissionCheck =  0
              case .authorized:
                  permissionCheck =  1
              case .notDetermined:
                  permissionCheck =  2
                  AVCaptureDevice.requestAccess(for: .video) { success in
                      if success {
                          permissionCheck =  1
                          print("Permission granted, proceed")
                      } else {
                          permissionCheck =  0
                          print("Permission denied")
                      }
                  }
              }
              return permissionCheck
          }
          
          func checkPhotoLibraryPermission()-> Int  {
              var permissionCheck: Int = 0
              let status = PHPhotoLibrary.authorizationStatus()
              switch status {
              case .authorized:
                  permissionCheck =  1
                  break
              //handle authorized status
              case .denied, .restricted :
                  permissionCheck = 0
                  break
              //handle denied status
              case .notDetermined:
                  // ask for permissions
                  permissionCheck = 2
                  PHPhotoLibrary.requestAuthorization { status in
                      switch status {
                      case .authorized:
                          permissionCheck =  1
                          break
                      // as above
                      case .denied, .restricted:
                          permissionCheck =  0
                          break
                      // as above
                      case .notDetermined:
                          permissionCheck =  2
                          break
                          // won't happen but still
                      case .limited:
                          break
                      @unknown default:
                          print("nnothng")
                      }
                  }
              case .limited:
                  break
              @unknown default:
                  print("nnothng")
              }
              return permissionCheck
          }
    func getTimeFromTimeStamp(timeSt : String) -> String
       {
           let number1: Double = (timeSt as NSString).doubleValue
           let date = NSDate(timeIntervalSince1970:(number1/1000))
           
           
           let dateFormatter = DateFormatter()
           // dateFormatter.dateFormat = "dd-mm-yyyy hh:mm:ss a"
           dateFormatter.dateFormat = "hh:mm a"
           dateFormatter.amSymbol = "AM"
           dateFormatter.pmSymbol = "PM"
           return dateFormatter.string(from: date as Date)
           // }
           
       }
    
    func getDayAndDate(dateStr : String) -> String
       {
           let dateFormatter = DateFormatter()
           dateFormatter.dateFormat = "dd MMM yyyy"
           let date1 = dateFormatter.date(from: dateStr)
           
           let calendar = Calendar.current
           let components = calendar.dateComponents([.day], from: Date(), to: date1!)
           
           if components.day == 0
           {
               return "Today"
           }
           else if components.day == -1
           {
               return "Yesterday"
           }
           else{
               return dateStr
           }
       }
          
}

//extension TTTAttributedLabel {
//      func showTextOnTTTAttributeLable(str: String, readMoreText: String, readLessText: String, font: UIFont?, charatersBeforeReadMore: Int, activeLinkColor: UIColor, isReadMoreTapped: Bool, isReadLessTapped: Bool) {
//
//        let text = str + readLessText
//        let attributedFullText = NSMutableAttributedString.init(string: text)
//        let rangeLess = NSString(string: text).range(of: readLessText, options: String.CompareOptions.caseInsensitive)
//          let rangeText = NSString(string: text).range(of: str, options: String.CompareOptions.caseInsensitive)
////Swift 5
//       // attributedFullText.addAttributes([NSAttributedStringKey.foregroundColor : UIColor.blue], range: rangeLess)
//        attributedFullText.addAttributes([NSAttributedString.Key.foregroundColor : activeLinkColor, NSAttributedString.Key.font : UIFont.systemFont(ofSize: 16.0)], range: rangeLess)
//        attributedFullText.addAttributes([NSAttributedString.Key.foregroundColor : UIColor.black, NSAttributedString.Key.font : UIFont.systemFont(ofSize: 16.0)], range: rangeText)
//
//        var subStringWithReadMore = ""
//        var beforeReadMore = ""
//        if text.count > charatersBeforeReadMore {
//          let start = String.Index(encodedOffset: 0)
//          let end = String.Index(encodedOffset: charatersBeforeReadMore)
//            beforeReadMore = String(text[start..<end])
//          subStringWithReadMore = String(text[start..<end]) + readMoreText
//        }
//
//        let attributedLessText = NSMutableAttributedString.init(string: subStringWithReadMore)
//        let nsRange = NSString(string: subStringWithReadMore).range(of: readMoreText, options: String.CompareOptions.caseInsensitive)
//        
//        let nsRangeText = NSString(string: subStringWithReadMore).range(of: beforeReadMore, options: String.CompareOptions.caseInsensitive)
//        //Swift 5
//       // attributedLessText.addAttributes([NSAttributedStringKey.foregroundColor : UIColor.blue], range: nsRange)
//        attributedLessText.addAttributes([NSAttributedString.Key.foregroundColor : UIColor.black, NSAttributedString.Key.font : UIFont.systemFont(ofSize: 16.0)], range: nsRangeText)
//        attributedLessText.addAttributes([NSAttributedString.Key.foregroundColor : activeLinkColor, NSAttributedString.Key.font : UIFont.systemFont(ofSize: 16.0)], range: nsRange)
//      //  if let _ = font {// set font to attributes
//      //   self.font = font
//      //  }
//        self.attributedText = attributedLessText
//        self.activeLinkAttributes = [NSAttributedString.Key.foregroundColor : activeLinkColor]
//        //Swift 5
//       // self.linkAttributes = [NSAttributedStringKey.foregroundColor : UIColor.blue]
//        self.linkAttributes = [NSAttributedString.Key.foregroundColor : activeLinkColor]
//        self.addLink(toTransitInformation: ["ReadMore":"1"], with: nsRange)
//
//        if isReadMoreTapped {
//          self.numberOfLines = 0
//          self.attributedText = attributedFullText
//          self.addLink(toTransitInformation: ["ReadLess": "1"], with: rangeLess)
//        }
//        if isReadLessTapped {
//          self.numberOfLines = 40
//          self.attributedText = attributedLessText
//        }
//      }
//    }
