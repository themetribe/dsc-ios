//
//  ViewController.swift
//  Decodeble
//  ComicHub
//
//  Created by Anil Jangir on 12/06/20.
//  Copyright © 2020 Anil Jangir. All rights reserved.
//

import Foundation
struct K {
    struct ProductionServer {
      
        //  static let kBaseURL = "http://dev.dstar-runner.com/api/" // dev url
        
        static let kBaseURL = "https://dstar-runner.com/api/" // prod url
        
    }
    
    struct ApiName {
      
        static let login                = "login"
        static let signup               = "signup"
        static let socialSignUp         = "social-signup"
        static let ForgotPassword       = "forgot-password"
        static let OtpVerify            = "verify-otp"
        static let ResendOtp            = "resend-otp"
        static let LogOut               = "logout"
        static let EditProfile          = "edit-profile"
        static let Home                 = "home"
        static let Profile              = "profile"
        static let notificationList     = "notification-list"
        static let notificationDelete   = "notification-delete"
        static let notificationRead     = "notification-read-unread"
        static let category             = "category"
        static let checkLocation        = "check-availability"
        static let checkVehicalPrice    = "request-vehicle-price"
        static let requestJob           = "request-send"
        static let CancelRequest        = "customer-cancel-request"
        static let TopRunner            = "top-ten-runners"
        static let ChangePassword       = "change-password"
        static let Redeem_Points_List   = "redeem-points-list"
        static let Redeem_Points        = "read-points"
        static let WalletHistory        = "wallet-history"
        static let RewardHistory        = "reward-history"
        static let RequestList          = "requests-for-customer"
        static let SavedCardList        = "card-list"
        static let AddPayment           = "add-card"
        static let RemoveCard           = "remove-card"
        static let OfferList            = "offer-list"
        static let CheckOffer           = "check-offer"
        static let QueueRequestSend     = "help-me-queue-request-send"
        static let PriceCalHMQ          = "help-me-queue-price-calculate"
        static let Rating               = "addrating"
        static let RequestDetail        = "request-detail"
        static let FavRunner            = "favourite-runners"
        static let FavUnFave            = "is-favourite"
        static let ChatImgUpload        = "upload-image"
        static let SendChatNotification = "sent-msg-notif"
        static let kBaseURL             = "https://dstar-runner.com/api/"
        static let giveTip              = "give-tip"
        static let adminReview          = "testimonials"
        static let twillioToken         = "twilio-token"
        
    }
}

enum HTTPHeaderField: String {
    case authentication = "Authorization"
    case contentType = "Content-Type"
    case acceptType = "Accept"
    case acceptEncoding = "Accept-Encoding"
    case token = "X-Access-Token"
}

enum ContentType: String {
    case json = "application/json"
}
