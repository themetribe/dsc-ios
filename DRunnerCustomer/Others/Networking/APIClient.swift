//
//  ViewController.swift
//  Decodeble
//  ComicHub
//
//  Created by Anil Jangir on 12/06/20.
//  Copyright © 2020 Anil Jangir. All rights reserved.
//

import Alamofire
//import CodableAlamofire

enum MimeType:String {
    case image = "image/png"
    case video = "video/mp4"
    case none = ""
}

enum DocumentType {
    case data
    case file
}

struct Document {
    var type:DocumentType
    var key:String
    var value:String?
    var data:Data?
    var mimeType:MimeType
    var fileExtension:String? = nil
}

class APIClient {
    
//    @discardableResult
//    private static func performRequest<T:Decodable>(route:APIRouter, decoder: JSONDecoder, keyPath:String? = nil, completion:@escaping (Result<T>)->Void) -> DataRequest {
//        print("URL:\(route.urlRequest!)")
//        print("Request:\(route.parameters!)")
//        ActivityIndicator.shared.startLoading()
//        return Alamofire.request(route).responseDecodableObject(queue: nil, keyPath: keyPath, decoder: decoder, completionHandler: { (response:DataResponse<T>) in
//            ActivityIndicator.shared.stopLoading()
//            print("Result:\(String(describing: response.result.value))")
//            completion(response.result)
//        })
//    }
//    
//    private static func uploadRequest<T:Decodable>(documentData:[Document], route:APIRouter, decoder: JSONDecoder, keyPath:String? = nil, completion:@escaping (Result<T>)->Void) {
//        
//        Alamofire.upload(multipartFormData: { (multipartFormData) in
//            for data in documentData {
//                switch data.type {
//                case .data:
//                    if let value = data.value {
//                        multipartFormData.append(value.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!, withName: data.key)
//                        
//                    }
//                case .file:
//                    if let d = data.data, let fileExtension = data.fileExtension {
//                        multipartFormData.append(d, withName: data.key, fileName: (data.key + fileExtension), mimeType: data.mimeType.rawValue)
//                    }
//                }
//            }
//        }, with: route) { (result) in
//            switch result {
//            case .success(let upload, _, _):
//                ActivityIndicator.shared.startLoading()
//                upload.responseDecodableObject(queue: nil, keyPath: keyPath, decoder: decoder, completionHandler: { (response:DataResponse<T>) in
//                    ActivityIndicator.shared.stopLoading()
//                    completion(response.result)
//                })
//            case .failure(let error):
//                print(error.localizedDescription)
//            }
//        }
//    }
//    
//    private static var jsonDecoder:JSONDecoder {
//        let jsonDecoder = JSONDecoder()
//        jsonDecoder.keyDecodingStrategy = .convertFromSnakeCase
//        jsonDecoder.dateDecodingStrategy = .secondsSince1970
//        return jsonDecoder
//    }
//    
//    private static var jsonDecoder1:JSONDecoder {
//        let jsonDecoder = JSONDecoder()
//        jsonDecoder.dateDecodingStrategy = .secondsSince1970
//        return jsonDecoder
//    }
    
//    public static func register(parameters:[String:Any], completion:@escaping (Result<User>) -> Void) {
//        performRequest(route: APIRouter.signup(parameters), decoder: jsonDecoder, completion: completion)
//    }
//    
//    public static func verifyOtp(parameters:[String:Any], completion:@escaping (Result<User>) -> Void) {
//        performRequest(route: APIRouter.verifyOtp(parameters), decoder: jsonDecoder, completion: completion)
//    }
//    
//    public static func login(parameters:[String:Any], completion:@escaping (Result<User>) -> Void) {
//        performRequest(route: APIRouter.login(parameters), decoder: jsonDecoder, completion: completion)
//    }
//    
//    public static func resendOtp(parameters:[String:Any], completion:@escaping (Result<User>) -> Void) {
//        performRequest(route: APIRouter.resendOtp(parameters), decoder: jsonDecoder, completion: completion)
//    }
//    
//    public static func forgotPassword(parameters:[String:Any], completion:@escaping (Result<User>) -> Void) {
//        performRequest(route: APIRouter.forgotPassword(parameters), decoder: jsonDecoder, completion: completion)
//    }
//    
//    public static func socialLoginCheck(parameters:[String:Any], completion:@escaping (Result<User>) -> Void) {
//        performRequest(route: APIRouter.socialLoginCheck(parameters), decoder: jsonDecoder, completion: completion)
//    }
//    
//    public static func socialLogin(parameters:[String:Any], completion:@escaping (Result<User>) -> Void) {
//        performRequest(route: APIRouter.socialLogin(parameters), decoder: jsonDecoder, completion: completion)
//    }
//    
//    public static func storeList(parameters:[String:Any], completion:@escaping (Result<ShopStore>) -> Void) {
//        performRequest(route: APIRouter.storeList(parameters), decoder: jsonDecoder, completion: completion)
//    }
//    
//    public static func tripList(parameters:[String:Any], completion:@escaping (Result<ShopTrip>) -> Void) {
//        performRequest(route: APIRouter.tripList(parameters), decoder: jsonDecoder, completion: completion)
//    }
//    
//    public static func requestReceive(parameters:[String:Any], completion:@escaping (Result<RequestReceive>) -> Void) {
//        performRequest(route: APIRouter.getRequestReceive(parameters), decoder: jsonDecoder, completion: completion)
//    }
//    
//    public static func myTripListing(parameters:[String:Any], completion:@escaping (Result<MyTripList>) -> Void) {
//        performRequest(route: APIRouter.myTripListing(parameters), decoder: jsonDecoder, completion: completion)
//    }
//    
//    public static func allStoreList(parameters:[String:Any], completion:@escaping (Result<StoreList>) -> Void) {
//        performRequest(route: APIRouter.allStoreList(parameters), decoder: jsonDecoder, completion: completion)
//    }
//    
//    public static func productList(parameters:[String:Any], completion:@escaping (Result<ProductList>) -> Void) {
//        performRequest(route: APIRouter.productList(parameters), decoder: jsonDecoder, completion: completion)
//    }
//    
//    public static func productDetail(parameters:[String:Any], completion:@escaping (Result<ProductDetailData>) -> Void) {
//        performRequest(route: APIRouter.productDetail(parameters), decoder: jsonDecoder, completion: completion)
//    }
//    
//    public static func citySearch(parameters:[String:Any], completion:@escaping (Result<CitySearch>) -> Void) {
//        performRequest(route: APIRouter.citySearch(parameters), decoder: jsonDecoder, completion: completion)
//    }
//    
//    public static func routeTripList(parameters:[String:Any], completion:@escaping (Result<TripList>) -> Void) {
//        performRequest(route: APIRouter.routeTripList(parameters), decoder: jsonDecoder, completion: completion)
//    }
//    
//    public static func sendRequestOffer(parameters:[String:Any], completion:@escaping (Result<SendRequest>) -> Void) {
//        performRequest(route: APIRouter.sendRequestOffer(parameters), decoder: jsonDecoder, completion: completion)
//    }
//    
//    public static func sendOffer(parameters:[String:Any], completion:@escaping (Result<CommonModel>) -> Void) {
//        performRequest(route: APIRouter.sendOffer(parameters), decoder: jsonDecoder, completion: completion)
//    }
//    public static func resendOffer(parameters:[String:Any], completion:@escaping (Result<CommonModel>) -> Void) {
//           performRequest(route: APIRouter.resendOffer(parameters), decoder: jsonDecoder, completion: completion)
//       }
//    
//    public static func getProfile(parameters:[String:Any], completion:@escaping (Result<Profile>) -> Void) {
//        performRequest(route: APIRouter.getProfile(parameters), decoder: jsonDecoder, completion: completion)
//    }
//    
//    public static func getNotification(parameters:[String:Any], completion:@escaping (Result<NotificationModel>) -> Void) {
//        performRequest(route: APIRouter.notificationList(parameters), decoder: jsonDecoder, completion: completion)
//    }
//    public static func readNotification(parameters:[String:Any], completion:@escaping (Result<NotificationModel>) -> Void) {
//           performRequest(route: APIRouter.notificationRead(parameters), decoder: jsonDecoder, completion: completion)
//       }
//    public static func getNotificationCount(parameters:[String:Any], completion:@escaping (Result<NotificationCountModel>) -> Void) {
//              performRequest(route: APIRouter.getNotificationCount(parameters), decoder: jsonDecoder, completion: completion)
//          }
//    
//    public static func addTrip(parameters:[String:Any], completion:@escaping (Result<TripAdd>) -> Void) {
//        performRequest(route: APIRouter.tripAdd(parameters), decoder: jsonDecoder, completion: completion)
//    }
//    
//    public static func tripCount(parameters:[String:Any], completion:@escaping (Result<TripCount>) -> Void) {
//        performRequest(route: APIRouter.tripCount(parameters), decoder: jsonDecoder, completion: completion)
//    }
//    
//    public static func logout(parameters:[String:Any], completion:@escaping (Result<CommonModel>) -> Void) {
//        performRequest(route: APIRouter.logout(parameters), decoder: jsonDecoder, completion: completion)
//    }
//    
//    public static func addProduct(documentData:[Document], completion:@escaping(Result<CommonModel>) -> Void) {
//        uploadRequest(documentData: documentData, route: APIRouter.productAdd, decoder: jsonDecoder, completion: completion)
//    }
//    
//    public static func getOfferRequest(parameters:[String:Any], completion:@escaping (Result<OfferRequest>) -> Void) {
//        performRequest(route: APIRouter.getOfferRequest(parameters), decoder: jsonDecoder, completion: completion)
//    }
//    
//    public static func makePayemnt(parameters:[String:Any], completion:@escaping (Result<CommonModel>) -> Void) {
//        performRequest(route: APIRouter.makePayemnt(parameters), decoder: jsonDecoder, completion: completion)
//    }
//    
//    public static func getOrderReceive(parameters:[String:Any], completion:@escaping (Result<OrderPlaced>) -> Void) {
//        performRequest(route: APIRouter.getOrderReceive(parameters), decoder: jsonDecoder, completion: completion)
//    }
//    
//    public static func orderOtpVerify(parameters:[String:Any], completion:@escaping (Result<CommonModel>) -> Void) {
//        performRequest(route: APIRouter.orderOtpVerify(parameters), decoder: jsonDecoder, completion: completion)
//    }
//    
//    public static func findTravllerTrip(parameters:[String:Any], completion:@escaping (Result<FindTraveller>) -> Void) {
//        performRequest(route: APIRouter.findTravellerTrip(parameters), decoder: jsonDecoder1, completion: completion)
//    }
//    
//    public static func byTrip(parameters:[String:Any], completion:@escaping (Result<RequestReceive>) -> Void) {
//        performRequest(route: APIRouter.byTrip(parameters), decoder: jsonDecoder, completion: completion)
//    }
//    
//    public static func requestStatusChange(parameters:[String:Any], completion:@escaping (Result<CommonModel>) -> Void) {
//        performRequest(route: APIRouter.requestStatusChange(parameters), decoder: jsonDecoder, completion: completion)
//    }
//    
//    public static func getOrderPlaced(parameters:[String:Any], completion:@escaping (Result<OrderPlaced>) -> Void) {
//        performRequest(route: APIRouter.getOrderPlaced(parameters), decoder: jsonDecoder, completion: completion)
//    }
//    
//    public static func staticPage(parameters:[String:Any], completion:@escaping (Result<StaticPage>) -> Void) {
//        performRequest(route: APIRouter.staticPage(parameters), decoder: jsonDecoder, completion: completion)
//    }
//    
//    public static func updateProfile(documentData:[Document], completion:@escaping(Result<EditProfile>) -> Void) {
//        uploadRequest(documentData: documentData, route: APIRouter.updateProfile, decoder: jsonDecoder, completion: completion)
//    }
//    
//    public static func orderCancel(parameters:[String:Any], completion:@escaping (Result<CommonModel>) -> Void) {
//        performRequest(route: APIRouter.orderCancel(parameters), decoder: jsonDecoder, completion: completion)
//    }
//    
//    public static func orderCancelRequest(parameters:[String:Any], completion:@escaping (Result<CommonModel>) -> Void) {
//        performRequest(route: APIRouter.orderCancelRequest(parameters), decoder: jsonDecoder, completion: completion)
//    }
//    
//    public static func rating(parameters:[String:Any], completion:@escaping (Result<CommonModel>) -> Void) {
//        performRequest(route: APIRouter.rating(parameters), decoder: jsonDecoder, completion: completion)
//    }
//    
////    public static func tripWiseRequestReceive(parameters:[String:Any], completion:@escaping (Result<RequestReceive>) -> Void) {
////           performRequest(route: APIRouter.tripWiseRequestReceive(parameters), decoder: jsonDecoder, completion: completion)
////       }
////
//    
//    public static func tripWiseRequestReceive(parameters:[String:Any], completion:@escaping (Result<RequestReceive>) -> Void) {
//        performRequest(route: APIRouter.tripWiseRequestReceive(parameters), decoder: jsonDecoder, completion: completion)
//    }
//    
//    public static func getRelatedTripOrder(parameters:[String:Any], completion:@escaping (Result<RequestReceive>) -> Void) {
//        performRequest(route: APIRouter.getRelatedTripOrder(parameters), decoder: jsonDecoder, completion: completion)
//    }
//    
//    public static func changePassword(parameters:[String:Any], completion:@escaping (Result<CommonModel>) -> Void) {
//        performRequest(route: APIRouter.changePassword(parameters), decoder: jsonDecoder, completion: completion)
//    }
//    
//    public static func tripCancel(parameters:[String:Any], completion:@escaping (Result<CommonModel>) -> Void) {
//        performRequest(route: APIRouter.tripCancel(parameters), decoder: jsonDecoder, completion: completion)
//    }
//    //trip_notification_status
//    public static func tripNotificationStatus(parameters:[String:Any], completion:@escaping (Result<CommonModel>) -> Void) {
//           performRequest(route: APIRouter.tripNotificationStatus(parameters), decoder: jsonDecoder, completion: completion)
//       }
//    
//    
//    public static func addAddress(parameters:[String:Any], completion:@escaping (Result<Address>) -> Void) {
//        performRequest(route: APIRouter.addAddress(parameters), decoder: jsonDecoder, completion: completion)
//    }
//    
//    public static func updateAddress(parameters:[String:Any], completion:@escaping (Result<Address>) -> Void) {
//        performRequest(route: APIRouter.updateAddress(parameters), decoder: jsonDecoder, completion: completion)
//    }
//    
//    public static func getShopOrderListing(parameters:[String:Any], completion:@escaping (Result<ShopOrderListing>) -> Void) {
//        performRequest(route: APIRouter.shopOrderListing(parameters), decoder: jsonDecoder, completion: completion)
//    }
//    
    
    
}

