////
////  ViewController.swift
////  Decodeble
////  ComicHub
////
////  Created by Anil Jangir on 12/06/20.
////  Copyright © 2020 Anil Jangir. All rights reserved.
////
//
//import Alamofire
//
//enum APIRouter: URLRequestConvertible {
//
//    case signup([String:Any])
//    case login([String:Any])
//    case verifyOtp([String:Any])
//    case resendOtp([String:Any])
//    case forgotPassword([String:Any])
//    case socialLogin([String:Any])
//    case socialLoginCheck([String:Any])
//    case storeList([String:Any])
//    case tripList([String:Any])
//    case getRequestReceive([String:Any])
//    case myTripListing([String:Any])
//    case allStoreList([String:Any])
//    case productList([String:Any])
//    case productDetail([String:Any])
//    case citySearch([String:Any])
//    case routeTripList([String:Any])
//    case sendRequestOffer([String:Any])
//    case sendOffer([String:Any])
//    case resendOffer([String :Any])
//    case getProfile([String:Any])
//    case notificationList([String:Any])
//    case tripAdd([String:Any])
//    case tripCount([String:Any])
//    case productAdd
//    case logout([String:Any])
//    case getOfferRequest([String:Any])
//    case makePayemnt([String:Any])
//    case getOrderReceive([String:Any])
//    case orderOtpVerify([String:Any])
//    case findTravellerTrip([String:Any])
//    case byTrip([String:Any])
//    case requestStatusChange([String:Any])
//    case getOrderPlaced([String:Any])
//    case staticPage([String:Any])
//    case updateProfile
//    case orderCancel([String:Any])
//    case orderCancelRequest([String:Any])
//    case rating([String:Any])
//    case tripWiseRequestReceive([String:Any])
//    case getRelatedTripOrder([String:Any])
//    case changePassword([String:Any])
//    case tripCancel([String : Any])
//    case tripNotificationStatus([String : Any])
//    case notificationRead([String:Any])
//    case getNotificationCount([String:Any])
//    case addAddress([String:Any])
//    case updateAddress([String:Any])
//    case shopOrderListing([String:Any])
//
//    // MARK: - HTTPMethod
//    private var method: HTTPMethod {
//        switch self {
//        case .signup, .login, .verifyOtp, .resendOtp, .forgotPassword, .socialLogin, .socialLoginCheck, .storeList, .tripList, .getRequestReceive, .myTripListing, .allStoreList, .productList, .productDetail, .citySearch, .routeTripList, .sendRequestOffer, .sendOffer, .resendOffer, .getProfile, .notificationList, .tripAdd, .tripCount, .productAdd, .logout, .getOfferRequest, .makePayemnt, .getOrderReceive, .orderOtpVerify, .findTravellerTrip, .byTrip, .requestStatusChange, .getOrderPlaced, .staticPage, .updateProfile, .orderCancel, .orderCancelRequest, .rating, .tripWiseRequestReceive, .getRelatedTripOrder, .changePassword , .tripCancel , .tripNotificationStatus, .notificationRead , .getNotificationCount , .addAddress, .updateAddress , .shopOrderListing:
//            return .post
//
//        }
//    }
//
//    private var basePath:String {
//        return ""
//    }
//
//    // MARK: - Path
//    private var path: String {
//        switch self {
//        case .signup:
//            return "/signup"
//
//        case .login:
//            return "/login"
//
//        case .verifyOtp:
//            return "/otp_confirmation"
//
//        case .resendOtp:
//            return "/resend_otp"
//
//        case .forgotPassword:
//            return "/forgot_password"
//
//        case .socialLogin:
//            return "/social_login"
//
//        case .socialLoginCheck:
//            return "/social_login_check"
//
//        case .storeList:
//            return "/store_list_by_type"
//
//        case .tripList:
//            return "/trip_listing"
//
//        case .getRequestReceive:
//            return "/ship_home"
//
//        case .myTripListing:
//            return "/my_trip_listing"
//
//        case .allStoreList:
//            return "/store_listing"
//
//        case .productList:
//            return "/product_listing"
//
//        case .productDetail:
//            return "/product_details"
//
//        case .citySearch:
//            return "/city_search"
//
//        case .routeTripList:
//            return "/get_trip_listing"
//
//        case .sendRequestOffer:
//            return "/send_request_offer"
//
//        case .sendOffer:
//            return "/send_offer"
//
//        case .resendOffer:
//            return "/re_send_offer"
//
//        case .getProfile:
//            return "/get_profile"
//
//        case .notificationList:
//            return "/notification_list"
//
//        case .tripAdd:
//            return "/trip_add"
//
//        case .tripCount:
//            return "/trip_count"
//
//        case .productAdd:
//            return "/product_add"
//
//        case .logout:
//            return "/logout"
//
//        case .getOfferRequest:
//            return "/get_offer_request"
//
//        case .makePayemnt:
//            return "/payment"
//
//        case .getOrderReceive:
//            return "/my_trip_awaiting_deliveries"
//
//        case .orderOtpVerify:
//            return "/order_otp_verify"
//
//        case .findTravellerTrip:
//            return "/find_traveller_trip"
//
//        case .byTrip:
//            return "/get_request_receive"
//
//        case .requestStatusChange:
//            return "/request_status_change"
//
//        case .getOrderPlaced:
//            return "/get_order_placed"
//
//        case .staticPage:
//            return "/pages_view"
//
//        case .updateProfile:
//            return "/update_profile"
//
//        case .orderCancel:
//            return "/order_cancel"
//
//        case .orderCancelRequest:
//            return "/order_cancel_status"
//
//        case .rating:
//            return "/rating"
//
//        case .tripWiseRequestReceive:
//            return "/trip_wise_request_receive"
//
//        case .getRelatedTripOrder:
//           // return "/get_related_trip_order"
//            return "/my_trip_matched_items"
//
//        case .changePassword:
//            return "/change_password"
//        case .tripCancel:
//            return "/trip_cancel"
//        case .tripNotificationStatus:
//            return "/trip_notification_status"
//        case .notificationRead:
//            return "/notificationRead"
//
//        case .getNotificationCount:
//            return "/notification_count"
//
//        case .addAddress:
//            return "/address_add"
//        case .updateAddress:
//            return "/update_address"
//        case .shopOrderListing:
//            return "/shop_order_listing"
//        }
//    }
//
//    // MARK: - Parameters
//     var parameters: Parameters? {
//        switch self {
//        case .signup(let parameter):
//            return parameter
//
//        case .login(let parameter):
//            return parameter
//
//        case .verifyOtp(let parameter):
//            return parameter
//
//        case .resendOtp(let parameter):
//            return parameter
//
//        case .forgotPassword(let parameter):
//            return parameter
//
//        case .socialLogin(let parameter):
//            return parameter
//
//        case .socialLoginCheck(let parameter):
//            return parameter
//
//        case .storeList(let parameter):
//            return parameter
//
//        case .tripList(let parameter):
//            return parameter
//
//        case .getRequestReceive(let parameter):
//            return parameter
//
//        case .myTripListing(let parameter):
//            return parameter
//
//        case .allStoreList(let parameter):
//            return parameter
//
//        case .productList(let parameter):
//            return parameter
//
//        case .productDetail(let parameter):
//            return parameter
//
//        case .citySearch(let parameter):
//            return parameter
//
//        case .routeTripList(let parameter):
//            return parameter
//
//        case .sendRequestOffer(let parameter):
//            return parameter
//
//        case .sendOffer(let parameter):
//            return parameter
//
//        case .resendOffer(let parameter):
//            return parameter
//
//        case .getProfile(let parameter):
//            return parameter
//
//        case .notificationList(let parameter):
//            return parameter
//
//        case .tripAdd(let parameter):
//            return parameter
//
//        case .tripCount(let parameter):
//            return parameter
//
//        case .productAdd, .updateProfile:
//            return nil
//
//        case .logout(let parameter):
//            return parameter
//
//        case .getOfferRequest(let parameter):
//            return parameter
//
//        case .makePayemnt(let parameter):
//            return parameter
//
//        case .getOrderReceive(let parameter):
//            return parameter
//
//        case .orderOtpVerify(let parameter):
//            return parameter
//
//        case .findTravellerTrip(let parameter):
//            return parameter
//
//        case .byTrip(let parameter):
//            return parameter
//
//        case .requestStatusChange(let parameter):
//            return parameter
//
//        case .getOrderPlaced(let parameter):
//            return parameter
//
//        case .staticPage(let parameter):
//            return parameter
//
//        case .orderCancel(let parameter):
//            return parameter
//
//        case .orderCancelRequest(let parameter):
//            return parameter
//
//        case .rating(let parameter):
//            return parameter
//
//        case .tripWiseRequestReceive(let parameter):
//            return parameter
//
//        case .getRelatedTripOrder(let parameter):
//            return parameter
//
//        case .changePassword(let paramteter):
//            return paramteter
//        case .tripCancel(let parameter):
//            return parameter
//
//        case .tripNotificationStatus(let parameter) :
//            return parameter
//
//        case .notificationRead(let parameter) :
//            return parameter
//
//        case .getNotificationCount(let parameter) :
//            return parameter
//
//        case .addAddress(let parameter):
//            return parameter
//        case .updateAddress(let parameter):
//            return parameter
//
//        case .shopOrderListing(let paramater):
//            return paramater
//        }
//    }
//
//    private var getParameters: Parameters? {
//        switch self {
//        case .login, .verifyOtp, .signup, .resendOtp, .forgotPassword, .socialLogin, .socialLoginCheck, .storeList, .tripList, .getRequestReceive, .myTripListing, .allStoreList, .productList, .productDetail, .citySearch, .routeTripList, .sendRequestOffer, .sendOffer, .resendOffer, .getProfile, .notificationList, .tripAdd, .tripCount, .productAdd, .logout, .getOfferRequest, .makePayemnt, .getOrderReceive, .orderOtpVerify, .findTravellerTrip, .byTrip, .requestStatusChange, .getOrderPlaced, .staticPage, .updateProfile, .orderCancel, .orderCancelRequest, .rating, .tripWiseRequestReceive, .getRelatedTripOrder, .changePassword, .tripCancel , .tripNotificationStatus , .notificationRead , .getNotificationCount , .addAddress , .updateAddress , .shopOrderListing:
//            return nil
//        }
//    }
//
////    private var token:String? {
////        switch self {
////        case .login, .verifyOtp, .signup, .resendOtp, .forgotPassword, .socialLogin, .socialLoginCheck:
////            return nil
////
////        case .storeList, .tripList, .getRequestReceive, .myTripListing, .allStoreList, .productList, .productDetail, .citySearch, .routeTripList, .sendRequestOffer, .sendOffer, .resendOffer, .getProfile, .notificationList, .tripAdd, .tripCount, .productAdd, .logout, .getOfferRequest, .makePayemnt, .getOrderReceive, .orderOtpVerify, .findTravellerTrip, .byTrip, .requestStatusChange, .getOrderPlaced, .staticPage, .updateProfile, .orderCancel, .orderCancelRequest, .rating, .tripWiseRequestReceive, .getRelatedTripOrder, .changePassword, .tripCancel, .tripNotificationStatus , .notificationRead , .getNotificationCount , .addAddress , .updateAddress , .shopOrderListing:
////            return UserManager.accessToken
////        }
////    }
//
//    var parameterEncoding: ParameterEncoding {
//        switch method {
//        case .get:
//            return URLEncoding.queryString
//        case .post:
//            return JSONEncoding.default
//
//        default:
//            return JSONEncoding.default
//        }
//    }
//
//    // MARK: - URLRequestConvertible
//    func asURLRequest() throws -> URLRequest {
//        var url:URL!
//        do {
//             url = try K.ProductionServer.baseURL.asURL()
//        } catch {
//            print(error.localizedDescription)
//        }
//
//        var urlRequest = URLRequest(url: url.appendingPathComponent(basePath + path))
//        // HTTP Method
//        urlRequest.httpMethod = method.rawValue
//        urlRequest = try URLEncoding.queryString.encode(urlRequest, with: getParameters)
//
//        // Common Headers
//       // urlRequest.setValue(ContentType.json.rawValue, forHTTPHeaderField: HTTPHeaderField.acceptType.rawValue)
//        urlRequest.setValue(ContentType.json.rawValue, forHTTPHeaderField: HTTPHeaderField.contentType.rawValue)
////        if let token = token {
////            print("X-Access-Token:\(token)")
////            urlRequest.setValue(token, forHTTPHeaderField: HTTPHeaderField.token.rawValue)
////        }
//        urlRequest = try parameterEncoding.encode(urlRequest, with: parameters)
//        return urlRequest
//    }
//}
//
