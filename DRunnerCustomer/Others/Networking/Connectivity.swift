//
//  Connectivity.swift
//  ComicHub
//
//  Created by Anil Jangir on 12/06/20.
//  Copyright © 2020 Anil Jangir. All rights reserved.
//
import Foundation
import Alamofire

class Connectivity {
    static let msgNoNetwork = "The Internet connection appears to be offline"

    class var isConnectedToInternet:Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}
