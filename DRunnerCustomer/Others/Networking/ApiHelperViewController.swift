//
//  ApiHelperViewController.swift
//  Rene_Harley
//
//  Created by Anil-Mac on 31/10/18.
//  Copyright © 2018 Octal. All rights reserved.
//

import UIKit
import Foundation
import Alamofire

//class Connectivity {
//    class func isConnectedToInternet() ->Bool {
//        return NetworkReachabilityManager()!.isReachable
//    }
//}

class API_HELPER {

static let sharedInstance = API_HELPER()
    fileprivate init(){
        
    }

  
    func getDataFromBacekend(request : URLRequest, inVC vc:UIViewController, showHud hud:Bool, completion : @escaping(_ responce : NSDictionary,_ message : String, _ status : Bool) -> ())
    {
        if Connectivity.isConnectedToInternet
        {
              let session = URLSession.shared
            
                let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
                DispatchQueue.main.async {
                    LOADER.hide(delegate: vc)
                }
                if  (response as? HTTPURLResponse)?.statusCode == 200
                {
                    if response != nil
                    {
                        //print(response!)
                        do {
                            let json = try JSONSerialization.jsonObject(with: data!) as! Dictionary<String, AnyObject>
                            
                           
                            
                            completion(json as NSDictionary , json["msg"] as? String ?? "", true)
                          
                            
                        } catch {
                            print("error")
                            DispatchQueue.main.async {
                               // Helper.shared.showAlert(msg: "An internal server error occurred. Please try again later.")
                            }
                        }
                    }
                    else{
                        DispatchQueue.main.async {
                           // Helper.shared.showAlert(msg: "An internal server error occurred. Please try again later.")
                        }
                        
                    }
                }
                else  if  (response as? HTTPURLResponse)?.statusCode == 409
                {
                    do {
                        let json = try JSONSerialization.jsonObject(with: data!) as! Dictionary<String, AnyObject>
                        print(json)
                        completion(json["result"] as! NSDictionary,json["msg"] as! String , false)
                        
                    } catch {
                        print("error")
                        
                    }
                    
                }
                else if (response as? HTTPURLResponse)?.statusCode == 401
                {
                    //Helper.shared.removeAllData()
                }
                else{
                    DispatchQueue.main.async {
                      //  Helper.shared.showAlert(msg: "An internal server error occurred. Please try again later.")
                    }
                }
                
            })
            
            task.resume()
            
        }
        else{
            Helper.shared.showAlert(msg: AppMessages.No_Internet)
        }
        
    }
    
    func newPostMethod(header: String, withParameter parameter: [[String:Any]], inVC vc: UIViewController, showHud hud: Bool, addAuthHeader authHeader : Bool,  completion: @escaping (_ responce : NSDictionary,_ message : String, _ status : Bool) -> ()) {
        
        if Connectivity.isConnectedToInternet
        {
           if hud == true
           {
            DispatchQueue.main.async
                {
            LOADER.show(views: vc.view)
            }
           }
            let urlString           = NSString(format: "%@%@",K.ProductionServer.kBaseURL,header)
            print("----------- API URL ------------")
            print(urlString)
            
            print("----------- JSON ------------")
            print(parameter)
            var request = URLRequest(url: URL(string: urlString as String)!)
            request.httpMethod = "POST"
            request.httpBody = try? JSONSerialization.data(withJSONObject: parameter, options: [])
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue(authHeader == true ? (""+Helper.shared.getAuthToken()+"") : "", forHTTPHeaderField: "Token")
        
            let session = URLSession.shared
            let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
            DispatchQueue.main.async
                {
            LOADER.hide(delegate: vc)
            }
            print((response as? HTTPURLResponse)?.statusCode)
            if  (response as? HTTPURLResponse)?.statusCode == 200
            {
            if response != nil
              {
            //print(response!)
               do {
                
                
                let jsonNew = try JSONSerialization.jsonObject(with: data!, options: [])
                let results = try? JSONSerialization.jsonObject(with: data!, options: [])
                let jsonData: Data? = try? JSONSerialization.data(withJSONObject: results! , options: .prettyPrinted)
                
                let myString = String(data: jsonData!, encoding: String.Encoding.utf8)
                print("Result: \(myString ?? "")")
                
                
                let json = try JSONSerialization.jsonObject(with: data!) as! Dictionary<String, AnyObject>
                print(json)
                if json["Data"] != nil{
                let result = json["Data"] as Any
                if json["Data"] is NSNull
                {
                    completion([:],json["Message"] as! String , json["Status"] as! String == "success" ? (true)  : (false)  )
                    
                }
                    else
                {
                if result is NSArray
                {
                    completion(json as NSDictionary,json["Message"] as! String , json["Status"] as! String == "success" ? (true)  : (false) )
                }
                else
                {
                   
                        completion(result as! NSDictionary,json["Message"] as! String , json["Status"] as! String == "success" ? (true)  : (false)  )
                   
                   
                    
                   
                }
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                                                       Helper.shared.showAlert(msg: json["Message"] as? String ?? "" )
                                                       }
                }
              
            
                 } catch {
                print("error")
                DispatchQueue.main.async {
                    //Helper.shared.showAlert(msg: Error_MSG)
                   }
               }
            }
            else{
                DispatchQueue.main.async {
                    // Helper.shared.showAlert(msg: Error_MSG)
                }
                
            }
            }
            else if (response as? HTTPURLResponse)?.statusCode == 400
            {
                do {
                    let json = try JSONSerialization.jsonObject(with: data!) as! Dictionary<String, AnyObject>
                   
                    completion(json as! NSDictionary,json["Message"] as! String , json["Status"] as! String == "success" ? (true)  : (false)  )
                    
                } catch {
                    print("error")
                    DispatchQueue.main.async {
                       // Helper.shared.showAlert(msg: Error_MSG)
                    }
                }
            }
            else if (response as? HTTPURLResponse)?.statusCode == 401
            {
                //DispatchQueue.main.async {
                   // Helper.shared.removeAllData()

               // }
            }
            else  if  (response as? HTTPURLResponse)?.statusCode == 500
            {
                do {
                     let json = try JSONSerialization.jsonObject(with: data!) as! Dictionary<String, AnyObject>
                    DispatchQueue.main.async {
                         Helper.shared.showAlert(msg: json["Message"] as? String ?? "")

                     }
    //            let json = try JSONSerialization.jsonObject(with: data!) as! Dictionary<String, AnyObject>
    //            print(json)
    //            completion(json as! NSDictionary,json["Message"] as! String , json["Status"] as! String == "success" ? (true)  : (false)  )
                
            } catch {
                print("error")
               
                }
                
            }
            else
            {
                DispatchQueue.main.async {
                    //Helper.shared.showAlert(msg: Error_MSG)
                }
            }
        })
        
        task.resume()
        }
        else{
              Helper.shared.showAlert(msg: AppMessages.No_Internet)
        }
        }
    
  func postDataInJsonUsingNS(header: String, withParameter parameter: NSDictionary, inVC vc: UIViewController, showHud hud: Bool, addAuthHeader authHeader : Bool,  completion: @escaping (_ responce : NSDictionary,_ message : String, _ status : Bool) -> ()) {
    
    if Connectivity.isConnectedToInternet
    {
       if hud == true
       {
        DispatchQueue.main.async
            {
        LOADER.show(views: vc.view)
        }
       }
        let urlString           = NSString(format: "%@%@",K.ProductionServer.kBaseURL,header)
        print("----------- API URL ------------")
        print(urlString)
        
        print("----------- JSON ------------")

        let jsonData = try! JSONSerialization.data(withJSONObject: parameter, options: JSONSerialization.WritingOptions.prettyPrinted)
        let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String

        print(jsonString)

        var request = URLRequest(url: URL(string: urlString as String)!)
        request.httpMethod = "POST"
        request.httpBody = try? JSONSerialization.data(withJSONObject: parameter, options: [])
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue(authHeader == true ? (""+Helper.shared.getAuthToken()+"") : "", forHTTPHeaderField: "auth")
    
        let configuration = URLSessionConfiguration.default
            configuration.timeoutIntervalForRequest = TimeInterval(180)
            configuration.timeoutIntervalForResource = TimeInterval(180)
           
               let session = URLSession(configuration: configuration)
      //  let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
            let srt = String(data: data!, encoding: .utf8)
            print(srt)
        DispatchQueue.main.async
            {
                if hud == true
                {
                    LOADER.hide(delegate: vc)
                }
        }
        print((response as? HTTPURLResponse)?.statusCode)
            print(error?.localizedDescription as Any)

        if  (response as? HTTPURLResponse)?.statusCode == 200
        {
        if response != nil
          {
        //print(response!)
           do {
            if data == nil{
                return
            }
            let json = try JSONSerialization.jsonObject(with: data!) as! Dictionary<String, AnyObject>
            
            print(json)
            
            if json["data"] != nil{
            let result = json["data"] as Any
            if json["data"] is NSNull
            {
                completion([:],json["message"] as? String ?? "" , json["status"] as? Bool ?? false  )
                
            }
                else
            {
            if result is NSArray
            {
                completion(json as NSDictionary,json["message"] as? String ?? "" , json["status"] as? Bool ?? false )
            }
            else
            {
                let status =  json["status"] as? Int ?? 0
                
                if status == -1
                {
                    DispatchQueue.main.async {
                        Helper.shared.showAlert(msg: json["message"] as? String ?? "" )
                        Helper.shared.setUserID(value: "0")
                        Helper.shared.setAuthToken(value: "")
                        APP_DELEGATE.setViewControllers()
                        
                    }
                }
                else
                {
                     completion(result as! NSDictionary,json["message"] as? String ?? "" , json["status"] as? Bool ?? false   )
                }
        
               }
                }
            }
            else
            {
                DispatchQueue.main.async {
                    Helper.shared.showAlert(msg: json["message"] as? String ?? "" )
            }
            }
          
        
             } catch {
            print("error")
            DispatchQueue.main.async {
                //Helper.shared.showAlert(msg: Error_MSG)
               }
           }
        }
        else{
            DispatchQueue.main.async {
                // Helper.shared.showAlert(msg: Error_MSG)
            }
            
        }
        }
        else if (response as? HTTPURLResponse)?.statusCode == 400
        {
            do {
                let json = try JSONSerialization.jsonObject(with: data!) as! Dictionary<String, AnyObject>
               
                completion(json as! NSDictionary,json["message"] as! String , json["status"] as! String == "success" ? (true)  : (false)  )
                
            } catch {
                print("error")
                DispatchQueue.main.async {
                   // Helper.shared.showAlert(msg: Error_MSG)
                }
            }
        }
        else if (response as? HTTPURLResponse)?.statusCode == 401
        {
            //DispatchQueue.main.async {
               // Helper.shared.removeAllData()

           // }
        }
        else  if  (response as? HTTPURLResponse)?.statusCode == 500
        {
            do {
                 let json = try JSONSerialization.jsonObject(with: data!) as! Dictionary<String, AnyObject>
                DispatchQueue.main.async {
                     Helper.shared.showAlert(msg: json["message"] as? String ?? "")

                 }

            
        } catch {
            print("error")
           
            }
            
        }
        else
        {
            DispatchQueue.main.async {
                //Helper.shared.showAlert(msg: Error_MSG)
            }
        }
    })
    
    task.resume()
    }
    else{
          Helper.shared.showAlert(msg: AppMessages.No_Internet)
    }
    }

  
    
    func uploadMultipartData(methodType : HTTPMethod, header: String, withParameter parameter: NSDictionary,imageArray imgVArray: NSArray, inVC vc: UIViewController, showHud hud: Bool, addAuthHeader authHeader : Bool, videoAdd isVideo : Bool, audioAdd isAideo : Bool, fileAdd isFile : Bool, gifAdd isGif : Bool, completion: @escaping (_ responce : NSDictionary,_ message : NSString, _ status : Bool) -> ()) {
        if Connectivity.isConnectedToInternet
        {
            
        if hud == true
        {
            DispatchQueue.main.async
            {
            LOADER.show(views: vc.view)
            }
        }
        
        let urlString           = NSString(format: "%@%@",K.ProductionServer.kBaseURL,header)
        let jsonData            = try! JSONSerialization.data(withJSONObject: parameter, options: .prettyPrinted)
 
        let url = try! URLRequest(url: URL(string:urlString as String)!, method: methodType, headers: authHeader == true ? ( ["auth" : Helper.shared.getAuthToken()]) : nil )
        print("------------ API URL -----------\n")
        print("\(urlString)\n")
        
        print("------------ API REQUEST -----------\n")
        print("\(url)\n")
            
            
        print("----------- JSON ------------")

        let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String

        print(jsonString)
       
          
        Alamofire.upload(
            multipartFormData: { multipartFormData in

                //multipartFormData.append(jsonData, withName: "Detail")
                
                for (key, value) in parameter {
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key as! String)
                }

                for index in 0 ..< imgVArray.count {
                  
                        let imageData = (imgVArray.object(at: index)  as! UIImage).jpegData(compressionQuality: 0.2)
                        if imageData != nil{
                        multipartFormData.append(imageData!, withName: "image", fileName: "image.png", mimeType: "image/png")
                      
                    }

                }


        },
            with: url,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseString(completionHandler: { (response) in
                        DispatchQueue.main.async {
                          LOADER.hide(delegate: vc)
                        }
                    print(response)
                })
                upload.responseJSON
                { response in

                    if response.result.value != nil
                    {
                        print("\(header) Responce->\(response.result.value!)")
                        let JSON = response.result.value! as AnyObject
                        let responce = APP_DELEGATE.convertAllDictionaryValueToNil((JSON as! NSDictionary).mutableCopy() as! NSMutableDictionary)

                        let sucess = responce.object(forKey: "status") as? Bool
                        var message = ""
                        if responce.object(forKey: "message") is NSNull
                        {
                            message = ""
                        }
                        else
                        {
                            message =  responce.object(forKey: "message") as! String

                        }

                        if  sucess == true
                        {

                            completion(responce.object(forKey: "data") as! NSDictionary,message as NSString , true )
                        }
                        else{
                            DispatchQueue.main.async {
                                     Helper.shared.showAlert(msg: message)
                            }
                        }

                    }

                }
                break
                case .failure( _):
                    DispatchQueue.main.async {
                        LOADER.hide(delegate: vc)

                    }

                    break
                }

        }
        )
    }
        else
        {
            Helper.shared.showAlert(msg: AppMessages.No_Internet)
        }
    }
    
    
    
    
    
   
    
func postDataForForgotPassword(header: String, withParameter parameter: NSDictionary, inVC vc: UIViewController, showHud hud: Bool, addAuthHeader authHeader : Bool,  completion: @escaping (_ responce : NSDictionary,_ message : String, _ status : Bool) -> ()) {
        
    if Connectivity.isConnectedToInternet
        {
            self.postDataInJsonUsingNS(header: header, withParameter: parameter, inVC: vc, showHud: true, addAuthHeader: authHeader) { (resultD, Message, status) in
                completion (resultD, Message, status)
            }
            
   
        }
        else{
            Helper.shared.showAlert(msg: AppMessages.No_Internet)
        }
    }
    
    
    // Profile Edit
    
    
    
    func editUserProfile(parameter: NSDictionary,imageArray imgVArray: NSArray, inVC vc: UIViewController, showHud: Bool , completion : @escaping ( _ status : Bool ) ->() )
        {
        API_HELPER.sharedInstance.uploadMultipartData(methodType: .post, header: K.ApiName.EditProfile, withParameter: parameter as NSDictionary, imageArray: imgVArray, inVC: vc, showHud: true, addAuthHeader: true, videoAdd: false, audioAdd: false, fileAdd: false, gifAdd: false) { (result, message, status) in
                DispatchQueue.main.async {
                    if status
                    {
                        let dict : NSMutableDictionary = APP_DELEGATE.convertAllDictionaryValueToNil(result.mutableCopy() as! NSMutableDictionary) as! NSMutableDictionary
                            defaults.set(dict, forKey: udUserInfo)
                            Helper.shared.setUserID(value: dict["user_id"] as? String ?? "0")
                            Helper.shared.setUserPoints(value: dict["reward_points"] as? String ?? "0")
                            Helper.shared.setUserWallet(value: dict["wallet_amount"] as? String ?? "0")
                            defaults.setValue("1", forKey: udLogin)

                            if dict["auth_token"] != nil
                            {
                                Helper.shared.setAuthToken(value: dict.object(forKey: "auth_token") as! String)
                            }
//                        APP_DELEGATE.setViewControllers()
                        completion(true)
                         
                        
                    }
                    else
                    {
                        vc.showToast(message as String)
                    }
                }
                
            }
            
        }
        
    
    
    
}


extension Dictionary {
    func toJson() -> String {
        let jsonData = try! JSONSerialization.data(withJSONObject: self, options: [])
        let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
        print("JSON string = \(jsonString)")
        
        return jsonString
    }
    
    func nullKeyRemoval() -> [AnyHashable: Any] {
        var dict: [AnyHashable: Any] = self
        
        let keysToRemove = dict.keys.filter { dict[$0] is NSNull }
        let keysToCheck = dict.keys.filter({ dict[$0] is Dictionary })
        for key in keysToRemove {
            dict.removeValue(forKey: key)
        }
        for key in keysToCheck {
            if let valueDict = dict[key] as? [AnyHashable: Any] {
                dict.updateValue(valueDict.nullKeyRemoval(), forKey: key)
            }
        }
        return dict
    }
}

extension NSDictionary {
    func toJson() {
        let jsonData = try! JSONSerialization.data(withJSONObject: self, options: JSONSerialization.WritingOptions.prettyPrinted)
        let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)
        print("JSON string = \(jsonString ?? "")")
    }
    
    func removeNullValueFromDict()-> NSDictionary
    {
        let mutableDictionary:NSMutableDictionary = NSMutableDictionary(dictionary: self)
        for key in mutableDictionary.allKeys
        {
            if("\(mutableDictionary.object(forKey: "\(key)")!)" == "<null>")
            {
                mutableDictionary.setValue("", forKey: key as! String)
            }
            else if((mutableDictionary.object(forKey: "\(key)")! as AnyObject).isKind(of: NSNull.classForCoder()))
            {
                mutableDictionary.setValue("", forKey: key as! String)
            }
            else if ((mutableDictionary.object(forKey: "\(key)")! as AnyObject).isKind(of: NSDictionary.classForCoder()))
            {
                mutableDictionary.setValue((mutableDictionary.object(forKey: "\(key)")! as! NSDictionary).removeNullValueFromDict(), forKey: key as! String)
            }
        }
        return mutableDictionary
    }
}
