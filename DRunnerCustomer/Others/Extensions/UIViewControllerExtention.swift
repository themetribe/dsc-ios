//
//  UIViewControllerExtention.swift
//  ComicHub
//
//  Created by Anil Jangir on 16/06/20.
//  Copyright © 2020 Anil Jangir. All rights reserved.
//

import UIKit

//import Toaster

extension UIViewController
{
    func showToast(_ text:String) {
            Alert.showSimpleAlert(title: kAppName, message: text, actionTitle: "OK", controller: self)
   
        }
    func isPresented() -> Bool {
               if self.presentingViewController != nil {
                   return true
               } else if self.navigationController?.presentingViewController?.presentedViewController == self.navigationController  {
                   return true
               } else if self.tabBarController?.presentingViewController is UITabBarController {
                   return true
               }
               
               return false
           }

           func topMostViewController() -> UIViewController {
               if let navigation = self as? UINavigationController {
                   if let visibleController = navigation.visibleViewController {
                       return visibleController.topMostViewController()
                   }
               }
               if let tab = self as? UITabBarController {
                   if let selectedTab = tab.selectedViewController {
                       return selectedTab.topMostViewController()
                   }
                   return tab.topMostViewController()
               }
               if let navigation = self.presentedViewController as? UINavigationController {
                   if let visibleController = navigation.visibleViewController {
                       return visibleController.topMostViewController()
                   }
               }
               if let tab = self.presentedViewController as? UITabBarController {
                   if let selectedTab = tab.selectedViewController {
                       return selectedTab.topMostViewController()
                   }
                   return tab.topMostViewController()
               }
               if self.presentedViewController == nil {
                   return self
               }
               
               return self.presentedViewController!.topMostViewController()
           }
     func showActionSheetWith(items:[String], title:String, message:String, completion:@escaping ((String) -> Void)) {
            let actionSheet = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
            for item in items {
                actionSheet.addAction(UIAlertAction(title: item, style: .default, handler: { (action) in
                    completion(action.title!)
                }))
            }
            
            actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            if let popoverController = actionSheet.popoverPresentationController {
                popoverController.sourceView = self.view
                popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
                popoverController.permittedArrowDirections = []
            }
            
            self.present(actionSheet, animated: true, completion: nil)
        }
        
        func showDoubleButtonAlert(title:String, message:String, action1:String, action2:String, completion1:@escaping ()->(), completion2:@escaping ()->()) {
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let act1 = UIAlertAction(title: action1, style: .default) { (action) in
                completion1()
            }
            let act2 = UIAlertAction(title: action2, style: .default) { (action) in
                completion2()
            }
            alert.addAction(act1)
            alert.addAction(act2)
            if let popoverController = alert.popoverPresentationController {
                popoverController.sourceView = self.view
                popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
                popoverController.permittedArrowDirections = []
            }
            
            present(alert, animated: true, completion: nil)
        }
        func showSingleButtonAlert(title:String, message:String, completion:@escaping ()->()) {
    //        let appearance = SCLAlertView.SCLAppearance(
    //            showCircularIcon: false
    //        )
    //        let alertView = SCLAlertView(appearance: appearance)
    //        alertView.showSuccess(title, subTitle: message)
           // SCLAlertView().showInfo(title, subTitle: message)
            
           let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
           let ok = UIAlertAction(title: "OK", style: .default) { (action) in
               completion()
           }
           alert.addAction(ok)
           present(alert, animated: true, completion: nil)
     }
}
