//
//  UIButtonExtention.swift
//  ComicHub
//
//  Created by Anil Jangir on 09/06/20.
//  Copyright © 2020 Anil Jangir. All rights reserved.
//

import UIKit



class ActualGradientButton: UIButton {

    override func layoutSubviews() {
        super.layoutSubviews()
        gradientLayer.frame = bounds
    }

    private lazy var gradientLayer: CAGradientLayer = {
        let l = CAGradientLayer()
        l.frame = self.bounds
        l.colors = [ ButtonStartColor.cgColor, ButtonEndColor.cgColor]
        l.startPoint = CGPoint(x: 0, y: 0.5)
        l.endPoint = CGPoint(x: 1, y: 0.5)
        l.cornerRadius = 16
        layer.insertSublayer(l, at: 0)
        return l
    }()
}
class TwoTagButton : UIButton {
    
    var row : Int?
    var section : Int?
    
}
